# SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
# SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
#
# SPDX-License-Identifier: MIT

"""fix_ids.py
rewrites the ids of the questions, so that they are in order to help with OCD.
should not be ran after final survey is built

"""

import yaml

id = 1

with open("meta/questions.yml") as q_yaml:
    with open("meta/questions_new.yml", "w") as q_yaml_new:
        for line in q_yaml:
            if "- id: Q0" in line:
                if id < 10:
                    q_yaml_new.write(f"- id: Q00{id}\n")
                else:
                    q_yaml_new.write(f"- id: Q0{id}\n")
                id += 1
            else:
                q_yaml_new.write(line)
