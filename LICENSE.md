# License Information

Copyright (c) 2021 Deutsches Elektronen-Synchrotron DESY
Copyright (c) 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
Copyright (c) 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
Copyright (c) 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
Copyright (c) 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
Copyright (c) 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ

This work is licensed under multiple licenses:

- The source code and the accompanying material are licensed under [MIT](LICENSES/MIT.txt).
- The documentation and the resulting plots are licensed under [CC-BY-4.0](LICENSES/CC-BY-4.0.txt).

Please see the individual files for more accurate information.

> **Hint:** We provided the copyright and license information in accordance to the [REUSE Specification 3.0](https://reuse.software/spec/).
