# SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
# SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
#
# SPDX-License-Identifier: MIT

"""
Pre-processing script for and adapted to the so called HIFIS-Survey-2021.

Pre-processing script that does a couple of steps to manipulate the survey
data to be clean and processed before it is used in the survey analysis within
survey analysis scripts contained in the HIFIS-Surveyval framework.
"""
from typing import List, Set

from pandas import DataFrame, Series

from hifis_surveyval.data_container import DataContainer
from hifis_surveyval.models.mixins.yaml_constructable import YamlDict
from hifis_surveyval.models.question_collection import QuestionCollection


def create_yaml_dict(
        var_collection_id: str,
        var_label: str,
        var_text_en: str,
        var_text_de: str) -> YamlDict:
    """
    Creates a YamlDict object that is used to create a new QuestionCollection.

    Args:
        var_collection_id (str):
            ID of the QuestionCollection to be created.
        var_label (str):
            Label to be used as the QuestionCollection label as well as
            Question label.
        var_text_en (str):
            English translation to be used as QuestionCollection text as well
            as Question text.
        var_text_de (str):
            German translation to be used as QuestionCollection text as well
            as Question text.

    Returns:
        YamlDict:
            YamlDict object that is used to create a new QuestionCollection.
    """
    var_question_id: str = "_"
    var_datatype: str = "str"
    var_mandatory: bool = False

    var_answeroption_yes_id: str = "A001"
    var_answeroption_yes_label: str = "Yes"
    var_answeroption_yes_text_en: str = "Yes"
    var_answeroption_yes_text_de: str = "Ja"

    var_answeroption_no_id: str = "A002"
    var_answeroption_no_label: str = "No"
    var_answeroption_no_text_en: str = "No"
    var_answeroption_no_text_de: str = "Nein"

    # QuestionCollection Dictionary
    yaml_dict: YamlDict = {
        "id": var_collection_id,
        "mandatory": var_mandatory,
        "label": var_label,
        "text": {
            "de-DE": var_text_de,
            "en-GB": var_text_en
        },
        "questions": [
            {
                "id": var_question_id,
                "mandatory": var_mandatory,
                "datatype": var_datatype,
                "label": var_label,
                "text": {
                    "de-DE": var_text_de,
                    "en-GB": var_text_en
                },
                "answers": [
                    {
                        "id": var_answeroption_yes_id,
                        "label": var_answeroption_yes_label,
                        "text": {
                            "de-DE": var_answeroption_yes_text_de,
                            "en-GB": var_answeroption_yes_text_en
                        }
                    },
                    {
                        "id": var_answeroption_no_id,
                        "label": var_answeroption_no_label,
                        "text": {
                            "de-DE": var_answeroption_no_text_de,
                            "en-GB": var_answeroption_no_text_en
                        }
                    }
                ]
            }
        ]
    }

    return yaml_dict


def add_data_to_collection(data: DataContainer,
                           collection: QuestionCollection,
                           data_all_idx: list,
                           data_true_idx: list) \
        -> QuestionCollection:
    """
    Adds data to an empty binary QuestionCollection.

    Args:
        data (DataContainer):
            HIFIS-Surveyval DataContainer with the data set.
        collection (QuestionCollection):
            QuestionCollection to add data to.
        data_all_idx (list):
            List of participant indexes which contains all users needed for
            evaluation.
        data_true_idx (list):
            List of participant indexes which contains the desired True
            outcome.

    Returns:
        collection (QuestionCollection):
            QuestionCollection containing data.
    """
    data_total_idx = \
        data.collection_for_id("Q001").as_data_frame().index.to_list()

    question = collection.question_for_id("_")

    # Loop over all participant IDs
    for participant_id in data_total_idx:
        if participant_id in data_all_idx:
            if participant_id in data_true_idx:
                question.add_answer(participant_id, "A001")
            else:
                question.add_answer(participant_id, "A002")
        else:
            question.add_answer(participant_id, None)

    return collection


def create_binary_collection(
    data: DataContainer,
    var_collection_id: str,
    var_label: str,
    var_text_en: str,
    var_text_de: str,
    data_all_idx: list,
    data_true_idx: list) \
        -> QuestionCollection:
    """
    Generating a True/False QuestionCollection.

    Args:
        data (DataContainer):
            HIFIS-Surveyval DataContainer with the data and metadata.
        var_collection_id (str):
            ID of the QuestionCollection to be created.
        var_label (str):
            Label to be used as the QuestionCollection label as well as
            Question label.
        var_text_en (str):
            English translation to be used as QuestionCollection text as well
            as Question text.
        var_text_de (str):
            German translation to be used as QuestionCollection text as well
            as Question text.
        data_all_idx (list):
            List of participant indexes which contains all users needed for
            evaluation.
        data_true_idx (list):
            List of participant indexes which contains the desired True
            outcome.

    Returns:
        DataContainer:
            Extended HIFIS-Surveyval DataContainer.
    """
    yaml_dict = create_yaml_dict(
        var_collection_id=var_collection_id,
        var_label=var_label,
        var_text_en=var_text_en,
        var_text_de=var_text_de
    )

    # Init QuestionCollection
    new_collection = QuestionCollection._from_yaml_dictionary(
        yaml_dict, settings=data._settings)

    new_collection = add_data_to_collection(
        data=data,
        collection=new_collection,
        data_all_idx=data_all_idx,
        data_true_idx=data_true_idx
    )

    return new_collection


def create_sw_developers_collection(data: DataContainer,
                                    var_collection_id: str,
                                    var_label: str,
                                    var_text_en: str,
                                    var_text_de: str) \
        -> QuestionCollection:
    """
    Creates a QuestionCollection from metadata and adds data.

    Args:
        data (DataContainer):
            DataContainer with metadata and data.
        var_collection_id (str):
            ID of the new QuestionCollection.
        var_label (str):
            Label to be used as the QuestionCollection label as well as
            Question label.
        var_text_en (str):
            English translation to be used as QuestionCollection text as well
            as Question text.
        var_text_de (str):
            German translation to be used as QuestionCollection text as well
            as Question text.

    Returns:
        QuestionCollection:
            Newly created QuestionCollection from metadata containing data.
    """
    # Get QuestionCollections
    centers: QuestionCollection = data.collection_for_id("Q001")
    swdev_job: QuestionCollection = data.collection_for_id("Q005")
    swdev_expertise: QuestionCollection = data.collection_for_id("Q006")

    # Get DataFrames
    centers_data: DataFrame = centers.as_data_frame()
    swdev_job_data: DataFrame = swdev_job.as_data_frame()
    swdev_expert_data: DataFrame = swdev_expertise.as_data_frame()

    # Drop other column from DataFrame
    centers_data = centers_data.drop(["Q001/other"], axis=1)

    # Filter out Nones
    centers_data_answered: DataFrame = centers_data.dropna()
    swdev_job_data_answered: DataFrame = swdev_job_data.dropna()
    swdev_expert_data_answered: DataFrame = swdev_expert_data.dropna()

    # Is developer?
    swdev_job_data_developer: DataFrame = \
        swdev_job_data_answered["Q005/_"] == "Yes"
    swdev_expert_data_developer: DataFrame = \
        swdev_expert_data_answered["Q006/_"] != "Non-developer"

    # Participant ID lists
    participant_ids: List[str] = centers_data_answered.index.to_list()
    developers_job: List[str] = swdev_job_data_developer.index.to_list()
    developers_expert: List[str] = swdev_expert_data_developer.index.to_list()

    # Determine developer participant IDs
    participant_ids_developer: List[int] = []
    # Loop over all participant IDs
    for participant_id in participant_ids:
        # Participant ID need to be in both developer sets
        if (participant_id in developers_job and
                participant_id in developers_expert):
            # Get index of participant IDs in each developer set
            developers_job_id = developers_job.index(participant_id)
            developers_expert_id = developers_expert.index(participant_id)
            # Value in each developer set needs to be True
            if (swdev_job_data_developer[developers_job_id] and
                    swdev_expert_data_developer[developers_expert_id]):
                # Add participant ID to final developer set
                participant_ids_developer.append(participant_id)

    new_collection = create_binary_collection(
        data=data,
        var_collection_id=var_collection_id,
        var_label=var_label,
        var_text_en=var_text_en,
        var_text_de=var_text_de,
        data_all_idx=participant_ids,
        data_true_idx=participant_ids_developer
    )

    return new_collection


def variable_sw_developers(data: DataContainer) -> DataContainer:
    """
    Extract participants that are either software developers or not.

    Args:
        data (DataContainer):
            DataContainer with metadata and data.

    Returns:
        DataContainer:
            DataContainer with metadata and data.
    """

    # IDs of new QuestionCollection / variable
    # IDs of additional variables start with a "V"
    var_collection_id: str = "V001"
    var_label: str = "Self-identification as software developer"
    var_text_en: str = "Do you consider yourself a software developer?"
    var_text_de: str = "Würden Sie sich als Software-Entwickler bezeichnen?"

    new_collection: QuestionCollection = \
        create_sw_developers_collection(data,
                                        var_collection_id,
                                        var_label,
                                        var_text_en,
                                        var_text_de)

    # Add new QuestionCollection to DataContainer
    data._survey_questions[var_collection_id] = new_collection

    return data


def variable_hifis(data: DataContainer) -> DataContainer:
    """
    Extract members of Helmholtz-centers that take part in the HIFIS-project.

    Args:
        data (DataContainer):
            HIFIS-Surveyval DataContainer with the data set.

    Returns:
        DataContainer:
            Extendend HIFIS-Surveyval DataContainer.
    """

    # Init working variables
    hifis_ids: list = []

    # Setting Constants
    var_collection_id: str = "V002"
    var_label: str = "Is your Helmholtz-centre part of the HIFIS-project?"
    var_text_en: str = "Are you working in a Helmholtz-centre " \
                       "that is part of the HIFIS-project?"
    var_text_de: str = "Arbeiten Sie in einem Helmholtz-Zentrum, " \
                       "welches zum HIFIS-Project gehört?"

    HIFIS: List[str] = ['AWI', 'DESY', 'DLR', 'DKFZ', 'FZJ', 'GFZ',
                        'HMGU', 'HZB', 'HZDR', 'KIT', 'UFZ']

    # ============================= #
    centers_data: Series = data.question_for_id("Q001/_").as_series()

    # Remove type NaN and None indexes
    centers_data = centers_data.dropna()

    # Valid center participant ids
    centers_ids = centers_data.index.to_list()

    # Generation of the participant ids of the hifis members
    for i, valA in enumerate(HIFIS):
        centers_tmp = centers_data[centers_data == valA].to_dict()

        for idx in centers_tmp.keys():
            hifis_ids.append(idx)

    new_collection = create_binary_collection(
        data=data,
        var_collection_id=var_collection_id,
        var_label=var_label,
        var_text_en=var_text_en,
        var_text_de=var_text_de,
        data_all_idx=centers_ids,
        data_true_idx=hifis_ids
    )

    # Add new QuestionCollection to DataContainer
    data._survey_questions[var_collection_id] = new_collection
    # ======================================================= #

    return data


def variable_team(data: DataContainer) -> DataContainer:
    """
    Extract participants that are either team or individual developers.

    Args:
        data (DataContainer):
            HIFIS-Surveyval DataContainer with the data set.

    Returns:
        DataContainer:
            Extended HIFIS-Surveyval DataContainer.
    """

    # Setting Constants
    var_collection_id: str = "V003"
    var_label: str = "Do you work in a software development team?"
    var_text_en: str = "Do you develop software in a team?"
    var_text_de: str = "Entwickeln Sie Software in einem Team?"

    group_data: Series = data.question_for_id("Q038/_").as_series()

    # Init working variables
    team_ids: list = []

    # Removing NaN and 'None' members
    group_data = group_data.dropna()

    # Valid participant group ids
    group_ids = group_data.index.to_list()

    # Participant ids of the HIFIS members
    for idx in group_ids:
        if 'team' in group_data[idx]:
            team_ids.append(idx)

    # Generating the extended data collection
    new_collection = create_binary_collection(
        data=data,
        var_collection_id=var_collection_id,
        var_label=var_label,
        var_text_en=var_text_en,
        var_text_de=var_text_de,
        data_all_idx=group_ids,
        data_true_idx=team_ids
    )

    # Add new QuestionCollection to DataContainer
    data._survey_questions[var_collection_id] = new_collection
    # ======================================================= #

    return data


def exclusion(data: DataContainer) -> DataContainer:
    """
    Exclude incomplete and invalid cases from the given data set.

    Args:
        data (DataContainer):
            HIFIS-Surveyval DataContainer with the data set.

    Returns:
        DataContainer:
            HIFIS-Surveyval DataContainer where incomplete and invalid cases
            are set to None.
    """
    # Survey section that should be completed by all respondents:
    # general questions, filter questions, and cloud questions
    questions: List = ["Q001", "Q002", "Q003", "Q004", "Q005", "Q006", "Q007",
                       "Q008", "Q009", "Q010", "Q011", "Q012", "Q013", "Q014"]

    # "Raw" data set for this section
    data_set: DataFrame = data.data_frame_for_ids(requested_ids=questions)
    all_cases: Set = set(data_set.index)

    # Sub-set indices
    # As the survey targeted different groups, respondents were presented with
    # different sub-sets of questions, resulting in different data sub-sets
    # Research staff: respondents who reported they worked in "Research" or
    #                 "Infrastructure", not in "IT" or "Administration"
    # Developers: respondents who reported some skill level or expertise in
    #             software development ("Beginner", "Competent", "Proficient",
    #             "Advanced", "Professional"), not that they categorized
    #             themselves as "Non-developers"
    # Active developers: respondents who reported that they developed software
    #                    in the past 12 months ("Yes"), not that they did not
    #                    ("No")
    research_staff: Set = set(
        data_set.loc[(data_set["Q003/_"] != "IT") &
                     (data_set["Q003/_"] != "Administration")].index
    )
    developers: Set = set(
        data_set.loc[data_set["Q006/_"] != "Non-developer"].index
    )
    active_developers: Set = set(
        data_set.loc[data_set["Q005/_"] == "Yes"].index
    )

    # Exclusion indices
    incomplete_cases: Set = set()

    # Set multi-index for better question collection handling
    data_set.columns = data_set.columns.str.split('/', expand=True)

    # Go through questions and find cases with missing values
    for question in questions:
        # Question Q004 ("research field") was only represented to respondents
        # who categorized themselves as research staff
        if question == "Q004":
            incomplete_sub_set_cases = research_staff.difference(
                set(data_set.loc[research_staff, question].dropna(how="any")
                    .index)
            )
        # Question Q007 ("Working time spent on software development") was only
        # presented to respondents who reported that they developed software
        # in the past 12 months
        elif question == "Q007":
            incomplete_sub_set_cases = active_developers.difference(
                set(data_set.loc[active_developers, question]
                    .dropna(how="any").index)
            )
        # Question Q008 ("RSE identity") was only presented to respondents who
        # reported some level of software development expertise
        elif question == "Q008":
            incomplete_sub_set_cases = developers.difference(
                set(data_set.loc[developers, question].dropna(how="any")
                    .index)
            )
        # Questions composed of multiple questions require at least one valid
        # answer in any of the sub-question; exclude if all answers are missing
        elif (question == "Q010" or question == "Q011" or question == "Q013"
              or question == "Q014" or question == "Q001"):
            incomplete_sub_set_cases = all_cases.difference(
                set(data_set.loc[:, question].dropna(how="all").index)
            )
        # Questions composed of a single question require a valid answer to
        # this question; exclude if any answer is missing
        else:
            incomplete_sub_set_cases = all_cases.difference(
                set(data_set.loc[:, question].dropna(how="any").index)
            )
        incomplete_cases.update(incomplete_sub_set_cases)
        print("{}: {} exclusions".format(
            question, len(incomplete_sub_set_cases)
        ))

    # Respondents who completed all questions of this section of the survey
    valid_cases: Set = all_cases.difference(incomplete_cases)

    # Find invalid cases, i.e. external to Helmholtz
    invalid_cases: Set = set(data_set.loc[
        data_set.loc[:, ("Q001", "_")] == "Other"
    ].index)
    print("{}: {} exclusions".format(
        "Q001", len(invalid_cases)
    ))

    # Find invalid cases, i.e. never worked for Helmholtz
    invalid_cases.update(set(data_set.loc[
        data_set.loc[:, ("Q002", "_")] == "0 years"
    ].index))
    print("{}: {} exclusions".format(
        "Q002", len(invalid_cases)
    ))

    # Respondents who completed all questions of this section of the survey
    # and also belong to target population
    valid_cases.difference_update(invalid_cases)

    # Remove incomplete and invalid cases
    data.mark_answers_invalid(incomplete_cases)
    data.mark_answers_invalid(invalid_cases)
    data.remove_invalid_answer_sets()

    print("\nAfter exclusion of {} incomplete and {} invalid cases the final "
          "data set contains n={} cases".format(len(incomplete_cases),
                                                len(invalid_cases),
                                                len(valid_cases)))

    return data


def preprocess(data: DataContainer) -> DataContainer:
    """
    Pre-process data set.

    Args:
        data (DataContainer):
            HIFIS-Surveyval DataContainer with the raw data set.

    Returns:
        DataContainer:
            HIFIS-Surveyval DataContainer of the final data set.
    """
    # Exclude incomplete and invalid cases
    data = exclusion(data)

    # Add new variable V001 about Developer/Non-Developer.
    data = variable_sw_developers(data)

    # Add new variable V002 about HIFIS/Non-HIFIS members.
    data = variable_hifis(data)

    # Add new variable V003 about team/individual developers.
    data = variable_team(data)

    # Display summary for final data
    final_data_set: DataFrame = data.data_frame_for_ids(
        requested_ids=data.question_collection_ids
    )

    print("\n", final_data_set.describe(include="all")[:1].T)

    return data
