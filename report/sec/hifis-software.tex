% SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
% SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
% SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
% SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
% SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
% SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
%
% SPDX-License-Identifier: CC-BY-4.0

The \textit{HIFIS Software Services} cluster comprises four work packages that support scientific and non-scientific staff and their software development projects. 
Before taking a look at work package specific questions, we analysed different aspects of software development including characteristics of different developer groups as well as characteristics of different software projects to get an overview of the role of software development and our main target group(s). 

\paragraph{Developer groups} 
Respondents, who reported that they developed software within the past 12 months, were asked to estimate the proportion of working time they spent on software development. On average, software development consumed about one third of their working time but varied greatly (Table \ref{tab:sample_stats}). 
As this variance suggested a fairly inhomogeneous group, we analysed the time consumption of software development for different sub-samples. In particular, we were interested in the influence of (1) research domain including the six \textit{Helmholtz research fields}, (2) competence level ranging from 1 - \textit{Beginner} to 5 - \textit{Professional}, and (3) occupation (Table \ref{tab:coding_time_stats}).

\input{tbl/coding-time.tex}

Figure \ref{fig:coding_time_per_field} shows that, across fields, half of the respondents spent at least 20\% their working time on software development, while respondents from the field of \textit{Information} reported the highest time investment with half of them spending more than 60\% on software development. 
Indeed, we found a small effect of research field on the time that was spent on software development, \textit{F}(5,176) = 2.62, \textit{p} = 0.026. Post-hoc pair-wise comparison using Tukey's HSD criterion showed, however, that differences were only statistically significant between sub-samples of \textit{Information} and \textit{Earth and environment} (\textit{p} = 0.018) and \textit{Information} and \textit{Energy} (\textit{p} = 0.016), while there were no significant differences between other groups (\textit{p} $>$ 0.05 for all other comparisons). 
Our findings suggest that, while there is substantial variance, software development is similarly important to all Helmholtz research fields. 

\begin{figure}[hbt]
	\centering
	\includegraphics[width=\textwidth]{fig/Q007-by-Q004-box-plot.pdf}
	\caption{Proportion of working time spent on software development for the six \textit{Helmholtz} research fields: \\
	\textit{Aeronautics} (n=59), \textit{Earth and environment} (n=47), \textit{Energy} (n=14), \textit{Health} (n=17), \textit{Information} (n=11), \textit{Matter} (n=34). Box plots display median (red solid line), range and inter-quartile range, circles indicate outliers (1.5 times inter-quartile range).}
	\label{fig:coding_time_per_field}
\end{figure}

The better one gets at something, the more likely they are to spend time doing it and some even make a career out of it. 
This seems also true for software development: 
Figure \ref{fig:coding_time_per_level} shows an overall increase in how much working time is spent on software development from beginners to professional software developers. 
However, the better one gets at something, the less time they will usually need to do it. 
Such a decrease in time that is spent on software related tasks could be expected for the transition from beginner to competent level. 
While the difference between these groups was not statistically significant (\textit{p} = 0.900), there was an overall effect of competence level on time spent on software development, \textit{F}(4, 218) = 11.67, \textit{p} $<$ 0.001. 
Respondents who categorised themselves as professional or advanced software developers spent significantly more time on software development than respondents who categorised themselves as beginner, competent or proficient software developers (\textit{p} $<$ 0.05 for all comparisons except \textit{Beginner} and \textit{Advanced}). 
These levels did not differ (\textit{p} = 0.05), neither did the two highest levels (\textit{p} = 0.144).

\begin{figure}[hbt]
	\centering
	\includegraphics[width=\textwidth]{fig/Q007-by-Q006-box-plot.pdf}
	\caption{Proportion of working time spent on software development for five competence levels: \\
	\textit{Beginner} (n=8), \textit{Competent} (n=38), \textit{Proficient} (n=63), \textit{Advanced} (n=72), \textit{Professional} (n=42). Box plots display median (red solid line), range and inter-quartile range, circles indicate outliers (1.5 times inter-quartile range).}
	\label{fig:coding_time_per_level}
\end{figure}

These results could be a good starting point for planning future software services. 
In particular in the context of educational interventions, such as software development workshops, it would be interesting to further explore this relationship between competence level and time needed for different software development tasks. 
Demonstrating that HIFIS Software training programs, that specifically aim to advance such skills, do not only improve software quality but even efficiency would be a strong argument for expanding such services in the future. 
In situations where limited resources require careful consideration regarding which offerings one should focus on, the present results might also contribute to the discussion of which groups will most benefit from such offerings. 
Based on the present results it could be argued that more advanced courses are strongly needed because software developers at an advanced or professional level spent significantly more of their working time on it. 
Finally, the occupational group (Figure \ref{fig:coding_time_per_occupation}) had no effect on the time spent on software development in our sample, \textit{F}(3,219) = 2.08, \textit{p} = 0.103. Except for the admininstration staff, all groups experience software development as a substantial part of their jobs.

\begin{figure}[hbt]
	\centering
	\includegraphics[width=\textwidth]{fig/Q007-by-Q003-box-plot.pdf}
	\caption{Proportion of working time spent on software development for four occupational groups: \\
		\textit{Administration} (n=2), \textit{Infrastructure} (n=27), \textit{IT} (n=39), \textit{Research} (n=155). Box plots display median (red solid line), range and inter-quartile range, circles indicate outliers (1.5 times inter-quartile range).}
	\label{fig:coding_time_per_occupation}
\end{figure}

Separate analyses were performed for different sub-samples grouped by research field, competence level and occupational group. 
Results show that advanced or professional software developers and those who work in the field of Information spent the most time on software development. 
The results show that there is considerable variance between groups but also within groups, supporting the view that other factors than these determine how much time is spent on software development. 
In addition to research field, competence level and occupation a number of intra- and interpersonal factors as well as characteristics of the software itself certainly have an effect on the time that is or must be spent on software development. 
Moreover, there might be better indicators of the importance of software than the working time spent. This coarse approach to analyse different target groups of software development support provided, nonetheless, some intriguing insights that we can draw on when planning future support services.
