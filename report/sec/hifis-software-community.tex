% SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
% SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
% SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
% SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
% SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
% SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
%
% SPDX-License-Identifier: CC-BY-4.0

One goal of \textit{HIFIS Software Community} is to establish and promote networking and knowledge exchange regarding software development across and within \textit{Helmholtz} centres. 
To provide a platform for such activities, events like the \textit{Helmholtz Hacky Hour} or the \textit{DLR Wissensaustauschworkshop} have been initiated. 
As these events strongly depend on the participation and active involvement of software developers and other interested parties, our main motivation for the current survey was to learn which aspects would contribute to the attractivity of these events.
Respondents were therefore asked to report on their (1) general interest in participating in community events, (2) satisfaction with previous events, (3) preferred size of such events, (4) preferred form or level of involvement, and lastly (5) topic suggestions for future events.

Additionally, we were interested to learn whether differently experienced groups would differ in their preference of certain aspects and thus should be targeted individually.
During previous events, for instance in a number of \textit{Helmholtz Hacky Hour} sessions, we observed an overall low participation and a noticeable imbalance in terms of contribution to the discussion. 
While the first observation could be explained by a lack of communication channels and contacts to particular \textit{Helmholtz} centres, in the latter case we hypothesised that participation could be motivated by a different fit for different groups. 
Specifically, we assumed that participants who were generally more involved in software development had a stronger interest in such events and were more likely to share their experience and actively participate in discussions than those who just started developing their own software projects. 
We explored this assumption through separate analysis for two sub-samples, respondents who identified as research software engineers (\textit{RSE}s) and those who did not.

\paragraph{General response to community events}
Figure \ref{fig:community_events_interest} shows that only a small proportion of respondents actually attended community events. 
While \textit{RSE}s indicated that they were rather interested in different topics, non-\textit{RSE}s were generally not interested in joining such events.
Respondents who attended events were overall satisfied (\textit{M}=4.12, \textit{SD}=0.95). Respondents who were not interested in community events provided additional information about their lack of interest. 
Comments were summarised and resulting categories are displayed in Figure \ref{fig:community_events_no_interest}. 
More than one third indicated that software community event were not relevant, which was not surprising because this question addressed respondents independent of their software development activities in the past 12 months. 
More interestingly, about one third indicated that they had other priorities or simply no time, which suggests a quite low importance of engaging in networking or knowledge exchange.

\begin{figure}[hbtp]
	\centering
	\includegraphics[width=\textwidth]{fig/Q030-by-Q008-bar-plot.pdf}
	\caption{Interest in community events of respondents who identified as research software engineers (n=35) and respondents who did not (n=153).}
	\label{fig:community_events_interest}
\end{figure}

\begin{figure}[hbtp]
	\centering
	\includegraphics[width=\textwidth]{fig/Q030-SQ001comment-bar-plot.pdf}
	\caption{Reasons for not participating in community events. Bars represent categories of free text responses provided by n=70 respondents.}
	\label{fig:community_events_no_interest}
\end{figure}

Among those events that respondents had heard of (Figure \ref{fig:community_events_known}) or participated in (Figure \ref{fig:community_events_attended}) workshops had raised most interest. Workshops, while they might not be considered as community events in a strict sense, can be an excellent starting point to introduce novices to the complex topic of software development and familiarise them with various aspects. 
On the other hand, additional effort of \textit{HIFIS Software Community} is required to also connect more advanced developers. 
Indeed, individual comments pointed out that the low interest in community events was due to the lack of events beyond introductory workshops.

\begin{figure}[hbtp]
	\centering
	\includegraphics[width=\textwidth]{fig/Q030-SQ003comment-bar-plot.pdf}
	\caption{Recall of community events that were announced in the past 12 months (n=19) as indicator of peripheral interest in such events. Bars represent categories of free text responses. \\
	SE = Software Engineering; WAW = Wissensaustauschworkshop}
	\label{fig:community_events_known}
\end{figure}

\begin{figure}[hbtp]
	\centering
	\includegraphics[width=\textwidth]{fig/Q030-SQ004comment-bar-plot.pdf}
	\caption{Community events respondents participated in (n=34) as indicator of successful formats. Bars represent categories of free text responses.\\
	SE = Software Engineering; WAW = Wissensaustauschworkshop}
	\label{fig:community_events_attended}
\end{figure}

\paragraph{Design aspects of community events}
In terms of event size, smaller or medium-sized events were clearly preferred by both groups, \textit{RSE}s and non-\textit{RSE}s, compared to large-scale events (Figure \ref{fig:community_events_size}). 
This result suggests the need for workshops and study-group-like formats or events such as the \textit{DLR Software Engineering Wissensaustauschworkshop} with a typical size of 60-70 participants rather than \textit{Helmholtz}-wide software conferences. 
While talk sessions were overall the most preferred format and seemed equally attractive for both groups, more interactive formats like group discussion and one-on-one sessions were more interesting for \textit{RSE}s than they were for non-\textit{RSE}s (Figure \ref{fig:community_events_involvement}). 
As expected, \textit{RSE}s were also much more willing to share their knowledge by giving a talk. 
Taken together, our results show that events other than workshops raise more interest among \textit{RSE}s but both groups prefer the same formats in terms of size and involvement with the exception of their interest in giving talks. 
Events that address both groups should focus on more talk-oriented formats not exceeding 75 participants, while events that address specifically more experienced software developers should allow for more direct interaction in smaller groups.

\begin{figure}[hbtp]
	\centering
	\includegraphics[width=\textwidth]{fig/Q032-by-Q008-bar-plot.pdf}
	\caption{Preferred size of community events among respondents who identified as research software engineers (n=35) and respondents who did not (n=112).}
	\label{fig:community_events_size}
\end{figure}

\begin{figure}[hbtp]
	\centering
	\includegraphics[width=\textwidth]{fig/Q033-by-Q008-bar-plot.pdf}
	\caption{Preferred involvement in community events among respondents who identified as research software engineers (n=35) and respondents who did not (n=111).}
	\label{fig:community_events_involvement}
\end{figure}

\paragraph{Topic suggestions for community events}
In particular the group of \textit{RSE}s reported difficulties of finding the right events that match their topics or level of experience, pointing out the need for events that specifically focus on more advanced aspects of software development. 
Figures \ref{fig:community_events_no_topics} and \ref{fig:community_events_topics} display topics that respondents felt were insufficiently addressed or not covered at all by previous community events and topic suggested for future events, respectively.
These results speak in favor of maintaining a focus on the programming language \textit{Python} (see \ref{subsubsec:education}) and add software testing and machine learning as dominant topics.

\begin{figure}[hbtp]
	\centering
	\includegraphics[width=\textwidth]{fig/Q030_SQ002comment-word-cloud.pdf}
	\caption{Topics not sufficiently covered by community events.}
	\label{fig:community_events_no_topics}
\end{figure}

\begin{figure}[hbtp]
	\centering
	\includegraphics[width=\textwidth]{fig/Q034-word-cloud.pdf}
	\caption{Topics suggested for future community events.}
	\label{fig:community_events_topics}
\end{figure}
