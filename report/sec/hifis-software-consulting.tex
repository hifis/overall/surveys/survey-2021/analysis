% SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
% SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
% SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
% SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
% SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
% SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
%
% SPDX-License-Identifier: CC-BY-4.0

\newcommand{\scaleF}{0.75}

In the consulting work package, we wanted to ascertain which challenges the survey participants face when working with software projects.
Furthermore, we wanted to find out about the preferred tools to approach these challenges, why these tools were chosen, and how useful they are.
We also wanted to know which forms of consulting are the preferred choice for solving a challenge.
For a more in-depth look, we compared the results between respondents from HIFIS and Non-HIFIS centres as well as developers with and without a team (Single/Team-Developers).


\subsubsection*{Faced Challenges}

In this section, we analyse the survey response for question 25. \emph{``Which were the biggest challenges you faced in the last twelve months when working on a software/coding project?''} had 14 multiple choice options viz: \emph{None, Start of a new project, Technology selection, Requirements analysis, Project management, Development process, Structuring of code, Code quality, Testing, Automation, Legal issues, Team resources, Physical resources, Lack of developers}.

A total of 288 survey participants answered the question.
From the responses, it can be seen that the issues related to writing high-quality source code and software testing were the most challenging for the respondents (\emph{Code Quality}: 38.9\%, \emph{Structuring of code}: 29.2\%, \emph{Testing}: 29.2\%) as seen in Figure \ref{fig:consulting_Q025a}. 
Overall, the participants also reportedly faced challenges related to human resources (\emph{Team resources}: 29.2\%, and \emph{Lack of developers}: 29.5\%).
\emph{Legal issues} were not reported as heavily challenging (10.4\%) along with 14.2\% reported having faced no challenges at all.

\begin{figure}[!h]
	\centering
	\includegraphics[scale=\scaleF]{fig/consulting_Q025-all-bar-list02}
	\caption{User answers given to question 25 \emph{``Which were the biggest challenges you faced in the last twelve months when working on a software/coding project?''}}
	\label{fig:consulting_Q025a}
\end{figure}

When we delineate the data based on the respondents' institute, it can be reported that the participants from HIFIS-associated centres were facing more challenges in the fields mentioned above than respondents from Non-HIFIS ones illustrated in Figure \ref{fig:consulting_Q025b}.

\begin{figure}[!h]
	\centering
	\includegraphics[scale=\scaleF]{fig/consulting_Q025-hifis-bar-list00}
	\caption{Answers given by HIFIS and non-HIFIS survey participants to Question 25: \emph{Structure of code, Code quality,} and \emph{Testing}}
	\label{fig:consulting_Q025b}
\end{figure}

When delineating the data based on respondents' team size (Single developer vs Team developers), a significant difference cannot be reported implying both categories faced those challenges with similar intensity as shown in Figure \ref{fig:consulting_Q025c}.
In addition, challenges related to \emph{Code quality} (39.5\% overall), team developers having issues regarding availability of \emph{Team resources} and \emph{Lack of developers}, with around 36.0\%.

\begin{figure}[!h]
	\centering
	\includegraphics[scale=\scaleF]{fig/consulting_Q025-team-bar-list01}
	\caption{Answers given by single and team developers in correspondence to the answer options: \emph{Structure of code, Code quality}, \emph{Testing}, \emph{Lack of developers}, and \emph{Team resources} from Question 25}
	\label{fig:consulting_Q025c}
\end{figure}

In conclusion, we can say that the responses indicate practical challenges in software development.
Writing high-quality code and proper testing routines are the most common issues. 
These observations align with our current focus in consulting. We support our clients in restructuring their software projects, implementing test routines and increasing their code quality according to our standards. 
Survey participants also reported a lack of human resources. 
HIFIS Consulting can not address those challenges directly but can mitigate and provide support for those issues that arose. 


\subsubsection*{Used Tools, Preferred Approaches and Usefulness}

In this section, we analyse the survey responses for question 26, 27 and 28 combined since the 3 of them are interconnected. Question 26: \emph{``Which tools or approaches did you use to solve these challenges when they occurred?''} had 11 multiple choice options viz:  \emph{None, Search Engine, Literature Research, Colleagues, Expert Knowledge-bases (reading answers), Expert Knowledge-bases (asking questions), Tutorial, Attending Training, Organising Workshops, Conferences} and \emph{Consulting}.

Consequently, question 27: \emph{``Why did you not use these tools or approaches to solve these challenges when they occurred?''} having the options \emph{Not Prefered, Lack of time, Not comfortable, Not Available, Not Aware}, enquired on why approaches/choices listed in question 26 were not used.

Furthermore, question 28:
\emph{``How useful were these tools or approaches with respect to solving these challenges when they occurred?''} having the options \emph{0, 1, 2, 3, 4, 5}, were to rank how workable and useful the approaches/choices selected in Question 26 were for the respondents.

Using \emph{Search engines} (79.8\%) and \emph{Reading answers in expert knowledge-bases} (67.5\%) were by far the most used approaches. 
This result, illustrated in Figure \ref{fig:consulting_Q026a}, is attributable to the fact that both tools are easy to access and deliver fast results.

\begin{figure}[!h]
	\centering
	\includegraphics[scale=\scaleF]{fig/consulting_Q026-all-bar-list01}
	\caption{User answers given to the question 26 \emph{``Which tools or approaches did you use to solve these challenges when they occurred?''}}
	\label{fig:consulting_Q026a}
\end{figure}

Nevertheless, reading answers in expert knowledge-bases are clearly preferred among respondents working in HIFIS-centres, as indicated in Figure \ref{fig:consulting_Q026b}. 
This suggests that expert knowledge is easier accessible within HIFIS-centres.

\begin{figure}[!h]
	\centering
	\includegraphics[scale=\scaleF]{fig/consulting_Q026-hifis-bar-list00}
	\caption{Answers given by HIFIS and non-HIFIS survey participants in correspondence the answer options: \emph{Expert knowledge-bases (reading answers)} in Question 26}
	\label{fig:consulting_Q026b}
\end{figure}

57.6\% of the participants answered in Question 26 that \emph{Colleagues} are also an important resource for solving issues when facing software development tasks as seen in Figure \ref{fig:consulting_Q026a}.
These three most given solution approaches from Question 26 (see Fig. \ref{fig:consulting_Q026a}) are also emphasized by the answers given in Question 28, as shown in Figure \ref{fig:consulting_Q028a}.

\begin{figure}
	\centering
	\includegraphics[height=0.62\textwidth,angle=90]{fig/consulting_Q028-all-rel-bar-list00}
	\caption{User answers given to the question 28 \emph{``How useful were these tools or approaches with respect to solving these challenges when they occurred?''}}
	\label{fig:consulting_Q028a}
\end{figure}

As indicated in Figure \ref{fig:consulting_Q026a}: \emph{Expert knowledge-bases asking questions}~(7,8\%), \emph{Organising workshops}~(5.8\%) and \emph{Attending trainings}~(11.5\%) are not preferred for solving challenges.
These approaches are more time-consuming and require preparation, making them less attractive.
This analogy is supported by Question 27, where these items obtained the highest values due to lack of time (see Figure \ref{fig:consulting_Q027a}).

\begin{figure}[!h]
	\centering
	\includegraphics[height=0.62\textwidth,angle=90]{fig/consulting_Q027-all-rel-bar-list01}
	\caption{User answers given to the question 27 \emph{``Why did you not use these tools or approaches to solve these challenges when they occurred?''}}
	\label{fig:consulting_Q027a}
\end{figure}

\emph{Literature Research}(30.9\%), as indicated in Figure \ref{fig:consulting_Q026a}, is not among the preferred approaches to solve a coding challenge.
The respondents also state that they do not use it foremost to lack of time, based on the answers given in Question 27 (54.8\%, shown in Figure \ref{fig:consulting_Q027a}).
The survey participants did not use \emph{Consulting} as a tool to solve their challenges (4.9\%, see Figure \ref{fig:consulting_Q026a}).
In Question 27 it becomes clear, that most respondents say it is not available for them (43.3\%) or time-consuming (21.7\%).
Additionally, question 27 points out that many respondents, in particular non-HIFIS members (55.8\%), stated that Consulting as a service was not available, 18\% more then HIFIS participants (see Figure \ref{fig:consulting_Q027b}). 

\begin{figure}[!h]
	\centering
	\includegraphics[scale=\scaleF]{fig/consulting_Q027-hifis-bar-list00}
	\caption{Answers given by HIFIS and non-HIFIS survey participants in correspondence the answer options: \emph{Expert knowledge-bases (reading answers)} in Question 27}
	\label{fig:consulting_Q027b}
\end{figure}

The results from question 26 to 28 indicate that the respondents prefer effortless approaches, that are easy to access and produce fast results.
Thus consulting could be made more attractive to clients if it appears as a helpful tool that provides customised support in an uncomplicated manner.
The results also show, that many respondents working in HIFIS-centres are not aware of HIFIS-Consulting.
Question 26 got 243 participants leading to 228 and 213 participants for the corresponding Questions 27 and 28, respectively.

Analysing the responses for Question 26, we can see that the choices \emph{Search engines} and \emph{Expert knowledge-bases (reading answers)} were by far the most preferred approaches.
This result is natural since both tools are easy to access and deliver fast results.
When delineating the data based on the respondents' institute, it can be reported that the participants from HIFIS-associated centres clearly preferred \emph{Expert knowledge-bases (reading answers)} with 71.6\% votes vs. 51\% from non-HIFIS centres (see Figure \ref{fig:consulting_Q026b}).

In general, across all centres, \emph{Colleagues}(57.6\%) was also a prominent choice of approach when facing challenges. These three choices, \emph{Search engines}, \emph{Expert knowledge}, and \emph{Colleagues}, are not only the most preferred in question 26, they were also reported in the responses of question 28 as the most rated ones, as well.

Other survey choices like the \emph{Expert knowledge-bases (Asking Questions)} (7.8\%), \emph{Organising workshops}(5.8\%) and \emph{Attending Trainings}(11.5\%) were among the least preferred choices of approach for solving challenges. These approaches are more time-consuming and do require preparation, which makes them less attractive. This is consistent with responses for Question 27, where these choices obtained the highest response of \emph{Lack of Time}.

Other choices like \emph{Literature Research} (30.9\%) was also not among the preferred approaches to solve a challenge.
The respondents also answered not using it due to \emph{Lack of time} in Question 27 (54.8\%).

The survey participants also did not opt \emph{Consulting} as a tool of preference to solve their challenges (4.9\%).
From the answers given in Question 27, it becomes clear that most respondents reported it either as \emph{Not available} (43.3\%) or \emph{Time consuming} (21.7\%).
Additionally, the responses from question 27 show, that many respondents even from HIFIS-associated centres were not aware, that they could make use of consulting (37.5\%). 
The results from question 26 to 28 indicate that the respondents prefer effortless approaches, that are easy to access and produce fast results.
Thus, HIFIS Consulting could be made more attractive to clients if it appears as a helpful tool that provides customised support in an uncomplicated manner. Nevertheless, it shouldn't be ignored that 55.8\% from non-HIFIS centres reported being unaware of the availability of consulting services like the HIFIS Consulting (see Figure \ref{fig:consulting_Q027b}). Thus, we should raise awareness that consulting is accessible across all centres.

\subsubsection*{Preferred Forms of Consulting}

In question 29, we wanted to ascertain which forms of consultations are most useful.
In this section, we analyse the survey response for question 29.
The question: \emph{``How useful would you find the following forms of consultation if you were to use consultation to solve a challenge in your sofware/project?''} had the participants asking to rank the following consulting approaches: \emph{Literature, Tutorials, Workshops, Discussion with expert, Analysis by an expert, Hands-on support} on a 6-point scale, ranging from 0 (not helpful at all) to 5 (very helpful).
A total of 229 survey participants answered the question.
Except for \emph{Literature} (Median of 3), the respondents rated all forms of consultation as helpful (Median of 4).
The respondents reported that they would prefer \emph{Discussion with an expert} by far (75\% of answers between 4-5, Median 4).
The options \emph{Analysis by an expert} and \emph{Hands on expert}, \emph{Tutorials} and \emph{Workshops} were ranked helpful, as well, all with a median rank of 4. 
All results are displayed in Figure \ref{fig:consulting_Q029a}.

\begin{figure}[!h]
	\centering
	\includegraphics[scale=\scaleF]{fig/consulting_Q029-all-box-list00}
	\caption{User answers given to the question 27 \emph{``How useful would you find the following forms of consultation if you were to use consultation to solve a challenge in your sofware/project?''}}
	\label{fig:consulting_Q029a}
\end{figure}

The responses indicate that the respondents regard a discussion with an expert as the most valuable form of consultation.
This is in line with our current HIFIS-consulting approach where we try to ensure, that we always have a (domain) expert is participating in the consultings.
