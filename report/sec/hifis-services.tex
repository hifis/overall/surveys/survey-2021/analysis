% SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
% SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
% SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
% SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
% SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
% SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
%
% SPDX-License-Identifier: CC-BY-4.0

The \textit{HIFIS} platform consists of three competence clusters, \textit{Backbone}, \textit{Cloud} and \textit{Software Services}, that together aim at providing a \textit{Helmholtz}-wide infrastructure for easily accessible cloud services as well as education and training on sustainable scientific software engineering. 
The present study focused on the work of \textit{Cloud} and \textit{Software Services} but we also included three questions addressing general aspects of \textit{HIFIS Services}. 

\paragraph{Use of \textit{HIFIS} services}
First of all, we were interested in assessing the overall use or recognition of \textit{HIFIS Services}. 
Figure \ref{fig:HIFIS_services_use} shows that there were only marginal differences between respondents belonging to centres that are actively involved in \textit{HIFIS} and those that are not. 
Less than 1 out of 5 respondents indicated that they used any of the services provided by \textit{HIFIS} in the past 12 months. 
As we did not specify any services, this result can be interpreted differently. 
It could mean that these respondents actually did not use any of the services or that they did not recognise using \textit{HIFIS services}. 
The latter would be a positive outcome in particular for \textit{Cloud} and \textit{Backbone}, as it demonstrates a seamless integration of their services in respondents workflows. 
Interestingly, respondents from centres not involved in \textit{HIFIS} were slightly more aware of using \textit{HIFIS} services than respondentes from \textit{HIFIS} centres.

\begin{figure}[hbt]
	\centering
	\includegraphics[width=\textwidth]{fig/Q035-by-V002-bar-plot.pdf}
	\caption{Use of \textit{HIFIS} services among \textit{Helmholtz} centres involved in \textit{HIFIS} (n=271) and \textit{Helmholtz} centres not involved in \textit{HIFIS} (n=78).}
	\label{fig:HIFIS_services_use}
\end{figure}

\paragraph{Language acceptance}
The use of tools or support offerings strongly depends on their accessibility. 
An important factor for the successful use of services that are mediated through social interaction is the language. 
Within the \textit{HIFIS} team, communication is mostly carried out in English and most services are currently offered in English due to the highly international target group. 
However, language is not only known as a facilitator but can also pose additional barriers if potential users of a service are not fluent in a particular language. 
We expected that specifically for services that include teaching as a key element, the acceptance of English as a default language would be lower. 
Figure \ref{fig:HIFIS_services_language} supports this view partly. 
For workshops, community events, and consultations, all of which involve a high level of verbal interaction, acceptance of English was comparatively lower than for reading material such as guidelines or documentation. 
Based on the present results, it appears vital to keep offering at least consultations also in German. 
Overall, the results suggest a high level of acceptance for offering services solely in English, however. 
It would have been interesting to compare these results to the acceptance of German as the default language. 

\begin{figure}[hbt]
	\centering
	\includegraphics[width=\textwidth]{fig/Q036-bar-plot.pdf}
	\caption{Language acceptance for different \textit{HIFIS} services.}
	\label{fig:HIFIS_services_language}
\end{figure}

\paragraph{Communication channels}
To optimise our communication strategy in the future and to get an idea how to reach non-\textit{HIFIS} centres, we asked respondents to indicate their preferred communication channels. 
As Figure \ref{fig:HIFIS_services_communication} shows, the majority of respondents chose mailing lists or the \textit{HIFIS} homepage as their primary communication channel. 
Twitter and RSS feeds were not considered as suitable communication channels. 
And again, we did not find any considerable differences between respondents from \textit{HIFIS} centres compared to those from centres that are not directly involved in \textit{HIFIS}, suggesting that there is no need to adjust our communication strategy depending on the centre.

\begin{figure}[hbt]
	\centering
	\includegraphics[width=\textwidth]{fig/Q037-by-V002-bar-plot.pdf}
	\caption{Preferred communication channels among \textit{Helmholtz} centres involved in \textit{HIFIS} (n=265) and \textit{Helmholtz} centres not involved in \textit{HIFIS} (n=77).}
	\label{fig:HIFIS_services_communication}
\end{figure}

In the following, we will present our results for the clusters \textit{Cloud Services} and \textit{Software Services} separately. 
\textit{Cloud Services}, on the one hand, provide services for the entire \textit{Helmholtz Association} and were, therefore, interested in a broader sample. 
\textit{Software Services}, on the other hand, focused mainly on respondents who were actively involved in software development during the past 12 months or were interested in improving their software development practices.