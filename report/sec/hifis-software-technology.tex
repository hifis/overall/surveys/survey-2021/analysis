% SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
% SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
% SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
% SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
% SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
% SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
%
% SPDX-License-Identifier: CC-BY-4.0

\paragraph{Introduction} \label{para:technology-intro}

The survey provides questions about the technologies participants were using
during the last twelve months.
We would like to gain insights into the technology stack employees of the
involved centres of the \textit{Helmholtz Association} are using.
These technologies were grouped into five categories:
(1) Version Control Systems,
(2) Software Project Management Platforms,
(3) Continuous Integration Platforms,
(4) Virtualisation tools,
(5) various other tools.
These categories correspond to the following five multiple choice questions:
\begin{enumerate}
  \item Did you use the following version control tools in the last twelve
  months?
  \item Did you use the following code hosting services in the last twelve
  months?
  \item Did you use the following automation tools or services in the last
  twelve months?
  \item Did you use the following containerisation tools or services in the
  last twelve months?
  \item Did you use the following tools or services in the last twelve
  months?
\end{enumerate}

\paragraph{Version Control Systems} \label{para:technology-vcs}

Version Control Systems (VCSs) are already a de-facto standard in the field of
software engineering.
They are perceived as an integral and requisite part in the process of software
development.
Several open-source software tools exist with varying ranges of popularity and
usage over time.
We wanted to know which VCSs were used by employees of the \textit{Helmholtz}
centres most often during the last twelve months.

Possible answer options were \textit{Git}, \textit{SVN}, and \textit{Mercurial}.
We chose these tools because they are best known in the software development
community.
The \textit{HIFIS} survey in 2020 revealed that \textit{Git} and \textit{SVN}
are currently the most widely used VCSs for software development in
\textit{Helmholtz}.
This has been underpinned by the answers given by the participants
(see figure \ref{fig:q015-vcs-per-options}).
In fact, 56 \% of of the participants are using \textit{Git} as VCS, while
only 15 \% use \textit{SVN} and 1 \% use \textit{Mercurial}.

\begin{figure}
	\centering
	\includegraphics[width=.96\textheight,angle=90]{fig/Q015-vcs-per-options}
	\caption{Usage of Version Control Systems}
	\label{fig:q015-vcs-per-options}
\end{figure}

Based on these results we were able to determine the fraction of participants
who used VCSs in the last twelve months (see figure \ref{fig:q015-vcs-overall}).
According to these numbers 59 \% of all participants use VCSs in their work.

\begin{figure}
	\centering
	\includegraphics[width=.75\textwidth]{fig/Q015-vcs-overall}
	\caption{Overall usage of Version Control Systems}
	\label{fig:q015-vcs-overall}
\end{figure}

Of course, these answers need to be grouped into an additional category that
determines which of the participants have software development skills and have
developed software within the last twelve months
(see figure \ref{fig:q015-vcs-swdevs}).
The resulting data split visualises that 81 \% of all participants who
identify themselves as software developers use VCSs.

\begin{figure}
	\centering
	\includegraphics[width=.75\textwidth]{fig/Q015-vcs-swdevs}
	\caption{Usage of Version Control Systems by developers and non-developers}
	\label{fig:q015-vcs-swdevs}
\end{figure}

In the last category we want to discuss the usage of VCSs based on the
distinction between working on a project as single developer or as part
of a team (see figure \ref{fig:q015-vcs-team}).
Here, we can see that as soon as software is developed by more than one
developer the fraction of participants who use VCSs increases from 72 \%
to 96 \%.

\begin{figure}
	\centering
	\includegraphics[width=.75\textwidth]{fig/Q015-vcs-team}
	\caption{Usage of Version Control Systems by individual and team developers}
	\label{fig:q015-vcs-team}
\end{figure}

\paragraph{Software Project Management Platforms} \label{para:technology-spm}

Software Project Management Platforms have become increasingly important in the
field of software engineering.
They are used as collaboration and project management platforms and offer
various features for collaboration and project management like projects, teams,
issue tracking, issue boards, milestone planning, code reviews, packaging,
continuous integration pipelines, and much more.

With regard to software development projects and in particular developers,
who work on open-source software, they offer a central place for establishing
and maintaining the whole development lifecycle.
These platforms define a common workflow for development teams and developers
and inherently demand the usage of Version Control Systems.
Both, Version Control Systems and Software Project Management Platforms, are
fundamental and essential prerequisites for sustainable software development.

In this survey we were interested in five different platforms
(see figure \ref{fig:q016-spm-per-options}):
\textit{GitHub}, \textit{GitLab}, \textit{BitBucket}, \textit{Redmine}, and
\textit{Gitea}.
Mostly used are \textit{GitLab} and \textit{GitHub} with 50 \% and 45 \%,
respectively.
\textit{BitBucket} and \textit{Redmine} are used with an equal frequency of
7 \%.
On the lower end is the usage of \textit{Gitea} denoted with 1 \% among the
survey participants.
Among those who do not use Software Project Management Platforms they are
either unknown or known but just not relevant to them.

\begin{figure}
	\centering
	\includegraphics[width=.96\textheight,angle=90]{fig/Q016-spm-per-options}
	\caption{Usage Software Project Management Platforms}
	\label{fig:q016-spm-per-options}
\end{figure}

Overall, the usage of Software Project Management Platforms accounts for about
63 \% (see figure \ref{fig:q016-spm-overall}) if we count answers given by
developers as well as non-developers.

\begin{figure}
	\centering
	\includegraphics[width=.75\textwidth]{fig/Q016-spm-overall}
	\caption{Overall usage of Software Project Management Platforms}
	\label{fig:q016-spm-overall}
\end{figure}

As we sorted the results by distinguishing between non-developers and
developers a different picture emerged (see figure \ref{fig:q016-spm-swdevs}).
Here, we can see that 84 \% of the developers use Software Project Management
Platforms and even those who say that they do not develop software 31 \% use
these platforms (which might be the case because they work together with
software developers that are part of their team or even use them in other
collaboration and project management contexts which are completely unrelated
to software development).

\begin{figure}
	\centering
	\includegraphics[width=.75\textwidth]{fig/Q016-spm-swdevs}
	\caption{Usage of Software Project Management Platforms by developers and
	         non-developers}
	\label{fig:q016-spm-swdevs}
\end{figure}

Similar insights can be gained by the distinction whether or not developers
develop their software in a team (see figure \ref{fig:q016-spm-team}).
As soon as people start working in teams of software developers we can observe
that they use Software Project Management Platforms significantly more often.
While 74 \% of individual developers use these services, 95 \% of team
developers use them to collaborate with other developers.
This emphasizes quite well how important these services are for the workflow
of software developers.

\begin{figure}
	\centering
	\includegraphics[width=.75\textwidth]{fig/Q016-spm-team}
	\caption{Usage of Software Project Management Platforms by individual and
	         team developers}
	\label{fig:q016-spm-team}
\end{figure}

This picture we just draw here in this paragraph is not unexpected if we take
into account the availability of these platforms.
There are public services like \textit{GitHub}, \textit{GitLab} and
\textit{BitBucket} that offer a certain feature set at no cost.
Of course, public code hosting platforms have their drawbacks.
One of which is data and code sovereignty.
In order to guarantee that data and code created by \textit{Helmholtz} centres
is still owned by \textit{Helmholtz} and hosted on \textit{Helmholtz}
infrastructure, many centres in the \textit{Helmholtz Association} offer their
own Software Project Management Platform like \textit{GitLab} to their
employees.
Additionally, the \textit{HIFIS} platform initiated to offer a \textit{GitLab}
service, the \textit{Helmholtz Codebase}, to all centres and employees of the
\textit{Helmholtz Association} that can be used to collaborate with external
partners as well.
By offering such a platform, centres are no longer forced to provide their own
code hosting service to their employees.
As pointed out above, one of the biggest advantages of this service is, that
the code is hosted by a \textit{Helmholtz} centre instead of external third
parties.
This guarantees the code sovereignty regarding research software developed by
researchers within \textit{Helmholtz}.
As a result outstanding and excellent research, regardless of the openness and
public accessibility of this research, can be conducted independently and is
protected inside infrastructure offered within \textit{Helmholtz}.

\paragraph{Continuous Integration Tools} \label{para:technology-ci}

Continuous Integration (CI) tools are another important building block in the
field of software engineering.
They are used to automate tasks like compiling software from code, generating
documentation, executing unit tests, calculating code metrics, and much more.
The name indicates already that these tools provide a way to continuously
integrate all parts of a software, in particular new features and bug fixes,
during the process of software development, so that software is always in a
state that is considered stable or at least executable.
Besides this original use case, most solutions are also suitable for automating
arbitrary tasks.

For this survey we asked participants which services they use most often
(see figure \ref{fig:q017-ci-per-options}):
\textit{Jenkins}, \textit{Travis CI}, \textit{Circle CI},
\textit{GitHub Actions}, \textit{GitLab CI (self-hosted)}, or
\textit{GitLab CI (gitlab.com)}.

\begin{figure}
	\centering
	\includegraphics[width=.96\textheight,angle=90]{fig/Q017-ci-per-options}
	\caption{Usage Continuous Integration tools}
	\label{fig:q017-ci-per-options}
\end{figure}

It becomes quite obvious, that these tools are far less known and used than
Version Control Systems and Software Project Management Platforms.
All but one service are used by less than 10 \% of the survey participants.
The exceptions are \textit{self-hosted GitLab CI} services that are provided by
particular \textit{Helmholtz} centres as well as the
\textit{self-hosted GitLab CI} service provided by the \textit{HIFIS} platform
that are used by 21 \% of the participants.

\begin{figure}
	\centering
	\includegraphics[width=.75\textwidth]{fig/Q017-ci-overall}
	\caption{Overall usage of Continuous Integration tools}
	\label{fig:q017-ci-overall}
\end{figure}

We expected the overall usage of the mentioned CI tools to be low
(see figure \ref{fig:q017-ci-overall}).
With only 28 \%  using CI tools over the past twelve months this expectation
was confirmed.

\begin{figure}
	\centering
	\includegraphics[width=.75\textwidth]{fig/Q017-ci-swdevs}
	\caption{Usage of Continuous Integration tools by developers and
	         non-developers}
	\label{fig:q017-ci-swdevs}
\end{figure}

The distinction between developers and non-developers is of particular
importance here (see figure \ref{fig:q017-ci-swdevs}).
Due to the fact that CI tools are rather tailored towards expert demands
people who do not develop software on a regular basis find it hard and
time-consuming to learn how to use such tools.
Having this in mind, it is no surprise that only 8 \% of the non-developers
stated to use these tools and even though they are acknowledged to be very
important to the work of developers only 42 \% of the them make use of CI.
From our point of view CI tools are one of the most promising technologies
in the field of software development today.
There are already powerful and established tools available and they should
belong to every developer's toolbox when writing software.
We have the strong conviction that the usage of CI tools has to increase in
order to be able to foster and strengthen sustainable, high-quality software
development in the \textit{Helmholtz Association}.

\begin{figure}
	\centering
	\includegraphics[width=.75\textwidth]{fig/Q017-ci-team}
	\caption{Usage of Continuous Integration tools by individual and
	         team developers}
	\label{fig:q017-ci-team}
\end{figure}

It is also interesting to compare the usage of CI tools by the group of
individual developers versus usage by the group of team developers
(see figure \ref{fig:q017-ci-team}).
There is a shift in the amount of how much CI tools are used if we compare
between individual and team developers.
32 \% of the individual developers use these services, while nearly twice as
much, 60 \% of the team developers, use them.
This means that these services become increasingly important to development
teams.

Due to the fact that we believe that Continuous Integration is a very important
practice, we would like to see this proportion twisted around in the upcoming
years.
It has been identified as one of the main objectives of the
\textit{Technology Services} working group of the \textit{HIFIS} platform.
We would like to support researchers by lowering the barriers to use these
services.
Not least, we are investing in compute resources and hardware that is able
to execute these Continuous Integration tasks and offer them to all
\textit{Helmholtz} at no cost and without any administrative barriers.

\paragraph{Virtualisation Tools} \label{para:technology-virt}

As soon as software is developed and expected to run within different
environments, stretching from local computers, local virtual machines,
local containers to remote servers, development teams start thinking about
how to guarantee consistent behaviour.
The usual way is to provide a single point of truth, realised through a single
environment which can be set up on all computers and servers using
virtualisation tools.
Yet, there is another good reason to use these tools which is reproducibility -
a topic that is particularly important for research software.
These environments are configured via declarative text files, called recipes,
that can be put under version control like any other code and can be shared
amongst all contributors.
This paradigm is called \textit{Infrastructure as Code}.
These environments can be created in a reproducible manner by each contributor
independently without the need to come up with an own environment.

Some of the more prominent tools are \textit{Vagrant}, \textit{Docker},
\textit{Singularity}, and \textit{Podman}.
We asked the participants whether they used the mentioned virtualisation tools
in the last twelve months (see figure \ref{fig:q018-virt-per-options}).
The application of these techniques and tools just recently started to emerge
within the field of software engineering.
Not so long ago these tasks were attributed solely to the operations team and
not the development team, but nowadays these borders become more and more
blurry.
Development teams and even separate DevOps teams (a Portmanteau of
"Developments" and "Operations") started to dedicate themselves to this topic
during the last years.
Because of its novelty our assumption was that these tools and techniques are
not as well-known as other technologies yet.
This was backed up by the responses of the participants.
Except for \textit{Docker}, which is used by 30 \% of the respondees, those
mentioned virtualisation tools are largely unknown and if they are known they
seem to be rather irrelevant to the group of responders.

\begin{figure}
	\centering
	\includegraphics[width=.96\textheight,angle=90]{fig/Q018-virt-per-options}
	\caption{Usage Virtualisation tools}
	\label{fig:q018-virt-per-options}
\end{figure}

The overall picture is almost the same.
Only 31 \% used virtualisation tools, while 69 \% did not
(see figure \ref{fig:q018-virt-overall}).

\begin{figure}
	\centering
	\includegraphics[width=.75\textwidth]{fig/Q018-virt-overall}
	\caption{Overall usage of Virtualisation tools}
	\label{fig:q018-virt-overall}
\end{figure}

Again, the distinction between developers and non-developers is important in
order to get a more informative impression on how the usage of these
technologies is distributed.
While 12 \% of the non-developers used virtualisation tools, which is not
a surprising outcome, only 44 \% of the developers said that they
made use of these tools (see figure \ref{fig:q018-virt-swdevs}).

\begin{figure}
	\centering
	\includegraphics[width=.75\textwidth]{fig/Q018-virt-swdevs}
	\caption{Usage of Virtualisation tools by developers and non-developers}
	\label{fig:q018-virt-swdevs}
\end{figure}

Of course we are also interested in the distribution of usage, when we
distinguish between individual and team developers
(see figure \ref{fig:q018-virt-team}).
These tools seem to be more important for developers that are part of a team.
35 \% of the individual developers and 59 \% of the team developers are using
virtualisation tools.
This reflects pretty well the necessity within development teams to provide
environments in which software can be run consistently and in reproducible
ways by all team members whilst offering more degrees of freedom.

\begin{figure}
	\centering
	\includegraphics[width=.75\textwidth]{fig/Q018-virt-team}
	\caption{Usage of Virtualisation tools by individual and team developers}
	\label{fig:q018-virt-team}
\end{figure}

\paragraph{Various Tools} \label{para:technology-mix}

Finally, we were particularly interested in the usage of the following tools
and services:
\textit{OpenStack}, \textit{JupyterHub}, \textit{Rocket.Chat},
\textit{Mattermost}, \textit{Nextcloud}, \textit{LimeSurvey},
\textit{ShareLaTex}, \textit{Jira}.

The most popular tools and services (in descending order) are
\textit{Nextcloud} (59 \%), \textit{Mattermost} (42 \%), and
\textit{JupyterHub} (20 \%) (see figure \ref{fig:q019-mix1-per-options} and
\ref{fig:q019-mix2-per-options}).
Except for \textit{Nextcloud} and \textit{Mattermost} a large amount of
participants said that those mentioned tools are unknown to them.

\begin{figure}
	\centering
	\includegraphics[width=.96\textheight,angle=90]{fig/Q019-mix1-per-options}
	\caption{Usage various tools}
	\label{fig:q019-mix1-per-options}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=.96\textheight,angle=90]{fig/Q019-mix2-per-options}
	\caption{Usage various tools}
	\label{fig:q019-mix2-per-options}
\end{figure}

\paragraph{Summary} \label{para:technology-summary}

In order to conclude this \textit{Technology} section we would like to
summarise our observations so far.
Our analysis of the survey data underpinned our assumption that for people
with a software development background it makes a significant difference
whether they collaborate within a team or act as a single contributor
on software they develop.
In all cases, from \textit{Version Control Systems},
\textit{Software Project Management Platforms},
\textit{Continuous Integration Tools} to \textit{Virtualisation Tools},
we were able to show the necessity for more advanced tools and services
in case of collaborative development within teams.
While the first two categories of tools and services are already widely
accepted as standard tools and best-practises, the latter two categories
of tools and services are rather under-represented and need significantly more
attention.
The \textit{HIFIS} platform and the \textit{Technology Services} working group
in particular already promote these tools and services in the research
communities.
We make them available to researchers and also offer support for becoming
familiar with these technologies.
We would like to actively contribute making the usage of these tools more
accessible and prominent, because it is our conviction, that they are mandatory
for more sustainable software development practises within the
\textit{Helmholtz} communities.
