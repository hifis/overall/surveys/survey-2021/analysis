# SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
# SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
#
# SPDX-License-Identifier: MIT

conditions:
  - ((Q006.NAOK != "A006"))
id: Q042
label: "Potential impact of malfunctioning"
questions:
  - answers:
      - id: A001
        label: "None"
        text:
          de-DE: "Es handelt sich nicht um missionskritische Software / Code"
          en-GB: "It is not mission-critical software / code"
      - id: A002
        label: "Negligible consequences"
        text:
          de-DE: "Vernachlässigbare Konsequenzen, die zu einer geringen
                  Verschlechterung der Mission ohne Risiken für Leben und
                  Umwelt führen können"
          en-GB: "Negligible consequences which could lead to a minor mission
                  degradation with no risks for life and environment"
      - id: A003
        label: "Major consequences"
        text:
          de-DE: "Große Konsequenzen, die zu einer erheblichen
                  Verschlechterung der Mission ohne Risiken für Leben und
                  Umwelt führen können"
          en-GB: "Major consequences which could lead to a major mission
                  degradation with no risks for life and environment"
      - id: A004
        label: "Critical consequences"
        text:
          de-DE: "Entscheidende Konsequenzen, die zu einem Missionsverlust
                  einschließlich vorübergehender einschränkender - aber nicht
                  lebensbedrohlicher - Verletzungen oder erheblicher
                  Umweltschäden führen können"
          en-GB: "Critical consequences which could lead to a loss of mission
                  including temporarily disabling but not life‐threatening
                  injuries or major detrimental environmental effects"
      - id: A005
        label: "Catastrophic consequences"
        text:
          de-DE: "Katastrophale Konsequenzen, die zu einem Missionsverlust
                  einschließlich des Verlusts des Lebens oder schwerwiegender
                  Umweltschäden führen können"
          en-GB: "Catastrophic consequences which could lead to a loss of
                  mission including loss of life or severe detrimental
                  environmental effects"
    datatype: str
    id: "_"
    label: "Potential impact of malfunctioning"
    mandatory: true
    text:
      de-DE: "Was sind die möglichen Auswirkungen einer Fehlfunktion der
              Software / des Codes?"
      en-GB: "What is the potential impact of a malfunction of the software /
              code?"
response_format: single choice
scale_level: ordinal
tags:
  - status quo
target_audiences:
  - developer
text:
  de-DE: "Was sind die möglichen Auswirkungen einer Fehlfunktion der Software
          / des Codes?"
  en-GB: "What is the potential impact of a malfunction of the software /
          code?"
wp_list:
  - software
