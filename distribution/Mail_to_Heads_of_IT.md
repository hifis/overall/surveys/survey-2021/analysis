<!--
SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ

SPDX-License-Identifier: CC-BY-4.0
-->

Liebe/r `<NAME>`,

die HIFIS-Plattform [1] hat unter anderem zum Ziel, alle Helmholtz-Zentren und -Forschungsbereiche mit einer flächendeckenden Infrastruktur für leicht zugängliche Cloud-Services [2], sowie Ausbildungsangeboten und Beratung für nachhaltige wissenschaftliche Softwareentwicklung [3] zu versorgen.

Hierfür fragen wir regelmäßig die Bedarfe in allen Zentren - insbesondere bei bisher noch nicht involvierten Personen - ab.
Bereits 2020 wurde hierfür eine erste Studie durchgeführt, deren Auswertungen hier zu finden sind [4].
Für 2021 möchten wir mit einer Folge-Studie aktuelle Entwicklungen im Bereich der Nutzung von Cloud Services und Softwareentwicklung erkennen, um unser Angebot darauf abstimmen zu können. 

Dafür benötigen wir eine repräsentative Abfrage der Nutzer:innen aller Zentren.
Daher wende ich mich mit folgender Bitte an Sie:

Können Sie bitte das folgende Schreiben an `<NUMBER>` zufällig ausgewählte Nutzer:innen Ihres Zentrums weiterleiten?
Die Zufallsauswahl sollte folgendermaßen erfolgen, um eine möglichst vergleichbare Stichprobenziehung zu erzielen:
Bitte weisen Sie jeder Person eine Zufallszahl zu, ordnen Sie die Zufallszahlen dann nach aufsteigender Reihenfolge und wählen Sie schließlich die ersten `<NUMBER>` Personen aus.
Es kommt uns sehr entgegen, wenn Sie die Mail innerhalb von 14 Tagen verteilen.

Hinweise zur Datenerhebung, -verarbeitung und -auswertung sowie eine Übersicht über die erhobenen Daten entnehmen Sie bitte dem beigefügten Antrag.

Bei Problemen, Verzögerungen oder Rückfragen wenden Sie sich bitte gerne an mich.

Mit freundlichen Grüßen,

Uwe Jandt




********** Mailvorschlag an die Teilnehmer:innen (deutsch/englisch) ***********


Liebe Kollegin, lieber Kollege, (*english version below*)

die Kollegen von HIFIS haben darum gebeten, diese Mail weiterzuleiten:

Die HIFIS-Plattform [1] hat zum Ziel, alle Helmholtz-Zentren und -Forschungsbereiche mit einer flächendeckenden Infrastruktur für leicht zugängliche Cloud-Services [2], sowie Ausbildungsangeboten und Beratung für nachhaltige wissenschaftliche Softwareentwicklung [3] zu versorgen.

Hierfür fragen wir regelmäßig die Bedarfe in allen Zentren - insbesondere bei bisher noch nicht involvierten Personen - ab.
Bereits 2020 wurde hierfür eine erste Studie durchgeführt, deren Auswertungen hier zu finden sind [4].
Für 2021 möchten wir mit einer Folge-Studie aktuelle Entwicklungen im Bereich der Nutzung von Cloud Services und wissenschaftlicher Softwareentwicklung erkennen, um unser Angebot darauf abstimmen zu können. 


Daher wenden wir uns an Sie mit der Bitte um Teilnahme an unserer Umfrage bis 9.Juni unter folgendem Link:

   `https://survey.pt-dlr.de/index.php?r=survey/index&sid=202105&lang=de`

Sie können die Umfrage gerne jederzeit speichern und zu einem späteren Zeitpunkt fortsetzen.
Bei Rückfragen oder Problemen melden Sie sich bitte gerne unter support@hifis.net. 

Vielen Dank im Voraus, viele Grüße und bleiben Sie gesund,

Ihr HIFIS-Team.

--

Dear colleague,

the HIFIS colleagues asked me to forward this mail to you:

The HIFIS platform [1] aims at providing easily accessible cloud services [2], as well as education & training on sustainable scientific software engineering [3] to all Helmholtz centres and scientific communities.

To this aim, we regularly ask for requirements and practices in all centres - especially of so far not involved persons.
In 2020, a first study had been conducted, with results being summarized here [4].
In 2021, we want to query current developments in cloud services usage and research software engineering, in order to adopt our service portfolio accordingly.

So, we kindly ask you to take part in our current survey until June 9th, the latest, at the following link:

   `https://survey.pt-dlr.de/index.php?r=survey/index&sid=202105&lang=en`

Note that you can save the survey's state and continue at a later point in time.
In case of questions or problems, don't hesitate to contact us at support@hifis.net.

Thank you very much in advance, all the best and stay healthy,

The HIFIS team.




[1] https://www.hifis.net  
[2] https://cloud.helmholtz.de/  
[3] https://software.hifis.net/services
[4] https://hifis.net/news/2021/03/26/survey-results

 
## Vorschlag Stichpobengröße

| Center | PJ   |  n  | t1 | t2 | t3 |
| :------| ---: | --: | -- | -- | -- |
| AWI    | 1067 | 282 | | | |
| CISPA  | _240_|_148_| | | |
| DESY   | 2127 | 325 | | | |
| DKFZ   | 2526 | 333 | | | |
| DLR    | 6011 | 361 | | | |
| DZNE   | 953  | 274 | | | |
| FZJ    | 3819 | 349 | | | |
| GEOMAR | 612  | 236 | | | |
| GFZ    | 816  | 261 | | | |
| GSI    | 1539 | 307 | | | |
| HMGU   | 2132 | 326 | | | |
| HZB    | 976  | 276 | | | |
| HZDR   | 1042 | 281 | | | |
| HZI    | 867  | 266 | | | |
| HZG    | 699  | 248 | | | |
| KIT    | 3461 | 346 | | | |
| MDC    | 1170 | 289 | | | |
| UFZ    | 980  | 276 | | | |
| HGF    | _110_| _86_| | | |