<!--
SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ

SPDX-License-Identifier: CC-BY-4.0
-->

Liebe/r `<NAME>`,

vor einigen Wochen schrieben wir Dir mit der Bitte um Verteilung der HIFIS-Survey. Vielen Dank, dass Ihr das in die Wege geleitet habt!

Inzwischen haben wir zwar insgesamt einige hundert Rückläufe, aber mehr ist wünschenswert für eine umfassende Auswertung. Daher möchten wir Dich/Euch bitten, wenn möglich, noch folgendes kurzes Erinnerungsschreiben an die Teilnehmerinnen und Teilnehmer zu verschicken.

Vielen Dank für jede Hilfe,

Uwe

********** Mailvorschlag an die Teilnehmer:innen (deutsch/englisch) ***********

Liebe Kollegin, lieber Kollege, (*English version below*)

die Kollegen von HIFIS haben darum gebeten, diese Mail weiterzuleiten.

Vor einigen Wochen wurde Ihnen der folgende Link zu einer Umfrage von HIFIS zugestellt:

* `https://survey.pt-dlr.de/index.php?r=survey/index&sid=202105&lang=de`

Die Teilnahme an der Umfrage ist freiwillig und anonym. 
Sie können die Umfrage gerne jederzeit speichern und zu einem späteren Zeitpunkt fortsetzen.

Wenn Sie an der Umfrage bereits teilgenommen haben, danken wir Ihnen für die Mithilfe!

**Falls Sie noch nicht teilgenommen haben, bitten wir Sie, dieses noch bis 5.7. nachzuholen.** 
Die Zeitdauer beträgt etwa 20-25 Minuten. 
Es lohnt sich! 
Bitte bedenken Sie, dass die Cloud- und Software-Services von HIFIS [1] nur dann auf für Sie relevante Themen zugeschnitten werden können, wenn wir von Ihnen Rückmeldung bekommen.

Bei Rückfragen oder Problemen melden Sie sich bitte gerne unter support@hifis.net. 

Vielen Dank und viele Grüße,

Ihr HIFIS-Team.

[1] https://hifis.net/services

--

Dear colleague,

the HIFIS colleagues asked me to forward this mail to you.

A few weeks ago, the following link was shared to you, inviting you to take part in a HIFIS survey:

* `https://survey.pt-dlr.de/index.php?r=survey/index&sid=202105&lang=en`

Participation in the survey is voluntary and anonymous. 
You can save the survey's state anytime and continue later.

If you took part already, we'd like to thank you very much for helping us!

**If have not yet taken part, we ask you to kindly do so until July 5.** 
Completing the survey takes around 20-25 minutes. 
It is worth the time! 
Please be aware that the cloud and software services of HIFIS [1] can only suit your needs if we get feedback on the topics that are relevant for you. 

In case of questions or problems, don't hesitate to contact us at support@hifis.net.

Thank you very much and all the best,

The HIFIS team.

[1] https://hifis.net/services

