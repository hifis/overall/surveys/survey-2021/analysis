<!--
SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ

SPDX-License-Identifier: CC-BY-4.0
-->

## Sample size

| Center | PJ   | Recommended sample size | t1  | t2  | t3  |
| :------| ---: | ----------------------: | --- | --- | --- |
| AWI    | 1067 | 282 | 16 | | |
| CISPA  | _240_|_148_| 0 | | |
| DESY   | 2127 | 325 | 0 | | |
| DKFZ   | 2526 | 333 | 2 | | |
| DLR    | 6011 | 361 | 68 | | |
| DZNE   | 953  | 274 | 0 | | |
| FZJ    | 3819 | 349 | 9 | | |
| GEOMAR | 612  | 236 | 27 | | |
| GFZ    | 816  | 261 | 8 | | |
| GSI    | 1539 | 307 | 0 | | |
| HGF    | _110_| _86_| 8 | | |
| HMGU   | 2132 | 326 | 5 | | |
| HZB    | 976  | 276 | 25 | | |
| HZDR   | 1042 | 281 | 21 | | |
| Hereon | 699  | 248 | 0 | | |
| HZI    | 867  | 266 | 10 | | |
| KIT    | 3461 | 346 | 16 | | |
| MDC    | 1170 | 289 | 7 | | |
| UFZ    | 980  | 276 | 2 | | |


