<!--
SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ

SPDX-License-Identifier: CC-BY-4.0
-->

## Teilnahmeinformationen

### Wer führt die Umfrage durch?
Wir, ein Team aus [HIFIS Cloud](https://hifis.net/team) und 
[HIFIS Software](https://software.hifis.net/team), führen diese Umfrage durch,
um mehr über Ihren beruflichen Alltag als Mitarbeiter:in der Helmholtz-
Gemeinschaft zu erfahren und wie HIFIS Sie bei Ihrer Forschung, 
Softwareentwicklung oder administrativen Tätigkeit unterstützen kann. 
Die [HIFIS-Plattform](https://www.hifis.net) hat unter anderem zum Ziel, 
alle Helmholtz-Zentren (AWI, CISPA, DESY, DKFZ, DLR, DZNE, FZJ, GEOMAR, GFZ, GSI, 
HMGU, HZB, HZDR, HZI, HZG, KIT, MDC, and UFZ) und -Forschungsbereiche mit einer 
flächendeckenden Infrastruktur für leicht zugängliche 
[Cloud-Services](https://cloud.helmholtz.de/) sowie 
[Ausbildungsangeboten](https://software.hifis.net/services/training) und 
[Beratung](https://software.hifis.net/services/consulting) 
für nachhaltige wissenschaftliche Softwareentwicklung zu versorgen.
Ihre Erfahrungen helfen uns dabei, unsere Angebote zu verbessern und zu einer 
exzellenten Helmholtz weiten IT-Infrastruktur beizutragen.

### Wie lange wird die Umfrage dauern?
Die Umfrage beinhaltet eine variable Anzahl an Fragen, wobei die Maximaldauer 
der Beantwortung ca. 20 Minuten beträgt.
Am besten erfolgt die Beantwortung ohne Unterbrechung, 
zum Zwischenspeichern Ihrer Antworten können Sie jedoch auch den 
`Später fortfahren` Button verwenden.
Die Umfrage kann jederzeit ohne Angabe von Gründen und ohne negative Konsequenzen 
abgebrochen werden, indem Sie den `Umfrage verlassen und Antworten löschen` 
Button in der oberen rechten Ecke betätigen.

### Was passiert mit den Daten?
Die Daten werden ohne Erfassung der IP-Adresse erhoben und auf einem 
DLR-internen Server gespeichert.
Es werden weder Kontaktdaten noch Daten bezüglich Ihres Namens, Geschlechts 
oder Alters erhoben. 
Datenanalyse und Publikation der Ergebnisse erfolgen anonymisiert in 
Form eines internen Berichts, wissenschaftlichen Beitrags oder Blog Posts.
Ein anonymisierter Datensatz wird gegebenenfalls zur Nachvollziehbarkeit der 
Ergebnisse auf einer entsprechenden Plattform veröffentlicht.

## Einwilligungserklärung
Ich habe die Datenschutzerklärung und Teilnahmeinformation gelesen und 
willige in die Teilnahme an der Umfrage und die Nutzung meiner Daten ein.

## Letzte Seite
Vielen Dank für Ihr Feedback!  
Wir bedanken uns herzlich, dass Sie sich die Zeit für die Teilnahme an dieser 
Umfrage genommen haben und damit einen wichtigen Beitrag zum Aufbau einer 
flächendeckenden, Helmholtz weiten Infrastruktur leisten. 
Wir hoffen, dass die bereitgestellten Cloud-Dienste und Angeboten zur 
nachhaltigen Softwareentwicklung Sie zukünftig bei Ihren Vorhaben bestmöglich 
unterstützen werden. 
Die Umfrage läuft voraussichtlich bis Ende Juni 
und wird dann anonymisiert ausgewertet. 
Ergebnisse der Auswertung werden unter anderem auf der [HIFIS Website](https://www.hifis.net/news) bereitgestellt.
Bei Problemen oder Rückfragen kontaktieren Sie uns gerne via support@hifis.net. 

## Participant information

### Who is conducting this survey?
We, a team of [HIFIS Cloud](https://hifis.net/team) 
and [HIFIS Software](https://software.hifis.net/team), 
are conducting this survey to learn more about your working routine as an 
employee within the Helmholtz Association and how HIFIS can support you in
your research, software development or administrative activities. 
The [HIFIS platform](https://www.hifis.net) aims at providing easily 
accessible [cloud services](https://cloud.helmholtz.de/), 
[education & training](https://software.hifis.net/services/training) on 
sustainable scientific software engineering, as well as 
[consulting services](https://software.hifis.net/services/consulting) 
for complex scientific problems to all 18 Helmholtz centres 
(AWI, CISPA, DESY, DKFZ, DLR, DZNE, FZJ, GEOMAR, GFZ, GSI, 
HMGU, HZB, HZDR, HZI, HZG, KIT, MDC, and UFZ) and scientific communities. 
Sharing your experiences will help us to improve our services and contribute to 
an excellent Helmholtz-wide information environment.

### How long will it take?
There is a variable number of questions in this survey and it will take max. 
20 minutes to complete.
We encourage you to complete the survey in one sitting but you can also use 
the `Resume later` button to save your responses.
The survey may be cancelled at any time without negative consequences by 
using the `Clear and Exit` button in the upper right corner.

### What will happen with the data?
Data will be collected without saving your IP address 
and then stored on a DLR-internal server.
We will not collect any data concerning your name, age, gender, or contact details. 
Responses will be analysed and results published anonymously in an internal or 
scientific report or a blog post.
An anonymised dataset might also be published on a designated platform to
ensure reproducibility of the results.

## Consent
I have read the data protection and participant information and consent to 
participating in this survey and the use of my data as described above.

## Last page
Thanks for your feedback!
We very much appreciate you taking the time to complete this survey and 
contributing to the development of a Helmholtz-wide information environment.
We hope that the cloud and software services provided by HIFIS will support
you and your future endeavours in the best possible way.
The survey will run until end of June and data then be analysed anonymously.
An overview of the results will be published on our [website](https://www.hifis.net/news).
If you experienced any difficulties or have any questions, 
please get in touch via support@hifis.net
please visit our website https://hifis.net/
