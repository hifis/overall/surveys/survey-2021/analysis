<!--
SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ

SPDX-License-Identifier: CC-BY-4.0
-->

# Survey Analysis 2021

This repository holds the metadata, as well as analysis and report pipeline of the [HIFIS Survey 2021](https://hifis.net/survey2021).

The raw data processed here included as submodule from the [data project](https://gitlab.hzdr.de/hifis/overall/surveys/survey-2021/data).

## Report and Graphs
* [**Browse** pdf report](https://gitlab.hzdr.de/hifis/overall/surveys/survey-2021/analysis/-/jobs/artifacts/master/file/report/hifis-survey-2021.pdf?job=build:graphsreport)
* [**Download** pdf report](https://gitlab.hzdr.de/hifis/overall/surveys/survey-2021/analysis/-/jobs/artifacts/master/raw/report/hifis-survey-2021.pdf?job=build:graphsreport)
* [Browse graphs](https://gitlab.hzdr.de/hifis/surveys/survey-2021/analysis/-/jobs/artifacts/master/browse/report/fig/?job=build:graphsreport)
* [Download pdf with all graphs](https://gitlab.hzdr.de/hifis/overall/surveys/survey-2021/analysis/-/jobs/artifacts/master/raw/report/fig/_allplots.pdf?job=build:graphsreport)
