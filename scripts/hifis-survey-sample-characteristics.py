# SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
# SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
#
# SPDX-License-Identifier: MIT

from typing import Dict

import modules.descriptive_stats as stats
from hifis_surveyval.data_container import DataContainer, Question
from hifis_surveyval.hifis_surveyval import HIFISSurveyval
from numpy import array
from pandas import DataFrame


def run(hifis_surveyval: HIFISSurveyval, data: DataContainer):
    """
    Compute descriptive statistics for general questions to describe the
    sample of Helmholtz employees who participated in the HIFIS survey 2021
    """
    # Output path
    path: str = str(hifis_surveyval.settings.ANALYSIS_OUTPUT_PATH) + "\\"

    # Relevant questions
    questions: Dict = {
        "Q001/_": _center_affiliation,
        "Q002/_": _years_of_employment,
        "Q003/_": _scope_of_work,
        "Q004/_": _helmholtz_research_field,
        "Q005/_": _active_software_development,
        "Q006/_": _software_development_level,
        "Q007/_": _software_development_time
    }

    # Create statistics table containing total number of cases
    sample_characteristics_tbl: DataFrame = stats.get_sample_size(
        data.data_frame_for_ids(requested_ids=list(questions.keys()))
    )

    # Run the respective analysis for each of the specified questions and
    # save descriptive statistics in joint table
    for question_id, analysis in questions.items():
        sample_characteristics_tbl = analysis(
            hifis_surveyval=hifis_surveyval,
            data=data,
            question_id=question_id,
            stats_tbl=sample_characteristics_tbl
        )
    sample_characteristics_tbl.rename({"mean": "M", "std": "SD"}, inplace=True)

    # Display and export resulting table of sample characteristics
    print("\nHIFIS survey sample characteristics:\n{}\n"
          .format(sample_characteristics_tbl))
    sample_characteristics_tbl.to_latex(
        buf=path + "sample-characteristics.tex",
        header=True,
        index=True,
        na_rep="",
        float_format="{:0.2f}".format,
        column_format="@{}p{0.9\\textwidth}r@{}r@{}",
        caption="Sample characteristics for HIFIS Survey 2021",
        label="tab:sample_stats",
        position="htbp",
        longtable=False
    )


def _center_affiliation(hifis_surveyval: HIFISSurveyval, data: DataContainer,
                        question_id: str, stats_tbl: DataFrame) -> DataFrame:
    # Get data on Helmholtz centre affiliation
    question: Question = data.question_for_id(question_id)

    # Distribution of respondents across Helmholtz centres
    [_, question_freq_rel, _] = stats.nominal_single_choice(question)
    question_freq_rel.drop(index="Other", inplace=True)
    question_freq_rel.columns = ["Respondents"]

    # Distribution of employees across Helmholtz centres
    n_per_center = array([1336, 265, 2766, 3152, 10095, 1219, 6791, 764,
                          988, 1526, 110, 2441, 1102, 1409, 1119, 888,
                          4154, 1253, 1214])
    n_helmholtz: int = sum(n_per_center)
    question_freq_rel.insert(
        loc=0, column="Employees", value=n_per_center / n_helmholtz * 100
    )

    # Plot distributions as bar plot
    stats.bar_plot(hifis_surveyval=hifis_surveyval, question=question,
                   stats_to_plot=question_freq_rel, array_question=True,
                   stacked=False, ylim=(0, 30))

    return stats_tbl


def _years_of_employment(hifis_surveyval: HIFISSurveyval, data: DataContainer,
                         question_id: str, stats_tbl: DataFrame) -> DataFrame:
    # Get data on years of employment
    question: Question = data.question_for_id(question_id)

    # Relative frequencies of observations
    [n, question_freq_rel, unit] = stats.nominal_single_choice(question)

    # Add question statistics to table
    stats_tbl = stats.add_to_table(
        tbl=stats_tbl,
        question=question,
        count=n,
        stats=question_freq_rel,
        unit=unit,
        compact=False
    )

    return stats_tbl


def _scope_of_work(hifis_surveyval: HIFISSurveyval, data: DataContainer,
                   question_id: str, stats_tbl: DataFrame) -> DataFrame:
    # Get data on personnel group
    question: Question = data.question_for_id(question_id)

    # Relative frequencies of observations
    [n, question_freq_rel, unit] = stats.nominal_single_choice(question)

    # Add question statistics to table
    stats_tbl = stats.add_to_table(
        tbl=stats_tbl,
        question=question,
        count=n,
        stats=question_freq_rel,
        unit=unit,
        compact=False
    )

    return stats_tbl


def _helmholtz_research_field(hifis_surveyval: HIFISSurveyval,
                              data: DataContainer, question_id: str,
                              stats_tbl: DataFrame) -> DataFrame:
    # Get data on research field
    question: Question = data.question_for_id(question_id)

    # Relative frequencies of observations
    [n, question_freq_rel, unit] = stats.nominal_single_choice(question)

    # Add question statistics to table
    stats_tbl = stats.add_to_table(
        tbl=stats_tbl,
        question=question,
        count=n,
        stats=question_freq_rel,
        unit=unit,
        compact=False
    )
    
    return stats_tbl


def _active_software_development(hifis_surveyval: HIFISSurveyval,
                                 data: DataContainer, question_id: str,
                                 stats_tbl: DataFrame) -> DataFrame:
    # Get data on software development activity within the past 12 months
    question: Question = data.question_for_id(question_id)

    # Relative frequencies of observations
    [n, question_freq_rel, unit] = stats.nominal_single_choice(question)

    # Add question statistics to table
    stats_tbl = stats.add_to_table(
        tbl=stats_tbl,
        question=question,
        count=n,
        stats=question_freq_rel,
        unit=unit,
        compact=False
    )

    return stats_tbl


def _software_development_level(hifis_surveyval: HIFISSurveyval,
                                data: DataContainer, question_id: str,
                                stats_tbl: DataFrame) -> DataFrame:
    # Get data on software development level
    question: Question = data.question_for_id(question_id)

    # Relative frequencies of observations
    [n, question_freq_rel, unit] = stats.nominal_single_choice(question)

    # Add question statistics to table
    stats_tbl = stats.add_to_table(
        tbl=stats_tbl,
        question=question,
        count=n,
        stats=question_freq_rel,
        unit=unit,
        compact=False
    )

    return stats_tbl


def _software_development_time(hifis_surveyval: HIFISSurveyval,
                               data: DataContainer, question_id: str,
                               stats_tbl: DataFrame) -> DataFrame:
    # Get data on percentage of working time spent on coding and
    question: Question = data.question_for_id(question_id)

    # Descriptive statistics for numeric variable
    [n, question_summary_stats, unit] = stats.scale_single_choice(
        question, custom_stats=["mean", "std"]
    )

    # Add question statistics to table
    stats_tbl = stats.add_to_table(
        tbl=stats_tbl,
        question=question,
        count=n,
        stats=question_summary_stats,
        unit=unit,
        compact=False
    )

    return stats_tbl
