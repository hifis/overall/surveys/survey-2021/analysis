# SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
# SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
#
# SPDX-License-Identifier: MIT

from pandas import DataFrame, Series
from hifis_surveyval.core import util
from hifis_surveyval.data_container import DataContainer
from hifis_surveyval.hifis_surveyval import HIFISSurveyval
from hifis_surveyval.models.question import Question
from hifis_surveyval.models.question_collection import QuestionCollection

# Global variable
SHOW_PLOTS = True
scale = 100
q_box_list = ["Q028","Q029"]
q_img_exceptions = ["all","all-rel"]
def div(value, denominator):
    return value/denominator


def q25(q_id: str, hfs: HIFISSurveyval, data: DataContainer, parameters: dict):
    # Variable declaration and init
    q_collection: QuestionCollection = data.collection_for_id(q_id)
    q_data: DataFrame             = q_collection.as_data_frame()
    question_text: str            = q_collection.text("en-GB")
    title: str                    = f'{q_id} : {question_text}'
    print_data: list              = []
    add_row: dict                 = {}
    answer_options                = {question.full_id: question.label for question in q_collection.questions}

    # removing all non answered columns
    q_data = q_data.dropna(axis=0, how='all', thresh=None, subset=None, inplace=False)

    # Definition of the joining series
    hifis_series: Series = data.question_for_id('V002/_').as_series()
    team_series:  Series = data.question_for_id('V003/_').as_series()

    # Intersecting all datasets => filters the cross section
    q_data_joined = q_data.join(hifis_series)
    q_data_joined = q_data_joined.join(team_series)

    # remove other
    if f'{q_id}/other' in q_data_joined.columns:
        q_data_joined = q_data_joined.drop(f"{q_id}/other", axis=1)

    if f'{q_id}/other' in q_data.columns:
        q_data = q_data.drop(f"{q_id}/other", axis=1)
    # ======================================================== #
    # == Section A: analysis of grouped datasets hifis/team == #
    users = {'hifis': { 'Yes': 0, 'No': 0}, 'team': { 'Yes': 0, 'No': 0}, 'total': 0}
    users['hifis']['Yes'] = len(q_data_joined[q_data_joined['V002/_'] == 'Yes'])
    users['hifis']['No']  = len(q_data_joined[q_data_joined['V002/_'] == 'No'])
    users['team']['Yes']  = len(q_data_joined[q_data_joined['V003/_'] == 'Yes'])
    users['team']['No']   = len(q_data_joined[q_data_joined['V003/_'] == 'No'])
    users['total']        = len(q_data_joined)
    # -------------------------------------------------------- #

    # -- Sums of answers in the repective groups -- #
    answers = {'hifis': {'Yes': 0, 'No': 0}, 'team': {'Yes': 0, 'No': 0}, 'total':0}
    for quest in q_data.keys():
        # Skip not analyzable survey options
        if quest == f"{q_id}/other":
            continue
        answers['hifis']['Yes'] += len(q_data_joined[(q_data_joined[quest] == True)
                                         & (q_data_joined['V002/_'] == 'Yes')])
        answers['hifis']['No']  += len(q_data_joined[(q_data_joined[quest] == True)
                                         & (q_data_joined['V002/_'] == 'No')])
        answers['team']['Yes']  += len(q_data_joined[(q_data_joined[quest] == True)
                                         & (q_data_joined['V003/_'] == 'Yes')])
        answers['team']['No']   += len(q_data_joined[(q_data_joined[quest] == True)
                                         & (q_data_joined['V003/_'] == 'No')])
    # --------------------------------------------- #

    # -- Generation of the relative plot data -- #
    tmp_hifis        = []
    tmp_team         = []
    tmp_freq_hifis   = DataFrame()
    tmp_freq_team    = DataFrame()
    freq_overall_abs = DataFrame()
    freq_overall_rel = DataFrame()


    for list_idx in range(len(parameters['q_list'])):
        for j,quest in enumerate(parameters['q_list'][list_idx]):
            # Skip not analyzable specific survey options
            if quest not in q_img_exceptions:
                # set new complite quest id
                quest =  f"{q_id}/{quest}"

                # loading specific answer set, eq. Q025/SQ000
                q_series: Series = data.question_for_id(quest).as_series()
                
                # combining base and reference series to a dataframe
                hifis_filtered: DataFrame = \
                    util.filter_and_group_series(base_data=q_series, group_by=hifis_series.dropna())
                team_filtered: DataFrame = \
                    util.filter_and_group_series(base_data=q_series, group_by=team_series.dropna())

                # PART 1: HIFIS/Non-HIFIS
                freq_hifis: DataFrame = util.dataframe_value_counts(
                    dataframe=hifis_filtered,
                    relative_values=False,
                    drop_nans=True,
                )

                # relative frequencies based users
                for idx in freq_hifis.index:
                    freq_hifis.loc[idx, 'Yes'] = freq_hifis.loc[idx, 'Yes'] / users['hifis']['Yes'] * scale
                    freq_hifis.loc[idx, 'No'] = freq_hifis.loc[idx, 'No'] / users['hifis']['No'] * scale

                # PART 2: Team/Single dev 
                freq_team: DataFrame = util.dataframe_value_counts(
                    dataframe=team_filtered,
                    relative_values=False,
                    drop_nans=True,
                )

                # relative frequencies based users
                for idx in freq_team.index:
                    freq_team.loc[idx, 'Yes'] = freq_team.loc[idx, 'Yes'] / users['team']['Yes'] * scale
                    freq_team.loc[idx, 'No'] = freq_team.loc[idx, 'No'] / users['team']['No'] * scale

                # PART 3: renaming 
                # rename the axis and title to the corresponding question tag
                for idx in range(len(q_data.keys())):
                    if quest == q_collection.questions[idx].full_id:
                        freq_team  = freq_team.rename({True: q_collection.questions[idx].label})
                        freq_hifis = freq_hifis.rename({True: q_collection.questions[idx].label})

                # rename the legend to the corresponding question tag
                freq_hifis = freq_hifis.rename(columns={'No': 'Non-HIFIS', 'Yes': 'HIFIS'})
                freq_team = freq_team.rename(columns={'No': 'Single dev.', 'Yes': 'Team'})

                tmp_hifis.append(freq_hifis.squeeze())
                tmp_team.append(freq_team.squeeze())

            elif quest in q_img_exceptions:
                if quest == 'all':
                    freq_overall_abs = util.dataframe_value_counts(
                        dataframe=q_data,
                        relative_values=False,
                        drop_nans=True
                    )

                    # relative frequencies based users
                    freq_overall_abs = freq_overall_abs.apply(
                        div, args=([[users['total']/scale] * len(freq_overall_abs.iloc[:])])
                    )

                    # map xtic
                    freq_overall_abs.rename(mapper=answer_options, axis='columns', inplace=True)


                if quest == 'all-rel':
                    freq_overall_rel = util.dataframe_value_counts(
                        dataframe=q_data,
                        relative_values=True,
                        drop_nans=True
                    )

                    # Scale to percent
                    for col in freq_overall_rel.columns:
                        freq_overall_rel[col] = freq_overall_rel[col] / sum(freq_overall_rel[col]) * scale

                    # map xtic
                    freq_overall_rel = freq_overall_rel.rename(mapper=answer_options, axis='columns')

        # generating the printable dataframes
        groups = {'hifis': tmp_hifis, 'team': tmp_team, 'all': freq_overall_abs, 'all-rel': freq_overall_rel }

        tmp_sum_freq = {}
        for group_name in groups:
            if parameters['q_list'][list_idx][0] not in q_img_exceptions:
                # which group images should be included ['hifis','team']
                if (group_name in parameters['q_include'][list_idx]) and (group_name not in q_img_exceptions):

                    if isinstance(groups[group_name][0],DataFrame):
                        tmp_freq = groups[group_name][0]
                        tmp_freq = tmp_freq.T
                    else:
                        tmp_freq = DataFrame(data=[groups[group_name][0]])
                        tmp_freq = tmp_freq.T

                    for k in range(1,len(groups[group_name])):
                        tmp_freq = tmp_freq.join(groups[group_name][k])

                    tmp_sum_freq[group_name] = tmp_freq
        # add dict
        if tmp_sum_freq:
            print_data.append(tmp_sum_freq)
            # reset temporary variables
            tmp_hifis = []
            tmp_team  = []

        for group_name in groups:
            if group_name in parameters['q_list'][list_idx]:
                dataframe_in_list: DataFrame = groups[group_name]
                if len(dataframe_in_list.index.values)==1:
                    dataframe_in_list = dataframe_in_list.T.sort_values(by=dataframe_in_list.index.values[0], ascending=False)
                    dataframe_in_list = dataframe_in_list.T
                print_data.append({group_name:dataframe_in_list})

    # rename rawdata for box plots
    q_data.rename(mapper=answer_options, axis='columns', inplace=True)
    if SHOW_PLOTS is True:
        for print_idx in range(len(print_data)):
            for plot_idx,plot_label in enumerate(print_data[print_idx].keys()):
                type_label = parameters['plotstyle'][print_idx][plot_idx]
                file_name =f"consulting_{q_id}-{plot_label}-{type_label}-list{print_idx:02d}"
                data = print_data[print_idx][plot_label]
                if parameters['q_list'][print_idx][0] == 'all': #in q_img_exceptions:
                    show_legend = False
                else:
                    show_legend = True
                if type_label == 'bar':
                    hfs.plotter.plot_bar_chart(
                        data_frame=data.T,
                        plot_file_name=file_name,
                        x_label_rotation=parameters['xtic_rot'][print_idx][plot_idx],
                        y_axis_label=parameters['ylabel'][print_idx][plot_idx],
                        round_value_labels_to_decimals=1,
                        show_legend=show_legend,
                        show_value_labels=parameters['barlabels'][print_idx][plot_idx],
                        figure_size=parameters['fig_size'][print_idx][plot_idx],
                        ylim=(parameters['ylimit'][print_idx][plot_idx])
                    )
                elif type_label == 'box':
                    hfs.plotter.plot_box_chart(
                        data_frame=q_data,
                        plot_file_name=file_name,
                        x_label_rotation=parameters['xtic_rot'][print_idx][plot_idx],
                        y_axis_label=parameters['ylabel'][print_idx][plot_idx],
                        figure_size=parameters['fig_size'][print_idx][plot_idx],
                        box_face_color="#669CC6"
                    )

# =================== #
# == Program Start == #
# =================== #

PARAMETERS = {
    "Q025": {
             'q_list':   [ ['SQ006','SQ007','SQ008'], ['SQ006','SQ007','SQ008','SQ013','SQ011'], ['all']            ],
             'q_include':[ ['hifis']      , ['team']                                         ],
             'fig_size': [ [(2.8,4)]             , [(4.8,4)]                , [(6,4)]               ],
             'ylimit':   [ [(0,45)]              , [(0,45)]                 , [(0,45)]              ],
             'xtic_rot': [ [45]                  , [45]                     , [45]                  ],
             'plotstyle':[ ['bar']               , ['bar']                  , ['bar']               ],
             'barlabels':[ [False]               , [False]                  , [True]                ],
             'ylabel'   :[ ['User answers in \%'] , ['User answers in \%']    , ['User answers in \%'] ]
            },
    "Q026": {
             'q_list':   [ ['SQ004']                , ['all']               ],
             'q_include':[ ['hifis']                                        ],
             'fig_size': [ [(2.55,2.5)]              , [(6,5)]               ],
             'ylimit':   [ [(0,100)]                 , [(0,85)]              ],
             'xtic_rot': [ [0]                      , [45]                  ],
             'plotstyle':[ ['bar']                  , ['bar']               ],
             'barlabels':[ [False]                   , [True]                ],
             'ylabel'   :[ ['User answers in \%']    , ['User answers in \%'] ]
            },
    "Q027": {
             'q_list':   [ ['SQ010']                , ['all-rel']           ],
             'q_include':[ ['hifis']                                        ],
             'fig_size': [ [(6.10,3.5)]             , [(11,5)]              ],
             'ylimit':   [ [(0,60)]                 , [(0,65)]              ],
             'xtic_rot': [ [45]                        , [45]               ],
             'plotstyle':[ ['bar']                  , ['bar']               ],
             'barlabels':[ [True]                   , [False]               ],
             'ylabel'   :[ ['User answers in \%']    , ['Relative answers for each category in \%'] ]
            },
    "Q028": {
             'q_list':   [ ['all-rel']                                      ],
             'q_include':[                                                  ],
             'fig_size': [ [(11,5)]                                         ],
             'ylimit':   [ [(0,65)]                                         ],
             'xtic_rot': [ [45]                                             ],
             'plotstyle':[ ['bar']                                          ],
             'barlabels':[ [False]                                          ],
             'ylabel'   :[ ['Relative answers for each category in \%']      ]
            },
    "Q029": {
             'q_list':   [ ['all']                                          ],
             'q_include':[                                                  ],
             'fig_size': [ [(4,4)]                                          ],
             'ylimit':   [ [(0,45)]                                         ],
             'xtic_rot': [ [45]                                             ],
             'plotstyle':[ ['box']                                          ],
             'barlabels':[ [True]                                           ],
             'ylabel'   :[ ['Answer']                                       ]
            }
}

QUESTIONS  = {"Q025": q25, "Q026": q25, "Q027": q25, "Q028": q25 , "Q029": q25}

def run(hifis_surveyval: HIFISSurveyval, data: DataContainer):
    for q_id, func in QUESTIONS.items():
        # extract question data
        func(q_id, hifis_surveyval, data, parameters=PARAMETERS[q_id])
    print('<-- DONE -->')
