# SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
# SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
#
# SPDX-License-Identifier: MIT

from typing import Dict, Set

import modules.descriptive_stats as stats
from hifis_surveyval.core import util
from pandas import DataFrame
from numpy import nan
from hifis_surveyval.data_container import DataContainer, Question, \
    QuestionCollection
from hifis_surveyval.hifis_surveyval import HIFISSurveyval


def run(hifis_surveyval: HIFISSurveyval, data: DataContainer):
    """
    Compute descriptive statistics for general questions regarding HIFIS
    services and communication.
    """
    # Relevant questions and corresponding analysis
    questions: Dict = {
        "Q035/_": _hifis_services_used,
        "Q036": _language_preference,
        "Q037": _communication_channels
    }

    # Create statistics table containing total number of cases
    hifis_services_sample: DataFrame = data.data_frame_for_ids(
            requested_ids=list(questions.keys())
        ).dropna(how="all")
    hifis_services_tbl: DataFrame = \
        stats.get_sample_size(hifis_services_sample)

    # Exclude cases from analysis who did not answer any community question
    incomplete_cases: Set = set(data.data_frame_for_ids(
        requested_ids=data.question_collection_ids
    ).index).difference(set(hifis_services_sample.index))

    # Run the respective analysis for each of the specified questions
    # and save descriptive statistics in joint table
    for question_id, analysis in questions.items():
        hifis_services_tbl = analysis(
            hifis_surveyval=hifis_surveyval,
            data=data,
            question_id=question_id,
            stats_tbl=hifis_services_tbl,
            cases=incomplete_cases
        )

    # Display and export resulting table of sample characteristics
    print("\nHIFIS survey community:\n{}\n"
          .format(hifis_services_tbl))


def _hifis_services_used(hifis_surveyval: HIFISSurveyval, data: DataContainer,
                         question_id: str, stats_tbl: DataFrame, cases: Set) \
        -> DataFrame:
    # Get data on use of HIFIS services
    question: Question = data.question_for_id(question_id)
    question.remove_answers(cases)

    # Relative frequencies of observations
    [n, question_freq_rel, unit] = stats.nominal_single_choice(question)

    # Add question statistics to table
    stats_tbl = stats.add_to_table(
         tbl=stats_tbl,
         question=question,
         count=n,
         stats=question_freq_rel,
         unit=unit,
         compact=False
    )

    # Grouping variable
    grouping_id: str = "V002/_"
    grouping: Question = data.question_for_id(grouping_id)

    # Group data by HIFIS vs. Non-HIFIS
    grouped_data: DataFrame = util.filter_and_group_series(
        question.as_series(), grouping.as_series().dropna()
    )

    # Relative frequencies of observations
    question_freq_rel["HIFIS"] = grouped_data["Yes"]\
                                     .value_counts(normalize=True) * 100
    question_freq_rel["Non-HIFIS"] = grouped_data["No"]\
                                         .value_counts(normalize=True) * 100

    # Plot use of HIFIS services for HIFIS vs. Non-HIFIS
    stats.grouped_bar_plot(
        hifis_surveyval=hifis_surveyval,
        stats_to_plot=question_freq_rel[["HIFIS", "Non-HIFIS"]],
        question=question,
        grouping=grouping
    )

    return stats_tbl


def _language_preference(hifis_surveyval: HIFISSurveyval, data: DataContainer,
                         question_id: str, stats_tbl: DataFrame, cases: Set) \
        -> DataFrame:
    # Get data on preferred language for different services
    question: QuestionCollection = data.collection_for_id(question_id)
    question.remove_answers(cases)

    # Relative frequencies of observations
    [n, question_freq_rel, unit] = stats.nominal_array(question)

    # Add question statistics to table
    stats_tbl = stats.add_to_table(
        tbl=stats_tbl,
        question=question,
        count=n,
        stats=question_freq_rel,
        unit=unit,
        compact=False
    )

    # Rearrange statistics data frame for plotting
    stats_to_plot = DataFrame()
    stats_to_plot = stats_to_plot.append(question_freq_rel[1:4].T)
    stats_to_plot = stats_to_plot.append(question_freq_rel[5:8].T)
    stats_to_plot = stats_to_plot.append(question_freq_rel[9:12].T)
    stats_to_plot = stats_to_plot.append(question_freq_rel[13:16].T)
    stats_to_plot = stats_to_plot.append(question_freq_rel[17:].T)
    stats_to_plot.index = question_freq_rel.index.values[[0, 4, 8, 12, 16]]

    # Plot preferred language for each HIFIS software service
    stats.bar_plot(
        hifis_surveyval=hifis_surveyval,
        stats_to_plot=stats_to_plot.astype(float, errors="raise"),
        question=question,
        array_question=True
    )

    return stats_tbl


def _communication_channels(hifis_surveyval: HIFISSurveyval,
                            data: DataContainer, question_id: str,
                            stats_tbl: DataFrame, cases: Set) \
        -> DataFrame:
    # Get data on preferred communication channels for HIFIS services updates
    question: QuestionCollection = data.collection_for_id(question_id)
    question.remove_answers(cases)

    # Relative frequencies of observations
    [n, question_freq_rel, unit] = stats.nominal_multiple_choice(question)

    # Add question statistics to table
    stats_tbl = stats.add_to_table(
        tbl=stats_tbl,
        question=question,
        count=n,
        stats=question_freq_rel,
        unit=unit,
        compact=False
    )

    # Plot preferred communication channels
    stats.bar_plot(
        hifis_surveyval=hifis_surveyval,
        stats_to_plot=question_freq_rel.astype(float, errors="raise"),
        question=question
    )

    # Grouping variable
    grouping_id: str = "V002/_"
    grouping: Question = data.question_for_id(grouping_id)

    # Group data by HIFIS vs. Non-HIFIS
    grouped_data: DataFrame = question.as_data_frame()\
        .groupby(grouping.as_series())\
        .sum()\
        .divide(grouping.as_series().value_counts(), axis="index") * 100


    # Replace sub-question ids with sub-question labels as column header
    sub_questions: Dict = {
        sub_question.full_id: sub_question.label for sub_question in
        question._questions.values()
    }
    grouped_data.rename(columns=sub_questions,
                        index={"No": "Non-HIFIS", "Yes": "HIFIS"},
                        inplace=True)

    # Plot preferred communication channels grouped by HIFIS vs. Non-HIFIS
    stats.grouped_bar_plot(
        hifis_surveyval=hifis_surveyval,
        question=question,
        grouping=grouping,
        stats_to_plot=grouped_data.T
    )

    return stats_tbl
