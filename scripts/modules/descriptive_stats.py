# SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
# SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
#
# SPDX-License-Identifier: MIT

from textwrap import wrap
from typing import List, Dict

import matplotlib.pyplot as plt
from hifis_surveyval.core import util
from hifis_surveyval.data_container import Question, QuestionCollection
from hifis_surveyval.hifis_surveyval import HIFISSurveyval
from numpy import array
from pandas import DataFrame, Series, set_option
from wordcloud import WordCloud


# Settings for proper displaying (e.g. question text will be cut off otherwise)
set_option("display.max_columns", None)
set_option("display.max_rows", None)
set_option("display.width", None)
set_option("display.max_colwidth", None)


def get_sample_size(sample: DataFrame = DataFrame()) -> DataFrame:
    """Return a table containing number of cases"""
    # Count rows to determine overall sample size
    n: Series = Series(sample.shape[0], index=["Total"])

    # Convert to data frame
    n_df: DataFrame = n.to_frame(name="n")

    return n_df


def nominal_single_choice(question: Question) -> [int, DataFrame, str]:
    """Return a table containing relative frequencies for a categorical
    variable if variable is a single choice question"""
    # Get data for question excluding missing values
    data: Series = question.as_series().dropna()

    # Count responses
    n_valid_responses: int = data.shape[0]

    # Get response labels for each response IDs
    response_options: List[str] = [
        answer_option.label for answer_option in
        question._answer_options.values() if not
        answer_option.label == "No answer"
    ]

    # Calculate frequencies relative to the number of cases
    stats: DataFrame = util.dataframe_value_counts(
        DataFrame(data), relative_values=True
    ) * 100

    if response_options:
        # Include all response options in the statistics, also those not chosen
        stats = stats.reindex(response_options, fill_value=0)

    stats.columns = [""]

    return n_valid_responses, stats, "%"


def nominal_multiple_choice(question: QuestionCollection, **kwargs) \
        -> [int, DataFrame, str]:
    """Return a table containing relative frequencies for a categorical
    variable if variable is a multiple choice question"""
    # By default frequencies are computed relative to the number of cases
    relative_to_cases: bool = kwargs.get("relative_to_cases", True)

    # Exclude non-responders to distinguish from non-selected
    data: DataFrame = question.as_data_frame().dropna(how="all")

    # Count responses after exclusion of non-responses
    n_valid_responses: int = data.shape[0]

    # Get response labels for response IDs
    sub_questions: Dict = {
        sub_question.full_id: sub_question.label for sub_question in
        question._questions.values() if
        not sub_question.full_id.endswith("comment")
    }

    # Exclude free text fields from analysis
    data = data[sub_questions.keys()]

    # By default frequencies are computed relative to the number of cases
    if relative_to_cases:
        # compute relative frequencies
        stats: Series = data.sum(axis=0) / n_valid_responses * 100
    # But it is also possible to compute frequencies relative to the number of
    # votes, indicating the relative importance of each aspect
    else:
        # Replace with sub-question label where sub-question was chosen
        for sub_question in data.columns.values:
            data.loc[:, sub_question].where(
                data.loc[:, sub_question].isna(), sub_question, inplace=True
            )
        # Compute relative frequencies
        stats = (data.stack().value_counts(normalize=True) * 100) \
            .sort_index(ascending=True)

    # Include all sub-questions in the statistics, also those not chosen
    stats_df: DataFrame = stats.to_frame(name="") \
        .reindex(sub_questions.keys(), fill_value=0) \
        .rename(mapper=sub_questions, axis=0)

    return n_valid_responses, stats_df, "%"


def nominal_array(question: QuestionCollection) -> [int, DataFrame, str]:
    """Return a table containing relative frequencies for each sub-question of
    a categorical variable if variable is an array question"""
    # Get data for question excluding non-respondents
    data: DataFrame = question.as_data_frame().dropna(how="all")

    # Count responses
    n_valid_responses: int = data.shape[0]

    # Get sub-question labels for sub-question IDs
    sub_questions: Dict = {
        question.short_id: question.label for question in
        question._questions.values()
    }

    # Compile one statistics table for all sub-questions
    stats_df: DataFrame = DataFrame()
    for sub_question, label in sub_questions.items():
        [n_sub_set, summary_stats, _] = nominal_single_choice(
            question=question.question_for_id(sub_question)
        )
        stats_df = stats_df.append(
            DataFrame(data=str(n_sub_set), index=[label], columns=[""])
        )
        stats_df = stats_df.append(summary_stats)

    return n_valid_responses, stats_df, "%"


def scale_single_choice(question: Question, **kwargs) \
        -> [int, DataFrame, str]:
    """Return table containing basic descriptive statistics (count, min, max,
    mean, standard deviation, quartiles) for a numeric variable if variable is
    a single choice question"""
    # By default statistics are not custom
    custom_stats: List[str] = kwargs.get("custom_stats", [])

    # Get data for question excluding missing values
    data = question.as_series().dropna()

    # Count responses
    n_valid_responses: int = data.shape[0]

    # Compute uni-variate descriptive statistics for numeric variable
    if not custom_stats:
        stats: Series = data.describe()
    # If a list of custom statistics is specified those will be computed
    else:
        stats: Series = data.agg(custom_stats)
    # Convert to data frame
    stats_df: DataFrame = stats.to_frame(name="")

    return n_valid_responses, stats_df, ""


def scale_array(question: QuestionCollection, **kwargs) \
        -> [int, DataFrame, str]:
    """Return table containing basic descriptive statistics (count, mean,
    standard deviation, quantiles) for each sub-question of a numeric variable
    if variable is an array question"""
    # By default statistics are not custom
    custom_stats: List[str] = kwargs.get("custom_stats", [])
    orientation_wide = kwargs.get("orientation_wide", True)

    # Get data for question excluding non-respondents
    data: DataFrame = question.as_data_frame().dropna(how="all")

    # Count responses
    n_valid_responses: int = data.shape[0]

    # Get sub question labels for sub-question IDs
    sub_questions: Dict = {
        sub_question.short_id: sub_question.label for sub_question in
        question._questions.values()
    }

    # Compile a horizontal statistics table for all sub-questions
    stats_df: DataFrame = DataFrame()
    if orientation_wide and not custom_stats:
        stats_df: DataFrame = data.describe()
    elif orientation_wide and custom_stats:
        stats_df: DataFrame = data.agg(custom_stats)
    else:
        # Compile a vertical statistics table for all sub-questions
        for sub_question, label in sub_questions.items():
            [n_sub_set, summary_stats, _] = scale_single_choice(
                question=question.question_for_id(sub_question),
                custom_stats=custom_stats
            )
            stats_df = stats_df.append(
                DataFrame(data=str(n_sub_set), index=[label], columns=[""])
            )
            stats_df = stats_df.append(summary_stats)

    return n_valid_responses, stats_df, ""


def add_to_table(tbl: DataFrame, question: Question, stats: DataFrame,
                 count: int, unit: str = "", compact: bool = False) \
        -> DataFrame:
    """Format statistics table specifically for the HIFIS survey report"""
    # Condense table in particular for grouped tables
    if compact:
        # Use shorter label instead of long question text
        question_label: str = question.label
        tbl.columns = [""]
        # Add label description and number of cases to the statistics table
        tbl = tbl.append(
            DataFrame(data=array([count]),
                      columns=[""],
                      index=[question_label]))
        # Add statistics for this variable without decimal places but with unit
        tbl = tbl.append(stats.fillna(value=0).round(0).astype("Int32")
                         .astype("str") + unit)
    # normal table
    else:
        # Use English question text to describe variable
        question_text: str = question.text("en-GB")
        # In most cases statistics will be relative frequencies using "%"
        if unit:
            # Some formatting
            unit = "(" + unit + ")"
        # Add ID, text description and number of cases to the table
        sub_header: DataFrame = DataFrame(
            data=array([[unit, count]]),
            columns=["", "n"],
            index=[question.full_id[:4] + ": " + question_text]
        )
        tbl = tbl.append(sub_header)

        # Reorder columns to have number of cases as last column
        tbl = tbl.append(stats).reindex(columns=["", "n"])

    return tbl


def bar_plot(hifis_surveyval: HIFISSurveyval, stats_to_plot: DataFrame,
             question: QuestionCollection, array_question: bool = False,
             **kwargs) -> None:
    # Annotation settings
    x_label_rotation: int = 45
    title_font_size: int = 11
    annotation_font_size: int = 10
    figure_size = [7, 4]
    ylim: tuple = kwargs.get("ylim", (0, 100))

    # Name the plot
    filename: str = kwargs.get("filename", question.full_id.replace("/_", "")
                               .replace("/", "-") + "-bar-plot")
    # Use question text as title if not specified otherwise
    plot_title: str = kwargs.get("plot_title", "\n".join(wrap(
        question.full_id[:4] + ": " + question.text("en-GB")
    )))
    # Use question label for x-axis if not specified otherwise
    x_label: str = kwargs.get("x_label", question.label)
    # Use percentage for y-axis if not specified otherwise
    y_label: str = kwargs.get("y_label", "Proportion (\\%)")

    # If single question
    if isinstance(question, Question):
        # Use answer labels as tick labels of x-axis if not specified otherwise
        x_tick_labels: List[str] = kwargs.get(
            "x_tick_labels", [answer_option.label for answer_option in
                              question._answer_options.values() if not
                              answer_option.label=="Other"]
        )
        # Find valid cases for this question
        question_df: Series = question.as_series()
        question_valid = set(question_df.dropna(how="any").index)

    # If multiple choice or array question
    else:
        # Use sub-question labels as tick labels of x-axis if not specified
        # otherwise
        x_tick_labels: List[str] = kwargs.get(
            "x_tick_labels", [sub_question.label for sub_question in
                              question._questions.values() if not
                              sub_question.full_id.endswith("comment")]
        )

        # Find valid cases for the question collection
        question_df: DataFrame = question.as_data_frame()
        question_valid = set(question_df.dropna(how="all").index)

    # For array questions
    if array_question:
        # Use bar for each sub-question
        stacked: bool = kwargs.get("stacked", True)

        # Show legend with answer labels
        show_legend: bool = kwargs.get("show_legend", True)
    else:
        # Use separate bar for each answer option
        stacked: bool = kwargs.get("stacked", False)

        # Do not show legend with answer labels
        show_legend: bool = kwargs.get("legend", False)

    print(
        "\nBar plot for {}:\n"
        "Number of HIFIS survey respondents (n={}):\n{}".format(
            question.full_id,
            len(question_valid),
            question_df.loc[question_valid].value_counts() \
                .reindex(index=x_tick_labels, fill_value=0)
        )
    )

    # Reorder column headers so that answers or sub-questions are listed in the
    # plot in the same order as in the meta data
    stats_to_plot = stats_to_plot.reindex(index=x_tick_labels)

    # Actual plotting via framework
    hifis_surveyval.plotter.plot_bar_chart(
        plot_file_name=filename,
        data_frame=stats_to_plot,
        plot_title=plot_title,
        x_axis_label=None,
        y_axis_label=y_label,
        x_label_rotation=x_label_rotation,
        ylim=ylim,
        round_value_labels_to_decimals=1,
        figure_size=figure_size,
        stacked=stacked,
        show_legend=show_legend
    )


def grouped_bar_plot(hifis_surveyval: HIFISSurveyval, stats_to_plot: DataFrame,
                     question: QuestionCollection, grouping: Question,
                     **kwargs) -> None:
    """Generate grouped bar plot"""
    # Annotation settings
    x_label_rotation: int = 45
    title_font_size: int = 11
    annotation_font_size: int = 10
    figure_size = [7, 4]
    ylim: tuple = kwargs.get("y_lim", (0, 100))

    # Name the plot
    filename: str = kwargs.get("filename",
                               question.full_id.replace("/_", "")
                               .replace("/", "-") + "-by-" +
                               grouping.full_id.replace("/_", "")
                               .replace("/", "-") + "-bar-plot")
    # Use question text as title if not specified otherwise
    plot_title: str = kwargs.get("plot_title", "\n".join(wrap(
        question.full_id[:4] + ": " + question.text("en-GB")
    )))
    # Use question label for x-axis if not specified otherwise
    x_label: str = kwargs.get("x_label", question.label)
    # Use percentage for y-axis if not specified otherwise
    y_label: str = kwargs.get("y_label", "Proportion (\\%)")

    # If single question
    if isinstance(question, Question):
        # Use answer labels as tick labels of x-axis if not specified otherwise
        x_tick_labels: List[str] = kwargs.get(
            "x_tick_labels", [answer_option.label for answer_option in
                              question._answer_options.values()]
        )
        # Find valid cases for this question
        question_valid = set(question.as_series().dropna(how="any").index)

    # If multiple choice or array question
    else:
        # Use sub-question labels as tick labels of x-axis if not specified
        # otherwise
        x_tick_labels: List[str] = kwargs.get(
            "x_tick_labels", [sub_question.label for sub_question in
                              question._questions.values() if not
                              sub_question.full_id.endswith("comment")]
        )
        # Find valid cases for the question collection
        question_valid = set(question.as_data_frame().dropna(how="all").index)

    # Find valid cases without missing values in either question
    grouping_valid = set(grouping.as_series().dropna(how="any").index)
    valid = grouping_valid.intersection(question_valid)

    # Info for caption
    print(
        "\nGrouped bar plot for {}:\n"
        "Number of HIFIS survey respondents (n={}) grouped by {}:\n{}".format(
            question.full_id,
            len(valid),
            grouping.label,
            grouping.as_series().loc[valid].value_counts()
        )
    )

    # Reorder index so that groups are listed in the plot in the same order
    # as in the meta data
    stats_to_plot = stats_to_plot.reindex(index=x_tick_labels)

    # Actual plotting via framework
    hifis_surveyval.plotter.plot_bar_chart(
        plot_file_name=filename,
        data_frame=stats_to_plot,
        plot_title=plot_title,
        x_axis_label=None,
        y_axis_label=y_label,
        x_label_rotation=x_label_rotation,
        show_legend=True,
        round_value_labels_to_decimals=1,
        figure_size=figure_size,
        ylim=ylim
    )


def grouped_box_plot(hifis_surveyval: HIFISSurveyval, stats_to_plot: DataFrame,
                     question: Question, grouping: Question, **kwargs) -> None:
    """Generate grouped box plot for numeric variable if variable is a single
    chocie question"""
    # Annotation settings
    x_label_rotation: int = 45
    title_font_size: int = 11
    annotation_font_size: int = 10
    figure_size = [7, 4]
    ylim = (0, 100)

    # Name the plot
    filename: str = kwargs.get("filename",
                               question.full_id.replace("/_", "")
                               .replace("/", "-") + "-by-" +
                               grouping.full_id.replace("/_", "")
                               .replace("/", "-") + "-box-plot")
    # Use question text as title
    plot_title: str = kwargs.get("plot_title", "\n".join(
        wrap(question.full_id[:4] + ": " + question.text("en-GB"))
    ))
    # Use grouping label for x-axis
    x_label: str = kwargs.get("x_label", grouping.label)
    # Use question label for y-axis
    y_label: str = kwargs.get("y_label", question.label)
    # Use answer labels of grouping variable as tick labels of x-axis
    x_tick_labels: List[str] = kwargs.get(
        "x_tick_labels", [answer_option.label for answer_option in
                          grouping._answer_options.values() if
                          answer_option.label in stats_to_plot.columns]
    )

    # Info for caption
    print(
        "\nBox plot for {}:\n"
        "Number of HIFIS survey respondents (n={}) grouped by {}:\n{}".format(
            question.full_id,
            stats_to_plot.notna().sum().sum(),
            x_label,
            stats_to_plot.notna().sum()
        )
    )

    # Reorder column headers so that groups are listed in the plot in the same
    # order as in the meta data
    stats_to_plot = stats_to_plot.reindex(columns=x_tick_labels)

    # Actual plotting
    hifis_surveyval.plotter.plot_box_chart(
        plot_file_name=filename,
        data_frame=stats_to_plot,
        plot_title=plot_title,
        x_axis_label=None,
        y_axis_label=y_label,
        x_label_rotation=x_label_rotation,
        figure_size=figure_size,
        ylim=ylim,
        showmeans=True,
        meanline=True
    )


def generate_word_cloud(free_text_response: str, question: str) -> None:
    """Generate word cloud"""
    # Create and generate a word cloud plot:
    wordcloud = WordCloud(background_color="white", max_words=None,
                          colormap="Blues", collocations=True) \
        .generate(free_text_response)

    # Display the generated plot
    plt.imshow(wordcloud, interpolation="bilinear")
    plt.axis("off")
    plt.tight_layout()
    plt.savefig(
        fname=question + "-word-cloud.pdf",
        dpi=300,
        format="pdf"
    )
    plt.show()
