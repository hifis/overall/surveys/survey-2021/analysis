# SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
# SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
#
# SPDX-License-Identifier: MIT

"""Analyse technology questions about Continuous Integration tools."""

from pathlib import Path
from typing import Dict, List

from hifis_surveyval.core import util
from pandas import DataFrame

from hifis_surveyval.data_container import DataContainer
from hifis_surveyval.hifis_surveyval import HIFISSurveyval
from hifis_surveyval.models.question import Question
from hifis_surveyval.models.question_collection import QuestionCollection


def run(hifis_surveyval: HIFISSurveyval, data: DataContainer):
    """Analyses technology question about Continuous Integration tools."""
    print("======== Beginning of Script: " + Path(__file__).stem + " ========")

    # Get QuestionCollections and Questions analysed in this script.
    ci: QuestionCollection = data.collection_for_id("Q017")
    developers: Question = data.question_for_id("V001/_")
    team: Question = data.question_for_id("V003/_")

    # Get data as DataFrames and Series for these Collections and Questions
    data_ci_raw: DataFrame = ci.as_data_frame()
    data_developers_raw: DataFrame = developers.as_series()
    data_team_raw: DataFrame = team.as_series()

    # Define a mapping to recode boolean to string values.
    bool_recode_dict: Dict[str, str] = {True: "Yes", False: "No"}

    # Define an ordering of the AnswerOptions.
    options_order_list: List[str] = ["Yes",
                                     "Don't know it",
                                     "Not relevant",
                                     "Don't know how",
                                     "Doesn't fit my needs",
                                     "Not available"]

    # Define an ordering of the recodes AnswerOptions.
    options_yesno_order_list: List[str] = ["Yes", "No"]

    # Define a mapping to replace the full ID by the label.
    questions_mapping_dict: Dict[str, str] = {question.full_id: question.label
                                              for question in ci.questions}

    title: str = ci.text("en-GB")
    print(title)
    questions: List[str] = [question.text("en-GB")
                            for question in ci.questions]
    print(questions)

    title_aggregated: str = \
        "Did you use automation tools or services in the last twelve months?"

    # Create a DataFrame in which only data of participants is contained
    # who answered the question and does not contain only None values.
    data_ci_answered_only: DataFrame = data_ci_raw.dropna(axis=0, how="all")
    # Rename columns in DataFrame from the full ID to the label.
    data_ci_renamed: DataFrame = data_ci_answered_only.rename(
        mapper=questions_mapping_dict, axis='columns')

    # Define a Series of boolean values which determine which rows have all
    # "No" in each cell.
    ci_all_no: DataFrame = \
        (data_ci_renamed["Jenkins"] != "Yes") & \
        (data_ci_renamed["Travis CI"] != "Yes") & \
        (data_ci_renamed["Circle CI"] != "Yes") & \
        (data_ci_renamed["GitHub Actions"] != "Yes") & \
        (data_ci_renamed["GitLab CI (self-hosted)"] != "Yes") & \
        (data_ci_renamed["GitLab CI (gitlab.com)"] != "Yes")

    # Invert it to a Series of boolean values which determine which cells have
    # not all "No" in each cell.
    ci_not_all_no = ~ci_all_no

    ###########################################################################
    # Frequencies of answer options given regarding usage of CI tools
    ###########################################################################

    data_ci_freq_abs: DataFrame = util.dataframe_value_counts(
        data_ci_renamed, relative_values=False)

    data_ci_freq_abs = data_ci_freq_abs.reindex(options_order_list)

    # Divide all absolute values by the number of answers given to get relative
    # values of multiple choice question.
    data_ci_freq_rel: DataFrame = \
        data_ci_freq_abs.div(len(data_ci_answered_only))

    hifis_surveyval.plotter.plot_bar_chart(
        data_frame=data_ci_freq_rel,
        plot_file_name="Q017-ci-per-options",
        plot_title=title,
        plot_title_fontsize=16,
        x_axis_label="Answer options",
        y_axis_label="Usage Continuous Integration tools (relative counts)",
        x_label_rotation=0,
        round_value_labels_to_decimals=2,
        figure_size=(19, 5))

    ###########################################################################
    # Frequencies regarding overall usage of Continuous Integration tools
    ###########################################################################

    data_ci_all_yesno_freq_rel: DataFrame = util.dataframe_value_counts(
        DataFrame(ci_not_all_no), relative_values=True)
    data_ci_all_yesno_freq_rel = \
        data_ci_all_yesno_freq_rel.rename(bool_recode_dict, axis=0)
    data_ci_all_yesno_freq_rel = \
        data_ci_all_yesno_freq_rel.rename(
            {0: "Continuous Integration tools"}, axis=1)
    data_ci_all_yesno_freq_rel = \
        data_ci_all_yesno_freq_rel.T[options_yesno_order_list]

    hifis_surveyval.plotter.plot_bar_chart(
        data_frame=data_ci_all_yesno_freq_rel,
        plot_file_name="Q017-ci-overall",
        plot_title=title_aggregated,
        plot_title_fontsize=16,
        x_axis_label="",
        y_axis_label="Usage Continuous Integration tools (relative counts)",
        x_label_rotation=0,
        round_value_labels_to_decimals=2,
        figure_size=(6, 5))

    ###########################################################################
    # Frequencies regarding usage of CI based on answers given by developers
    ###########################################################################

    data_ci_per_developers: DataFrame = \
        util.filter_and_group_series(ci_not_all_no,
                                     data_developers_raw.dropna())

    data_ci_all_yesno_per_developers_freq_rel: DataFrame = \
        util.dataframe_value_counts(data_ci_per_developers,
                                    relative_values=True)
    data_ci_all_yesno_per_developers_freq_rel = \
        data_ci_all_yesno_per_developers_freq_rel.rename(
            {"Yes": "Software-Developers", "No": " Non-Software-Developers"},
            axis=1)
    data_ci_all_yesno_per_developers_freq_rel = \
        data_ci_all_yesno_per_developers_freq_rel.rename(
            bool_recode_dict, axis=0)
    data_ci_all_yesno_per_developers_freq_rel = \
        data_ci_all_yesno_per_developers_freq_rel.T[options_yesno_order_list]

    hifis_surveyval.plotter.plot_bar_chart(
        data_frame=data_ci_all_yesno_per_developers_freq_rel,
        plot_file_name="Q017-ci-swdevs",
        plot_title=title_aggregated,
        plot_title_fontsize=16,
        x_axis_label="",
        y_axis_label="Usage Continuous Integration tools (relative counts)",
        x_label_rotation=0,
        round_value_labels_to_decimals=2,
        figure_size=(6, 5))

    ###########################################################################
    # Frequencies regarding usage of CI tools depending on team size
    ###########################################################################

    data_ci_plus_developers = \
        DataFrame(ci_not_all_no).join(DataFrame(data_developers_raw),
                                       on="id",
                                       how="inner")
    data_ci_only_developers = \
        data_ci_plus_developers[data_ci_plus_developers["V001/_"] == "Yes"]

    data_ci_per_team_size: DataFrame = \
        util.filter_and_group_series(data_ci_only_developers[0],
                                     data_team_raw.dropna())

    data_ci_all_yesno_per_team_size_freq_rel: DataFrame = \
        util.dataframe_value_counts(data_ci_per_team_size,
                                    relative_values=True)
    data_ci_all_yesno_per_team_size_freq_rel = \
        data_ci_all_yesno_per_team_size_freq_rel.rename(
            {"Yes": "Team-Developer", "No": "Individual-Developer"},
            axis=1)
    data_ci_all_yesno_per_team_size_freq_rel = \
        data_ci_all_yesno_per_team_size_freq_rel.rename(
            bool_recode_dict, axis=0)
    data_ci_all_yesno_per_team_size_freq_rel = \
        data_ci_all_yesno_per_team_size_freq_rel[["Individual-Developer",
                                                   "Team-Developer"]]
    data_ci_all_yesno_per_team_size_freq_rel = \
        data_ci_all_yesno_per_team_size_freq_rel.T[options_yesno_order_list]

    hifis_surveyval.plotter.plot_bar_chart(
        data_frame=data_ci_all_yesno_per_team_size_freq_rel,
        plot_file_name="Q017-ci-team",
        plot_title=title_aggregated,
        plot_title_fontsize=16,
        x_axis_label="Team Size",
        y_axis_label="Usage Continuous Integration tools (relative counts)",
        x_label_rotation=0,
        round_value_labels_to_decimals=2,
        figure_size=(6, 5))

    ###########################################################################

    print("=========== End of Script: " + Path(__file__).stem + " ===========")
