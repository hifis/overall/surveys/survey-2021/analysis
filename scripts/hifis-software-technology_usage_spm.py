# SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
# SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
#
# SPDX-License-Identifier: MIT

"""Analyse technology questions about software project management platforms."""

from pathlib import Path
from typing import Dict, List

from hifis_surveyval.core import util
from pandas import DataFrame

from hifis_surveyval.data_container import DataContainer
from hifis_surveyval.hifis_surveyval import HIFISSurveyval
from hifis_surveyval.models.question import Question
from hifis_surveyval.models.question_collection import QuestionCollection


def run(hifis_surveyval: HIFISSurveyval, data: DataContainer):
    """Analyses technology question about SW project management platforms."""
    print("======== Beginning of Script: " + Path(__file__).stem + " ========")

    # Get QuestionCollections and Questions analysed in this script.
    spm: QuestionCollection = data.collection_for_id("Q016")
    developers: Question = data.question_for_id("V001/_")
    team: Question = data.question_for_id("V003/_")

    # Get data as DataFrames and Series for these Collections and Questions
    data_spm_raw: DataFrame = spm.as_data_frame()
    data_developers_raw: DataFrame = developers.as_series()
    data_team_raw: DataFrame = team.as_series()

    # Define a mapping to recode boolean to string values.
    bool_recode_dict: Dict[str, str] = {True: "Yes", False: "No"}

    # Define an ordering of the AnswerOptions.
    options_order_list: List[str] = ["Yes",
                                     "Don't know it",
                                     "Not relevant",
                                     "Don't know how",
                                     "Doesn't fit my needs",
                                     "Not available"]

    # Define an ordering of the recodes AnswerOptions.
    options_yesno_order_list: List[str] = ["Yes", "No"]

    # Define a mapping to replace the full ID by the label.
    questions_mapping_dict: Dict[str, str] = {question.full_id: question.label
                                              for question in spm.questions}

    title: str = spm.text("en-GB")
    print(title)
    questions: List[str] = [question.text("en-GB")
                            for question in spm.questions]
    print(questions)

    title_aggregated: str = \
        "Did you use code hosting services in the last twelve months?"

    # Create a DataFrame in which only data of participants is contained
    # who answered the question and does not contain only None values.
    data_spm_answered_only: DataFrame = data_spm_raw.dropna(axis=0, how="all")
    # Rename columns in DataFrame from the full ID to the label.
    data_spm_renamed: DataFrame = data_spm_answered_only.rename(
        mapper=questions_mapping_dict, axis='columns')

    # Define a Series of boolean values which determine which rows have all
    # "No" in each cell.
    spm_all_no: DataFrame = (data_spm_renamed["GitHub"] != "Yes") & \
                            (data_spm_renamed["GitLab"] != "Yes") & \
                            (data_spm_renamed["BitBucket"] != "Yes") & \
                            (data_spm_renamed["Redmine"] != "Yes") & \
                            (data_spm_renamed["Gitea"] != "Yes")

    # Invert it to a Series of boolean values which determine which cells have
    # not all "No" in each cell.
    spm_not_all_no = ~spm_all_no

    ###########################################################################
    # Frequencies of answer options given regarding usage of SPM platforms
    ###########################################################################

    data_spm_freq_abs: DataFrame = util.dataframe_value_counts(
        data_spm_renamed, relative_values=False)

    data_spm_freq_abs = data_spm_freq_abs.reindex(options_order_list)

    # Divide all absolute values by the number of answers given to get relative
    # values of multiple choice question.
    data_spm_freq_rel: DataFrame = \
        data_spm_freq_abs.div(len(data_spm_answered_only))

    hifis_surveyval.plotter.plot_bar_chart(
        data_frame=data_spm_freq_rel,
        plot_file_name="Q016-spm-per-options",
        plot_title=title,
        plot_title_fontsize=16,
        x_axis_label="Answer options",
        y_axis_label="Usage Software Project Management Platforms "
                     "(relative counts)",
        x_label_rotation=0,
        round_value_labels_to_decimals=2,
        figure_size=(15, 5))

    ###########################################################################
    # Frequencies regarding overall usage of SPM platforms
    ###########################################################################

    data_spm_all_yesno_freq_rel: DataFrame = util.dataframe_value_counts(
        DataFrame(spm_not_all_no), relative_values=True)
    data_spm_all_yesno_freq_rel = \
        data_spm_all_yesno_freq_rel.rename(bool_recode_dict, axis=0)
    data_spm_all_yesno_freq_rel = \
        data_spm_all_yesno_freq_rel.rename(
            {0: "Software Project Management Platforms"}, axis=1)
    data_spm_all_yesno_freq_rel = \
        data_spm_all_yesno_freq_rel.T[options_yesno_order_list]

    hifis_surveyval.plotter.plot_bar_chart(
        data_frame=data_spm_all_yesno_freq_rel,
        plot_file_name="Q016-spm-overall",
        plot_title=title_aggregated,
        plot_title_fontsize=16,
        x_axis_label="",
        y_axis_label="Usage Software Project Management Platforms "
                     "(relative counts)",
        x_label_rotation=0,
        round_value_labels_to_decimals=2,
        figure_size=(6, 5))

    ###########################################################################
    # Frequencies regarding usage of SPMPs based on answers given by developers
    ###########################################################################

    data_spm_per_developers: DataFrame = \
        util.filter_and_group_series(spm_not_all_no,
                                     data_developers_raw.dropna())

    data_spm_all_yesno_per_developers_freq_rel: DataFrame = \
        util.dataframe_value_counts(data_spm_per_developers,
                                    relative_values=True)
    data_spm_all_yesno_per_developers_freq_rel = \
        data_spm_all_yesno_per_developers_freq_rel.rename(
            {"Yes": "Software-Developers", "No": " Non-Software-Developers"},
            axis=1)
    data_spm_all_yesno_per_developers_freq_rel = \
        data_spm_all_yesno_per_developers_freq_rel.rename(
            bool_recode_dict, axis=0)
    data_spm_all_yesno_per_developers_freq_rel = \
        data_spm_all_yesno_per_developers_freq_rel.T[options_yesno_order_list]

    hifis_surveyval.plotter.plot_bar_chart(
        data_frame=data_spm_all_yesno_per_developers_freq_rel,
        plot_file_name="Q016-spm-swdevs",
        plot_title=title_aggregated,
        plot_title_fontsize=16,
        x_axis_label="",
        y_axis_label="Usage Software Project Management Platforms "
                     "(relative counts)",
        x_label_rotation=0,
        round_value_labels_to_decimals=2,
        figure_size=(6, 5))

    ###########################################################################
    # Frequencies regarding usage of SPM platforms depending on team size
    ###########################################################################

    data_spm_plus_developers = \
        DataFrame(spm_not_all_no).join(DataFrame(data_developers_raw),
                                       on="id",
                                       how="inner")
    data_spm_only_developers = \
        data_spm_plus_developers[data_spm_plus_developers["V001/_"] == "Yes"]

    data_spm_per_team_size: DataFrame = \
        util.filter_and_group_series(data_spm_only_developers[0],
                                     data_team_raw.dropna())

    data_spm_all_yesno_per_team_size_freq_rel: DataFrame = \
        util.dataframe_value_counts(data_spm_per_team_size,
                                    relative_values=True)
    data_spm_all_yesno_per_team_size_freq_rel = \
        data_spm_all_yesno_per_team_size_freq_rel.rename(
            {"Yes": "Team-Developer", "No": "Individual-Developer"},
            axis=1)
    data_spm_all_yesno_per_team_size_freq_rel = \
        data_spm_all_yesno_per_team_size_freq_rel.rename(
            bool_recode_dict, axis=0)
    data_spm_all_yesno_per_team_size_freq_rel = \
        data_spm_all_yesno_per_team_size_freq_rel[["Individual-Developer",
                                                   "Team-Developer"]]
    data_spm_all_yesno_per_team_size_freq_rel = \
        data_spm_all_yesno_per_team_size_freq_rel.T[options_yesno_order_list]

    hifis_surveyval.plotter.plot_bar_chart(
        data_frame=data_spm_all_yesno_per_team_size_freq_rel,
        plot_file_name="Q016-spm-team",
        plot_title=title_aggregated,
        plot_title_fontsize=16,
        x_axis_label="Team Size",
        y_axis_label="Usage Software Project Management Platforms "
                     "(relative counts)",
        x_label_rotation=0,
        round_value_labels_to_decimals=2,
        figure_size=(6, 5))

    ###########################################################################

    print("=========== End of Script: " + Path(__file__).stem + " ===========")
