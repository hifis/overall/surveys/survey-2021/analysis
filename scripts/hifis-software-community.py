# SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
# SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
#
# SPDX-License-Identifier: MIT

from typing import Dict, List, Set

import modules.descriptive_stats as stats
from pandas import DataFrame, Series
from hifis_surveyval.data_container import DataContainer, Question, \
    QuestionCollection
from hifis_surveyval.hifis_surveyval import HIFISSurveyval


def run(hifis_surveyval: HIFISSurveyval, data: DataContainer):
    """
    Compute descriptive statistics for WP Community specific questions to
    analyze different aspects of respondent's interest in community events
    """
    # Output path
    path: str = str(hifis_surveyval.settings.ANALYSIS_OUTPUT_PATH) + "\\"

    # Relevant questions
    questions: Dict = {
        "Q008/_": _rse_identity,
        "Q030": _community_events_interest,
        "Q031": _community_events_satisfaction,
        "Q032": _community_events_size,
        "Q033": _community_events_participation
    }

    # Create statistics table containing total number of cases
    community_sample: DataFrame = data.data_frame_for_ids(
            requested_ids=list(questions.keys())
        ).dropna(how="all")
    community_tbl: DataFrame = stats.get_sample_size(community_sample)

    # Exclude cases from analysis who did not answer any community question
    incomplete_cases: Set = set(data.data_frame_for_ids(
        requested_ids=data.question_collection_ids
    ).index).difference(set(community_sample.index))

    # Run the respective analysis for each of the specified questions
    # and save descriptive statistics in joint table
    for question_id, analysis in questions.items():
        community_tbl = analysis(
            hifis_surveyval=hifis_surveyval,
            data=data,
            question_id=question_id,
            stats_tbl=community_tbl,
            cases=incomplete_cases
        )
    community_tbl.rename({"mean": "M", "std": "SD"}, inplace=True)

    # Display and export resulting table of sample characteristics
    print("\nHIFIS survey community:\n{}\n"
          .format(community_tbl))
    community_tbl.to_latex(
        buf=path + "community-statistics.tex",
        header=True,
        index=True,
        na_rep="",
        float_format="{:0.2f}".format,
        column_format="@{}p{0.9\\textwidth}r@{}r@{}",
        caption="Community",
        label="tab:community_stats",
        position="htbp",
        longtable=False
    )


def _rse_identity(hifis_surveyval: HIFISSurveyval, data: DataContainer,
                  question_id: str, stats_tbl: DataFrame, cases: Set) \
        -> DataFrame:
    # Get data on identification as research software engineer
    question: Question = data.question_for_id(question_id)
    question.remove_answers(cases)

    # Relative frequencies of observations
    [n, question_freq_rel, unit] = stats.nominal_single_choice(question)

    # Add question statistics to table
    stats_tbl = stats.add_to_table(
         tbl=stats_tbl,
         question=question,
         count=n,
         stats=question_freq_rel,
         unit=unit,
         compact=False
    )

    return stats_tbl


def _community_events_interest(hifis_surveyval: HIFISSurveyval,
                                   data: DataContainer, question_id: str,
                                   stats_tbl: DataFrame, cases: Set) \
        -> DataFrame:
    # Get data on general interest in community events
    question: QuestionCollection = data.collection_for_id(question_id)

    # Relative frequencies of observations
    [n, question_freq_rel, unit] = stats.nominal_multiple_choice(question)

    # Add question statistics to table
    stats_tbl = stats.add_to_table(
         tbl=stats_tbl,
         question=question,
         count=n,
         stats=question_freq_rel,
         unit=unit,
         compact=False
    )

    # Grouping variable
    grouping: Question = data.question_for_id("Q008/_")
    grouping.remove_answers(cases)
    grouping_variable: Series = grouping.as_series()\
        .replace({"Not sure": None})

    # Group data by RSEs vs non-RSEs and compute relative frequencies
    question_variable: DataFrame = question.as_data_frame()
    question_variable.drop(
        columns=[column for column in question_variable.columns
                 if column.endswith("comment")],
        inplace=True
    )
    grouped_data: DataFrame = question_variable\
        .groupby(grouping_variable)\
        .sum()
    grouped_data = grouped_data.divide(
        grouped_data.sum(axis="columns"), axis="index"
    ) * 100

    # Replace sub-question ids with sub-question labels as column header
    sub_questions: Dict = {
        sub_question.full_id: sub_question.label for sub_question in
        question._questions.values()
    }
    grouped_data.rename(columns=sub_questions,
                        index={"No": "Non-RSE", "Yes": "RSE"},
                        inplace=True)

    # Plot general interest in community events grouped by RSE identity
    stats.grouped_bar_plot(
        hifis_surveyval=hifis_surveyval,
        question=question,
        grouping=grouping,
        stats_to_plot=grouped_data.T
    )

    # Plot reasons for not participating in any community events
    sub_question: Question = data.question_for_id("Q030/SQ001comment")
    [_, sq001_freq_rel, _] = stats.nominal_single_choice(sub_question)
    stats.bar_plot(
        hifis_surveyval=hifis_surveyval,
        question=sub_question,
        stats_to_plot=DataFrame(sq001_freq_rel)
    )

    # Group data by RSEs vs non-RSEs and compute relative frequencies
    sub_question_variable: Series = sub_question.as_series()
    response_options: List[str] = [
        answer_option.label for answer_option in
        sub_question._answer_options.values()
    ]
    grouped_data: DataFrame = DataFrame(
        sub_question_variable.groupby(grouping_variable).value_counts()
    )
    grouped_data = grouped_data.loc["No", :]\
        .join(grouped_data.loc["Yes", :], how="outer", lsuffix="_no",
              rsuffix="_yes")\
        .rename(columns={"Q030/SQ001comment_yes": "RSE",
                         "Q030/SQ001comment_no": "Non-RSE"}) \
        .fillna(value=0)
    grouped_data = (grouped_data.divide(
        grouped_data.sum(axis="index"), axis="columns"
    ) * 100).reindex(response_options, fill_value=0)

    # Plot reasons for not participating in any community events grouped by
    # RSE identity
    stats.grouped_bar_plot(
        hifis_surveyval=hifis_surveyval,
        question=sub_question,
        grouping=grouping,
        stats_to_plot=grouped_data,
        y_tick_labels=grouped_data.index
    )

    # Plot community events respondents have heard of
    sub_question: Question = data.question_for_id("Q030/SQ003comment")
    [_, sq003_freq_rel, _] = stats.nominal_single_choice(sub_question)
    stats.bar_plot(
        hifis_surveyval=hifis_surveyval,
        question=sub_question,
        stats_to_plot=DataFrame(sq003_freq_rel)
    )

    # Group data by RSEs vs non-RSEs and compute relative frequencies
    sub_question_variable: Series = sub_question.as_series()
    response_options: List[str] = [
        answer_option.label for answer_option in
        sub_question._answer_options.values()
    ]
    grouped_data: DataFrame = DataFrame(
        sub_question_variable.groupby(grouping_variable).value_counts()
    )
    grouped_data = grouped_data.loc["No", :] \
        .join(grouped_data.loc["Yes", :], how="outer", lsuffix="_no",
              rsuffix="_yes") \
        .rename(columns={"Q030/SQ003comment_yes": "RSE",
                         "Q030/SQ003comment_no": "Non-RSE"})\
        .fillna(value=0)
    grouped_data = (grouped_data.divide(
        grouped_data.sum(axis="index"), axis="columns"
    ) * 100).reindex(response_options, fill_value=0)

    # Plot events respondents have heard of grouped by RSE identity
    stats.grouped_bar_plot(
        hifis_surveyval=hifis_surveyval,
        question=sub_question,
        grouping=grouping,
        stats_to_plot=grouped_data,
        y_tick_labels=grouped_data.index
    )

    # Plot community events respondents participated in
    sub_question: Question = data.question_for_id("Q030/SQ004comment")
    [_, sq004_freq_rel, _] = stats.nominal_single_choice(sub_question)
    stats.bar_plot(
        hifis_surveyval=hifis_surveyval,
        question=sub_question,
        stats_to_plot=DataFrame(sq004_freq_rel)
    )

    # Group data by RSEs vs non-RSEs and compute relative frequencies
    sub_question_variable: Series = sub_question.as_series()
    response_options: List[str] = [
        answer_option.label for answer_option in
        sub_question._answer_options.values()
    ]
    grouped_data: DataFrame = DataFrame(
        sub_question_variable.groupby(grouping_variable).value_counts()
    )
    grouped_data = grouped_data.loc["No", :] \
        .join(grouped_data.loc["Yes", :], how="outer", lsuffix="_no",
              rsuffix="_yes") \
        .rename(columns={"Q030/SQ004comment_yes": "RSE",
                         "Q030/SQ004comment_no": "Non-RSE"})\
        .fillna(value=0)
    grouped_data = (grouped_data.divide(
        grouped_data.sum(axis="index"), axis="columns"
    ) * 100).reindex(response_options, fill_value=0)

    # Plot community events respondents participated in grouped by RSE identity
    stats.grouped_bar_plot(
        hifis_surveyval=hifis_surveyval,
        question=sub_question,
        grouping=grouping,
        stats_to_plot=grouped_data,
        y_tick_labels=grouped_data.index
    )

    return stats_tbl


def _community_events_satisfaction(hifis_surveyval: HIFISSurveyval,
                                   data: DataContainer, question_id: str,
                                   stats_tbl: DataFrame, cases: Set) \
        -> DataFrame:
    # Get data on community event satisfaction
    question: QuestionCollection = data.collection_for_id(question_id)

    # Relative frequencies of observations
    [n, summary_stats, unit] = stats.scale_array(question=question,
                                                 orientation_wide=False,
                                                 custom_stats=["mean", "std"])

    # Add question statistics to table
    stats_tbl = stats.add_to_table(
         tbl=stats_tbl,
         question=question,
         count=n,
         stats=summary_stats,
         unit=unit,
         compact=False
    )

    return stats_tbl


def _community_events_size(hifis_surveyval: HIFISSurveyval,
                           data: DataContainer, question_id: str,
                           stats_tbl: DataFrame, cases: Set) -> DataFrame:
    # Get data on preferred community event size
    question: QuestionCollection = data.collection_for_id(question_id)

    # Relative frequencies of observations
    [n, question_freq_rel, unit] = stats.nominal_multiple_choice(question)

    # Add question statistics to table
    stats_tbl = stats.add_to_table(
         tbl=stats_tbl,
         question=question,
         count=n,
         stats=question_freq_rel,
         unit=unit,
         compact=False
    )

    # Grouping variable
    grouping: Question = data.question_for_id("Q008/_")
    grouping.remove_answers(cases)
    grouping_variable: Series = grouping.as_series()\
        .replace({"Not sure": None})

    # Group data by RSEs vs non-RSEs and compute relative frequencies
    grouped_data: DataFrame = question.as_data_frame()\
        .groupby(grouping_variable)\
        .sum()\
        .divide(grouping_variable.value_counts(), axis="index") * 100

    # Replace sub-question ids with sub-question labels as column header
    sub_questions: Dict = {
        sub_question.full_id: sub_question.label for sub_question in
        question._questions.values()
    }
    grouped_data.rename(columns=sub_questions,
                        index={"No": "Non-RSE", "Yes": "RSE"},
                        inplace=True)

    # Plot preferred community event size grouped by RSE identity
    stats.grouped_bar_plot(
        hifis_surveyval=hifis_surveyval,
        question=question,
        grouping=grouping,
        stats_to_plot=grouped_data.T
    )

    return stats_tbl


def _community_events_participation(hifis_surveyval: HIFISSurveyval,
                                    data: DataContainer, question_id: str,
                                    stats_tbl: DataFrame, cases: Set) \
        -> DataFrame:
    # Get data on preferred forms of community event participation
    question: QuestionCollection = data.collection_for_id(question_id)

    # Relative frequencies of observations
    [n, question_freq_rel, unit] = stats.nominal_multiple_choice(question)

    # Add question statistics to table
    stats_tbl = stats.add_to_table(
                     tbl=stats_tbl,
                     question=question,
                     count=n,
                     stats=question_freq_rel,
                     unit=unit,
                     compact=False
    )

    # Grouping variable
    grouping: Question = data.question_for_id("Q008/_")
    grouping.remove_answers(cases)
    grouping_variable: Series = grouping.as_series()\
        .replace({"Not sure": None})

    # Group data by RSEs vs non-RSEs and compute relative frequencies
    grouped_data: DataFrame = question.as_data_frame()\
        .groupby(grouping_variable)\
        .sum()\
        .divide(grouping_variable.value_counts(), axis="index") * 100

    # Replace sub-question ids with sub-question labels as column header
    sub_questions: Dict = {
        sub_question.full_id: sub_question.label for sub_question in
        question._questions.values()
    }
    grouped_data.rename(columns=sub_questions,
                        index={"No": "Non-RSE", "Yes": "RSE"},
                        inplace=True)

    # Plot preferred forms of community event participation grouped by RSE
    # identity
    stats.grouped_bar_plot(
        hifis_surveyval=hifis_surveyval,
        question=question,
        grouping=grouping,
        stats_to_plot=grouped_data.T
    )

    return stats_tbl
