#!/bin/bash

# SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
# SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
#
# SPDX-License-Identifier: MIT

# Cloud specific scripts, analyses etc

mv -v cloud*.csv report/stats/
pdfunite report/fig/cloud*.pdf report/fig/_allcloudplots.pdf

pushd report/stats/

# print header
echo "Q/S,group_id,group_selected,absolute counts,total in group,relative[%],relative_rounded[%]" > cloud-extracted-group-statistics.csv

for q1 in "Q010" "Q011" "Q013" "Q014"; do      # loop through every primary question
   for q2 in "Q003" "Q002" "Q005"; do          # loop through groups

      # extract all subgroup labels
      labels=$(cat cloud-statistics-clustered.csv | sed -e 's/ /_/g' | grep "$q1"/ | grep "$q2"/ | awk 'BEGIN{FS=","}{printf ("%s\n",$3)}'|sort|uniq)

      # sort for each label; according to relative values (column 6). Also print relative values as rounded value (then column 7)
      for label in $labels; do
         cat cloud-statistics-clustered.csv | sed -e 's/ /_/g' | grep "$q1"/ | grep "$q2"/ | grep $label | sort -g -t ',' -r -k 6 | awk '
            BEGIN{FS=","}{
               printf ("%s,%.0f\n",$0,$6)
               sum+=$4
            }END{
               printf (",,sum:,%g\n",sum);
            }'
         #echo $label
         echo
      done
      echo
   done
done >> cloud-extracted-group-statistics.csv
