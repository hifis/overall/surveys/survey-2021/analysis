# SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
# SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
#
# SPDX-License-Identifier: MIT

import sys
from pandas import DataFrame, Series
from hifis_surveyval.core import util
from hifis_surveyval.data_container import DataContainer
from hifis_surveyval.hifis_surveyval import HIFISSurveyval
from hifis_surveyval.models.question import Question
from hifis_surveyval.models.question_collection import QuestionCollection

# Global variable
SHOW_PLOTS = True
scale = 100
q_img_exceptions = ["all"]
xtic_rot=45
def div(value, denominator):
    return value/denominator


def qanalyze(q_id: str, hfs: HIFISSurveyval, data: DataContainer, parameters: dict, group_select: str, plot_all: bool):

    # Variable declaration and init
    q_collection: QuestionCollection = data.collection_for_id(q_id)
    q_data: DataFrame             = q_collection.as_data_frame()
    question_text: str            = q_collection.text("en-GB")
    title: str                    = f'{q_id} : {question_text}'
    print_data: list              = []
    add_row: dict                 = {}
    answer_options                = {question.full_id: question.label for question in q_collection.questions}
    original_stdout               = sys.stdout # save that to revert when necessary.




    # preparations depending on selected group

    # select column/variable to evaluate
    if group_select=='developer':
        col_select='V001/_'
    elif group_select=='hifis':
        col_select='V002/_'
    elif group_select=='team':
        col_select='V003/_'
    elif group_select=='employedtime':
        col_select='Q002/_'
    elif group_select=='workinggroup':
        col_select='Q003/_'
    elif group_select=='researchfield':
        col_select='Q004/_'
    elif group_select=='developerQ5':
        col_select='Q005/_'

    users = { group_select: {}, 'total': 0}
    q_data = q_data.dropna(axis=0, how='all', thresh=None, subset=None, inplace=False)
    group_series: Series = data.question_for_id(col_select).as_series()

    q_data_joined = q_data.join(group_series)
    if f'{q_id}/other' in q_data_joined.columns:
        q_data_joined = q_data_joined.drop(f"{q_id}/other", axis=1)

    if f'{q_id}/other' in q_data.columns:
        q_data = q_data.drop(f"{q_id}/other", axis=1)

    if group_select=='developer' or group_select=='hifis' or group_select=='team' or group_select=='developerQ5':
        users[group_select]['Yes'] = len(q_data_joined[q_data_joined[col_select] == 'Yes'])
        users[group_select]['No']  = len(q_data_joined[q_data_joined[col_select] == 'No'])
    elif group_select=='employedtime':
        users[group_select]['0 years']             = len(q_data_joined[q_data_joined[col_select] == '0 years'])
        users[group_select]['Up to one year']      = len(q_data_joined[q_data_joined[col_select] == 'Up to one year'])
        users[group_select]['1-3 years']           = len(q_data_joined[q_data_joined[col_select] == '1-3 years'])
        users[group_select]['3-6 years']           = len(q_data_joined[q_data_joined[col_select] == '3-6 years'])
        users[group_select]['6-10 years']          = len(q_data_joined[q_data_joined[col_select] == '6-10 years'])
        users[group_select]['More than 10 years']  = len(q_data_joined[q_data_joined[col_select] == 'More than 10 years'])
    elif group_select=='workinggroup':
        users[group_select]['Administration']      = len(q_data_joined[q_data_joined[col_select] == 'Administration'])
        users[group_select]['Infrastructure']      = len(q_data_joined[q_data_joined[col_select] == 'Infrastructure'])
        users[group_select]['IT']                  = len(q_data_joined[q_data_joined[col_select] == 'IT'])
        users[group_select]['Research']            = len(q_data_joined[q_data_joined[col_select] == 'Research'])
    elif group_select=='researchfield':
        users[group_select]['Aeronautics']         = len(q_data_joined[q_data_joined[col_select] == 'Aeronautics'])
        users[group_select]['Earth and environment']      = len(q_data_joined[q_data_joined[col_select] == 'Earth and environment'])
        users[group_select]['Energy']              = len(q_data_joined[q_data_joined[col_select] == 'Energy'])
        users[group_select]['Health']              = len(q_data_joined[q_data_joined[col_select] == 'Health'])
        users[group_select]['Information']         = len(q_data_joined[q_data_joined[col_select] == 'Information'])
        users[group_select]['Matter']              = len(q_data_joined[q_data_joined[col_select] == 'Matter'])

    users['total']        = len(q_data_joined)

    with open('cloud-groups-statistics.csv', 'a') as f:
        sys.stdout = f # Change the standard output to the file we created.
        print ("Statistics on n of groups:")
        print (users)
        sys.stdout = original_stdout # revert to original

    # -- Generation of the relative plot data -- #
    tmp_group: list = []
    freq_overall_abstorel: DataFrame = DataFrame()
    freq_overall_rel: DataFrame = DataFrame()


    for list_idx in range(len(parameters['q_list'])):

        for j,quest in enumerate(parameters['q_list'][list_idx]):

            # Skip not analyzable specific survey options
            if quest not in q_img_exceptions:

                # set new complite quest id
                quest =  f"{q_id}/{quest}"

                # loading specific answer set, eq. Q025/SQ000
                q_series: Series = data.question_for_id(quest).as_series()

                #if quest.replace(f'{q_id}/','') in dataset_frac and quest not in q_img_exceptions:

                # combining base and reference series to a dataframe
                group_filtered: DataFrame = \
                    util.filter_and_group_series(base_data=q_series, group_by=group_series.dropna())


                freq_group: DataFrame = util.dataframe_value_counts(
                    dataframe=group_filtered,
                    relative_values=False,
                    drop_nans=True,
                )

                # relative frequencies based users
                with open('cloud-statistics-clustered.csv', 'a') as f:
                    sys.stdout = f # Change the standard output to the file we created.

                    if group_select=='developer' or group_select=='hifis' or group_select=='team' or group_select=='developerQ5':
                        for idx in freq_group.index:
                            print (quest+","+col_select+","+group_select+"=Yes,"+str(freq_group.loc[idx, 'Yes'])+","+str(users[group_select]['Yes'])+","+str(freq_group.loc[idx, 'Yes'] / users[group_select]['Yes'] * scale))
                            freq_group.loc[idx, 'Yes'] = freq_group.loc[idx, 'Yes'] / users[group_select]['Yes'] * scale
                            print (quest+","+col_select+","+group_select+"=No ,"+str(freq_group.loc[idx, 'No'])+","+str(users[group_select]['No'])+","+str(freq_group.loc[idx, 'No'] / users[group_select]['No'] * scale))
                            freq_group.loc[idx, 'No'] = freq_group.loc[idx, 'No'] / users[group_select]['No'] * scale
                    elif group_select=='employedtime':
                        for idx in freq_group.index:
                            print (quest+","+col_select+","+group_select+"=Up to one year,"+str(freq_group.loc[idx, 'Up to one year'])+","+str(users[group_select]['Up to one year'])+","+str(freq_group.loc[idx, 'Up to one year'] / users[group_select]['Up to one year'] * scale))
                            freq_group.loc[idx, 'Up to one year'] = freq_group.loc[idx, 'Up to one year'] / users[group_select]['Up to one year'] * scale
                            print (quest+","+col_select+","+group_select+"=1-3 years,"+str(freq_group.loc[idx, '1-3 years'])+","+str(users[group_select]['1-3 years'])+","+str(freq_group.loc[idx, '1-3 years'] / users[group_select]['1-3 years'] * scale))
                            freq_group.loc[idx, '1-3 years'] = freq_group.loc[idx, '1-3 years'] / users[group_select]['1-3 years'] * scale
                            print (quest+","+col_select+","+group_select+"=3-6 years,"+str(freq_group.loc[idx, '3-6 years'])+","+str(users[group_select]['3-6 years'])+","+str(freq_group.loc[idx, '3-6 years'] / users[group_select]['3-6 years'] * scale))
                            freq_group.loc[idx, '3-6 years'] = freq_group.loc[idx, '3-6 years'] / users[group_select]['3-6 years'] * scale
                            print (quest+","+col_select+","+group_select+"=6-10 years,"+str(freq_group.loc[idx, '6-10 years'])+","+str(users[group_select]['6-10 years'])+","+str(freq_group.loc[idx, '6-10 years'] / users[group_select]['6-10 years'] * scale))
                            freq_group.loc[idx, '6-10 years'] = freq_group.loc[idx, '6-10 years'] / users[group_select]['6-10 years'] * scale
                            print (quest+","+col_select+","+group_select+"=More than 10 years,"+str(freq_group.loc[idx, 'More than 10 years'])+","+str(users[group_select]['More than 10 years'])+","+str(freq_group.loc[idx, 'More than 10 years'] / users[group_select]['More than 10 years'] * scale))
                            freq_group.loc[idx, 'More than 10 years'] = freq_group.loc[idx, 'More than 10 years'] / users[group_select]['More than 10 years'] * scale
                    elif group_select=='workinggroup':
                        for idx in freq_group.index:
                            print (quest+","+col_select+","+group_select+"=Administration,"+str(freq_group.loc[idx, 'Administration'])+","+str(users[group_select]['Administration'])+","+str(freq_group.loc[idx, 'Administration'] / users[group_select]['Administration'] * scale))
                            freq_group.loc[idx, 'Administration'] = freq_group.loc[idx, 'Administration'] / users[group_select]['Administration'] * scale
                            print (quest+","+col_select+","+group_select+"=Infrastructure,"+str(freq_group.loc[idx, 'Infrastructure'])+","+str(users[group_select]['Infrastructure'])+","+str(freq_group.loc[idx, 'Infrastructure'] / users[group_select]['Infrastructure'] * scale))
                            freq_group.loc[idx, 'Infrastructure'] = freq_group.loc[idx, 'Infrastructure'] / users[group_select]['Infrastructure'] * scale
                            print (quest+","+col_select+","+group_select+"=IT,"+str(freq_group.loc[idx, 'IT'])+","+str(users[group_select]['IT'])+","+str(freq_group.loc[idx, 'IT'] / users[group_select]['IT'] * scale))
                            freq_group.loc[idx, 'IT'] = freq_group.loc[idx, 'IT'] / users[group_select]['IT'] * scale
                            print (quest+","+col_select+","+group_select+"=Research,"+str(freq_group.loc[idx, 'Research'])+","+str(users[group_select]['Research'])+","+str(freq_group.loc[idx, 'Research'] / users[group_select]['Research'] * scale))
                            freq_group.loc[idx, 'Research'] = freq_group.loc[idx, 'Research'] / users[group_select]['Research'] * scale
                    elif group_select=='researchfield':
                        for idx in freq_group.index:
                            print (quest+","+col_select+","+group_select+"=Aeronautics,"+str(freq_group.loc[idx, 'Aeronautics'])+","+str(users[group_select]['Aeronautics'])+","+str(freq_group.loc[idx, 'Aeronautics'] / users[group_select]['Aeronautics'] * scale))
                            freq_group.loc[idx, 'Aeronautics'] = freq_group.loc[idx, 'Aeronautics'] / users[group_select]['Aeronautics'] * scale
                            print (quest+","+col_select+","+group_select+"=Earth and environment,"+str(freq_group.loc[idx, 'Earth and environment'])+","+str(users[group_select]['Earth and environment'])+","+str(freq_group.loc[idx, 'Earth and environment'] / users[group_select]['Earth and environment'] * scale))
                            freq_group.loc[idx, 'Earth and environment'] = freq_group.loc[idx, 'Earth and environment'] / users[group_select]['Earth and environment'] * scale
                            print (quest+","+col_select+","+group_select+"=Energy,"+str(freq_group.loc[idx, 'Energy'])+","+str(users[group_select]['Energy'])+","+str(freq_group.loc[idx, 'Energy'] / users[group_select]['Energy'] * scale))
                            freq_group.loc[idx, 'Energy'] = freq_group.loc[idx, 'Energy'] / users[group_select]['Energy'] * scale
                            print (quest+","+col_select+","+group_select+"=Health,"+str(freq_group.loc[idx, 'Health'])+","+str(users[group_select]['Health'])+","+str(freq_group.loc[idx, 'Health'] / users[group_select]['Health'] * scale))
                            freq_group.loc[idx, 'Health'] = freq_group.loc[idx, 'Health'] / users[group_select]['Health'] * scale
                            print (quest+","+col_select+","+group_select+"=Information,"+str(freq_group.loc[idx, 'Information'])+","+str(users[group_select]['Information'])+","+str(freq_group.loc[idx, 'Information'] / users[group_select]['Information'] * scale))
                            freq_group.loc[idx, 'Information'] = freq_group.loc[idx, 'Information'] / users[group_select]['Information'] * scale
                            print (quest+","+col_select+","+group_select+"=Matter,"+str(freq_group.loc[idx, 'Matter'])+","+str(users[group_select]['Matter'])+","+str(freq_group.loc[idx, 'Matter'] / users[group_select]['Matter'] * scale))
                            freq_group.loc[idx, 'Matter'] = freq_group.loc[idx, 'Matter'] / users[group_select]['Matter'] * scale
                    elif group_select=='operatingsystem':
                        for idx in freq_group.index:
                            print (quest+","+col_select+","+group_select+"=Windows,"+str(freq_group.loc[idx, 'Windows'])+","+str(users[group_select]['Windows'])+","+str(freq_group.loc[idx, 'Windows'] / users[group_select]['Windows'] * scale))
                            freq_group.loc[idx, 'Windows'] = freq_group.loc[idx, 'Windows'] / users[group_select]['Windows'] * scale
                            print (quest+","+col_select+","+group_select+"=Linux,"+str(freq_group.loc[idx, 'Linux'])+","+str(users[group_select]['Linux'])+","+str(freq_group.loc[idx, 'Linux'] / users[group_select]['Linux'] * scale))
                            freq_group.loc[idx, 'Linux'] = freq_group.loc[idx, 'Linux'] / users[group_select]['Linux'] * scale
                            print (quest+","+col_select+","+group_select+"=MacOS,"+str(freq_group.loc[idx, 'MacOS'])+","+str(users[group_select]['MacOS'])+","+str(freq_group.loc[idx, 'MacOS'] / users[group_select]['MacOS'] * scale))
                            freq_group.loc[idx, 'MacOS'] = freq_group.loc[idx, 'MacOS'] / users[group_select]['MacOS'] * scale

                    print()
                    sys.stdout = original_stdout


                # ----------------------- #
                # PART 3: renaming #
                # rename the axis and title to the corresponding question tag
                for idx in range(len(q_data.keys())):
                    if quest == q_collection.questions[idx].full_id:
                        freq_group = freq_group.rename({True: q_collection.questions[idx].label})

                # rename the legend to the corresponding question tag
                if group_select=='developer' or group_select=='developerQ5':
                    freq_group = freq_group.rename(columns={'No': 'No dev.', 'Yes': 'Software dev.'})
                elif group_select=='hifis':
                    freq_group = freq_group.rename(columns={'No': 'Non-HIFIS', 'Yes': 'HIFIS'})
                elif group_select=='team':
                    freq_group = freq_group.rename(columns={'No': 'Single dev.', 'Yes': 'Team'})

                tmp_group.append(freq_group.squeeze())

            elif quest in q_img_exceptions:

                if quest == 'all':
                    freq_overall_abstorel = util.dataframe_value_counts(
                        dataframe=q_data,
                        relative_values=False,
                        drop_nans=True
                    )

                    with open('cloud-statistics-global.csv', 'a') as f:
                        original_stdout = sys.stdout
                        sys.stdout = f # Change the standard output to the file we created.
                        print ()
                        print (q_id+": - "+title+":")
                        print ("Absolute:")
                        print (freq_overall_abstorel)
                        # relative frequencies based users
                        freq_overall_abstorel = freq_overall_abstorel.apply(
                            div, args=([[users['total']/scale] * len(freq_overall_abstorel.iloc[:])])
                        )

                        print ("Relative:")
                        print (freq_overall_abstorel)
                        sys.stdout = original_stdout



                    # map xtic
                    freq_overall_abstorel.rename(mapper=answer_options, axis='columns', inplace=True)

        groups = {group_select: tmp_group, 'all': freq_overall_abstorel}

        tmp = {}
        for group_name in groups:
            # print (q_id+' - Group name 1: '+group_name)
            if parameters['q_list'][list_idx][0] not in q_img_exceptions:
                # which group images should be included [group_select,group_select]
                if (group_name in group_select) and (group_name not in q_img_exceptions):

                    # print (q_id+' - Group name 2: '+group_name)

                    if isinstance(groups[group_name][0],DataFrame):
                        tmp_freq = groups[group_name][0]
                        tmp_freq = tmp_freq.T
                    else:
                        tmp_freq = DataFrame(data=[groups[group_name][0]])
                        tmp_freq = tmp_freq.T

                    for k in range(1,len(groups[group_name])):
                        tmp_freq = tmp_freq.join(groups[group_name][k])

                    tmp[group_name] = tmp_freq
        # add dict
        if tmp:
            print_data.append(tmp)
            tmp_group = []


        for group_name in groups:
            if group_name in parameters['q_list'][list_idx]:
                dataframe_in_list: DataFrame = groups[group_name]
                print_data.append({group_name:dataframe_in_list})

    # rename rawdata for box plots
    q_data.rename(mapper=answer_options, axis='columns', inplace=True)

    if SHOW_PLOTS is True:
        for print_idx in range(len(print_data)):
            for plot_idx,plot_label in enumerate(print_data[print_idx].keys()):
                if plot_all==False and plot_label=='all':
                    continue
                type_label = parameters['plotstyle'][print_idx][plot_idx]
                file_name =f"cloud_{q_id}-{plot_label}-{type_label}"
                data = print_data[print_idx][plot_label]

                if parameters['q_list'][print_idx][0] in q_img_exceptions:
                    show_legend = False
                else:
                    show_legend = True

                if type_label == 'bar':
                    hfs.plotter.plot_bar_chart(
                        data_frame=data.T,
                        plot_file_name=file_name,
                        x_label_rotation=xtic_rot,
                        y_axis_label=parameters['ylabel'][print_idx][plot_idx],
                        round_value_labels_to_decimals=0,
                        show_legend=show_legend,
                        show_value_labels=parameters['barlabels'][print_idx][plot_idx],
                        figure_size=parameters['fig_size'][print_idx][plot_idx],
                        ylim=(parameters['ylimit'][print_idx][plot_idx])
                    )

                elif type_label == 'box':
                    hfs.plotter.plot_box_chart(
                        data_frame=q_data,
                        plot_file_name=file_name,
                        x_label_rotation=xtic_rot,
                        y_axis_label=parameters['ylabel'][print_idx][plot_idx],
                        figure_size=parameters['fig_size'][print_idx][plot_idx],
                        box_face_color="#005AA0"
                    )

# =================== #
# == Program Start == #
# =================== #


PARAMETERS = {
    "Q010": {
             'q_list':   [ ['SQ001','SQ002','SQ003','SQ004','SQ005','SQ006','SQ007','SQ008','SQ009','SQ010'], ['all']               ],
             'fig_size': [ [(7,3.8)]               , [(7,3.8)]                                ],
             'ylimit':   [ [(0,65)]                 , [(0,65)]              ],
             'plotstyle':[ ['bar']                  , ['bar']               ],
             'barlabels':[ [False]                   , [True]                ],
             'ylabel'   :[ ['Answers in % per group']    , ['User answers in %'] ]
            },

    "Q011": {
             'q_list':   [ ['SQ000','SQ001','SQ002','SQ003','SQ004','SQ005','SQ006','SQ007','SQ008','SQ010','SQ011','SQ012'], ['all']               ],
             'fig_size': [ [(7,3.3)]               , [(7,3.3)]                                ],
             'ylimit':   [ [(0,55)]                 , [(0,55)]              ],
             'plotstyle':[ ['bar']                  , ['bar']               ],
             'barlabels':[ [False]                   , [True]                ],
             'ylabel'   :[ ['Answers in % per group']    , ['User answers in %'] ]
            },

    "Q012": {
             'q_list':   [ ['SQ001','SQ002','SQ003','SQ004'], ['all']               ],
             'fig_size': [ [(7,4)]               , [(7,4)]                                ],
             'ylimit':   [ [(0,55)]                 , [(0,55)]              ],
             'plotstyle':[ ['bar']                  , ['bar']               ],
             'barlabels':[ [False]                   , [True]                ],
             'ylabel'   :[ ['Answers in % per group']    , ['User answers in %'] ]
            },

    "Q013": {
             'q_list':   [ ['SQ000','SQ001','SQ002','SQ003','SQ004','SQ005','SQ006','SQ007','SQ008','SQ010','SQ011'], ['all']               ],
             'fig_size': [ [(7,3.0)]               , [(7,3.0)]                                ],
             'ylimit':   [ [(0,70)]                 , [(0,70)]              ],
             'plotstyle':[ ['bar']                  , ['bar']               ],
             'barlabels':[ [False]                   , [True]                ],
             'ylabel'   :[ ['Answers in % per group']    , ['User answers in %'] ]
            },

    "Q014": {
             'q_list':   [ ['SQ001','SQ002','SQ003','SQ004','SQ005','SQ006','SQ007','SQ008','SQ009','SQ010','SQ011','SQ012'], ['all']               ],
             'fig_size': [ [(7,3.5)]               , [(7,3.5)]                                ],
             'ylimit':   [ [(0,80)]                 , [(0,80)]              ],
             'plotstyle':[ ['bar']                  , ['bar']               ],
             'barlabels':[ [False]                   , [True]                ],
             'ylabel'   :[ ['Answers in % per group']    , ['User answers in %'] ]
            }
}

QUESTIONS  = {"Q010": qanalyze, "Q011": qanalyze, "Q013": qanalyze, "Q014": qanalyze}
QUESTIONS2 = {"Q012": qanalyze}

def run(hifis_surveyval: HIFISSurveyval, data: DataContainer):

    # Extra treatment for Q012
    #for q_id, func in QUESTIONS.items():
    #    func(q_id, hifis_surveyval, data, parameters=PARAMETERS[q_id], group_select='total', plot_all=False)

    for q_id, func in QUESTIONS.items():
        # extract question data
        func(q_id, hifis_surveyval, data, parameters=PARAMETERS[q_id], group_select='hifis', plot_all=False)
        func(q_id, hifis_surveyval, data, parameters=PARAMETERS[q_id], group_select='researchfield', plot_all=False)
        func(q_id, hifis_surveyval, data, parameters=PARAMETERS[q_id], group_select='workinggroup', plot_all=False)
        func(q_id, hifis_surveyval, data, parameters=PARAMETERS[q_id], group_select='employedtime', plot_all=False)
        #func(q_id, hifis_surveyval, data, parameters=PARAMETERS[q_id], group_select='developer', plot_all=True)    # [UJ] Not sure why the extra variable V001 was introduced here, yields same results as Q005
        func(q_id, hifis_surveyval, data, parameters=PARAMETERS[q_id], group_select='developerQ5', plot_all=False)
        func(q_id, hifis_surveyval, data, parameters=PARAMETERS[q_id], group_select='team', plot_all=True)



    print('<-- DONE -->')
