# SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
# SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
#
# SPDX-License-Identifier: MIT

from typing import Dict, Set

import matplotlib.pyplot as plt
import modules.descriptive_stats as stats
from hifis_surveyval.core import util
from hifis_surveyval.data_container import DataContainer, Question, \
    QuestionCollection
from hifis_surveyval.hifis_surveyval import HIFISSurveyval
from pandas import DataFrame, Series
from scipy.stats import f_oneway, probplot
from statsmodels.stats.multicomp import pairwise_tukeyhsd


def run(hifis_surveyval: HIFISSurveyval, data: DataContainer):
    """
    Compute descriptive statistics for software specific questions to
    analyze aspects of the development context of different software projects
    """
    # Output path
    path: str = str(hifis_surveyval.settings.ANALYSIS_OUTPUT_PATH) + "\\"

    # Relevant questions
    questions: Dict = {
        "Q038/_": _developer_team_size,
        "Q040": _user_base,
        "Q039/_": _software_size,
        "Q041/_": _software_lifetime,
        "Q042/_": _software_criticality,
        "Q043": _software_type,
        "Q044": _software_target_platform,
        "Q007/_": _software_development_time
    }

    # Create statistics table containing total number of cases
    software_sample: DataFrame = data.data_frame_for_ids(
        requested_ids=list(questions.keys())[:-1]
    ).dropna(how="all")
    software_tbl: DataFrame = stats.get_sample_size(software_sample)

    # Exclude cases from analysis who did not answer any software question
    incomplete_cases: Set = set(data.data_frame_for_ids(
        requested_ids=data.question_collection_ids
    ).index).difference(set(software_sample.index))

    print(software_sample.head())

    # Run the respective analysis for each of the specified questions
    # and save descriptive statistics in joint table
    for question_id, analysis in questions.items():
        software_tbl = analysis(
            hifis_surveyval=hifis_surveyval,
            data=data,
            question_id=question_id,
            stats_tbl=software_tbl,
            cases=incomplete_cases
        )

    # Display and export resulting table of sample characteristics
    print("\nHIFIS software software:\n{}\n"
          .format(software_tbl))
    software_tbl.to_latex(
       buf=path + "software.tex",
       header=True,
       index=True,
       na_rep="",
       float_format="{:0.2f}".format,
       column_format="@{}p{0.9\\textwidth}r@{}r@{}",
       caption="Overview of software characteristics",
       label="tab:software_charcteristics",
       position="htbp",
       longtable=True
    )


def _developer_team_size(hifis_surveyval: HIFISSurveyval, data: DataContainer,
                         question_id: str, stats_tbl: DataFrame, cases: Set) \
        -> DataFrame:
    # Get data on size of different software development teams
    question: Question = data.question_for_id(question_id)
    question.remove_answers(cases)

    # Relative frequencies of observations
    [n, question_freq_rel, unit] = stats.nominal_single_choice(question)

    # Add question statistics to table
    stats_tbl = stats.add_to_table(
        tbl=stats_tbl,
        question=question,
        count=n,
        stats=question_freq_rel,
        unit=unit,
        compact=False
    )

    # Get data on size of different software projects
    question: Question = data.question_for_id(question_id[:5] + "comment")
    question.remove_answers(cases)
    question_data: Series = question.as_series()

    # Summary statistics of numeric variable team size excluding 1-person teams
    question_summary_stats: Series = question_data.mask(question_data==1,
                                                        None).describe()
    print("\nTeam sizes for (n={}) software development teams:\n{}".format(
        int(question_summary_stats[0]), question_summary_stats
    ))

    return stats_tbl


def _software_size(hifis_surveyval: HIFISSurveyval, data: DataContainer,
                   question_id: str, stats_tbl: DataFrame, cases: Set) \
        -> DataFrame:
    # Get data on time size of different software projects
    question: Question = data.question_for_id(question_id)
    question.remove_answers(cases)

    # Relative frequencies of observations
    [n, question_freq_rel, unit] = stats.nominal_single_choice(question)

    # Add question statistics to table
    stats_tbl = stats.add_to_table(
        tbl=stats_tbl,
        question=question,
        count=n,
        stats=question_freq_rel,
        unit=unit,
        compact=False
    )

    return stats_tbl


def _user_base(hifis_surveyval: HIFISSurveyval, data: DataContainer,
               question_id: str, stats_tbl: DataFrame, cases: Set) \
        -> DataFrame:
    # Get data on persons or groups the software is shared with
    question: QuestionCollection = data.collection_for_id(question_id)
    question.remove_answers(cases)

    # Relative frequencies of observations
    [n, question_freq_rel, unit] = stats.nominal_multiple_choice(question)

    print("\nUser groups of (n={}) software products:\n{}".format(
        n, question_freq_rel
    ))

    # Transform multiple choice into single choice question
    ratings: Dict = {
        sub_question.full_id: {True: int(sub_question.full_id[-1::])} for
        sub_question in question._questions.values()
    }
    question_data: DataFrame = question.as_data_frame()
    question_data["Q040/R001"] = question_data \
        .replace(to_replace=ratings, value=None) \
        .max(axis=1, skipna=True).astype("Int32")

    # Replace ratings with sub-question labels
    sub_questions: Dict = {
        sub_question.short_id: sub_question.label for sub_question in
        question._questions.values()
    }
    question_data["Q040/R002"] = \
        ("SQ00" + question_data["Q040/R001"].dropna().astype(str)) \
            .replace(to_replace=sub_questions, value=None)

    question_freq_rel: DataFrame = DataFrame(
        question_data["Q040/R002"].value_counts(normalize=True) * 100
    )
    question_freq_rel = \
        question_freq_rel.reindex(sub_questions.values(), fill_value=0)
    question_freq_rel.index = question_freq_rel.index + " at most"
    question_freq_rel.columns = [""]

    # Add question statistics to table
    stats_tbl = stats.add_to_table(
        tbl=stats_tbl,
        question=question,
        count=n,
        stats=question_freq_rel,
        unit=unit,
        compact=False
    )

    return stats_tbl


def _software_lifetime(hifis_surveyval: HIFISSurveyval, data: DataContainer,
                       question_id: str, stats_tbl: DataFrame, cases: Set) \
        -> DataFrame:
    # Get data on estimated life time of different software projects
    question: Question = data.question_for_id(question_id)
    question.remove_answers(cases)

    # Relative frequencies of observations
    [n, question_freq_rel, unit] = stats.nominal_single_choice(question)

    # Add question statistics to table
    stats_tbl = stats.add_to_table(
        tbl=stats_tbl,
        question=question,
        count=n,
        stats=question_freq_rel,
        unit=unit,
        compact=False
    )

    return stats_tbl


def _software_criticality(hifis_surveyval: HIFISSurveyval, data: DataContainer,
                          question_id: str, stats_tbl: DataFrame, cases: Set) \
        -> DataFrame:
    # Get data on impact of malfunctioning of different software products
    question: Question = data.question_for_id(question_id)
    question.remove_answers(cases)

    # Relative frequencies of observations
    [n, question_freq_rel, unit] = stats.nominal_single_choice(question)

    # Add question statistics to table
    stats_tbl = stats.add_to_table(
        tbl=stats_tbl,
        question=question,
        count=n,
        stats=question_freq_rel,
        unit=unit,
        compact=False
    )

    return stats_tbl


def _software_type(hifis_surveyval: HIFISSurveyval, data: DataContainer,
                   question_id: str, stats_tbl: DataFrame, cases: Set) \
        -> DataFrame:
    # Get data on categories of different software projects
    question: QuestionCollection = data.collection_for_id(question_id)
    question.remove_answers(cases)

    # Relative frequencies of observations
    [n, question_freq_rel, unit] = stats.nominal_multiple_choice(question)

    # Add question statistics to table
    stats_tbl = stats.add_to_table(
        tbl=stats_tbl,
        question=question,
        count=n,
        stats=question_freq_rel,
        unit=unit,
        compact=False
    )

    return stats_tbl


def _software_target_platform(hifis_surveyval: HIFISSurveyval,
                              data: DataContainer, question_id: str,
                              stats_tbl: DataFrame, cases: Set) -> DataFrame:
    # Get data on target platforms of different software projects
    question: QuestionCollection = data.collection_for_id(question_id)
    question.remove_answers(cases)

    # Relative frequencies of observations
    [n, question_freq_rel, unit] = stats.nominal_multiple_choice(question)

    # Add question statistics to table
    stats_tbl = stats.add_to_table(
        tbl=stats_tbl,
        question=question,
        count=n,
        stats=question_freq_rel,
        unit=unit,
        compact=False
    )

    return stats_tbl


def _software_development_time(hifis_surveyval: HIFISSurveyval,
                               data: DataContainer, question_id: str,
                               stats_tbl: DataFrame, cases: Set) -> DataFrame:
    # Get data on percentage of working time spent on coding and
    question: Question = data.question_for_id(question_id)

    # Grouping variable
    grouping_id: str = "Q004/_"
    grouping: Question = data.question_for_id(grouping_id)

    # Group data by research field
    grouped_data: DataFrame = util.filter_and_group_series(
        question.as_series(), grouping.as_series().dropna()
    )

    # Plot descriptive statistics for numeric variable
    stats.grouped_box_plot(
        hifis_surveyval=hifis_surveyval,
        question=question,
        grouping=grouping,
        stats_to_plot=grouped_data
    )

    # TODO: Move to function
    # Generate normal probability plots to check normality assumption
    for group in grouped_data.columns:
        probplot(
            grouped_data[group].dropna().astype(int), dist="norm", plot=plt
        )
        plt.title("Probability Plot: " + question.label + " in " + group)
        plt.show()

    # Compute ratio of min and max standard deviation to check homogeneity of
    # variance assumption
    group_stats: DataFrame = grouped_data.astype(float).describe()
    ratio: float = (group_stats.loc["std", :].max() /
                    group_stats.loc["std", :].min())
    print("\nAssumption of homogeneity is fulfilled: {}\nRatio is {}"
          .format((ratio < 2), ratio)
          )
    print("\n", group_stats)

    # Conduct one-way ANOVA to detect an overall effect of group level
    [f_statistic, p_value] = f_oneway(
        grouped_data.iloc[:, 0].dropna(),
        grouped_data.iloc[:, 1].dropna(),
        grouped_data.iloc[:, 2].dropna(),
        grouped_data.iloc[:, 3].dropna(),
        grouped_data.iloc[:, 4].dropna(),
        grouped_data.iloc[:, 5].dropna()
    )

    # Display results
    df1: int = len(grouped_data.columns) - 1
    df2: int = grouped_data.notna().sum().sum() - len(grouped_data.columns)
    print("\nOne-way ANOVA: F({},{}) = {}, p = {}"
          .format(df1, df2, round(f_statistic, 2), p_value, 3)
          )

    # Prepare data for post-hoc test
    data_df: DataFrame = data.data_frame_for_ids(
        requested_ids=[question_id, grouping_id]
    ).dropna(how="any")

    # Perform pair-wise comparison using Tukey's HSD test
    tukey = pairwise_tukeyhsd(endog=data_df[question_id],
                              groups=data_df[grouping_id],
                              alpha=0.05)

    # Display results
    print("\n", tukey)

    # Grouping variable
    grouping_id: str = "Q006/_"
    grouping: Question = data.question_for_id(grouping_id)

    # Group data by software development skill level
    grouped_data: DataFrame = util.filter_and_group_series(
        question.as_series(), grouping.as_series().dropna()
    )

    grouped_data.drop(columns="Non-developer", inplace=True)

    # Plot descriptive statistics for numeric variable
    stats.grouped_box_plot(
        hifis_surveyval=hifis_surveyval,
        question=question,
        grouping=grouping,
        stats_to_plot=grouped_data
    )

    for group in grouped_data.columns:
        probplot(
            grouped_data[group].dropna().astype(int), dist="norm", plot=plt
        )
        plt.title("Probability Plot: " + question.label + " in " + group)
        plt.show()

    # Compute ratio of min and max standard deviation to check homogeneity of
    # variance assumption
    group_stats: DataFrame = grouped_data.astype(float).describe()
    ratio: float = (group_stats.loc["std", :].max() /
                    group_stats.loc["std", :].min())
    print("\nAssumption of homogeneity is fulfilled: {}\nRatio is {}"
          .format((ratio < 2), ratio)
          )
    print("\n", group_stats)

    # Conduct ANOVA
    [f_statistic, p_value] = f_oneway(
        grouped_data.iloc[:, 0].dropna(),
        grouped_data.iloc[:, 1].dropna(),
        grouped_data.iloc[:, 2].dropna(),
        grouped_data.iloc[:, 3].dropna(),
        grouped_data.iloc[:, 4].dropna())

    df1: int = len(grouped_data.columns) - 1
    df2: int = grouped_data.notna().sum().sum() - len(grouped_data.columns)
    print("\nOne-way ANOVA: F({},{}) = {}, p = {}"
          .format(df1, df2, round(f_statistic, 2), p_value)
          )

    # Prepare data for post-hoc test
    data_df: DataFrame = data.data_frame_for_ids(
        requested_ids=[question_id, grouping_id]
    ).dropna(how="any")

    # Perform pair-wise comparison using Tukey's HSD test
    tukey = pairwise_tukeyhsd(endog=data_df[question_id],
                              groups=data_df[grouping_id],
                              alpha=0.05)

    # Display results
    print("\n", tukey)

    # Grouping variable
    grouping_id: str = "Q003/_"
    grouping: Question = data.question_for_id(grouping_id)

    # Group data by occupation
    grouped_data: DataFrame = util.filter_and_group_series(
        question.as_series(), grouping.as_series().dropna()
    )

    # Plot descriptive statistics for numeric variable
    stats.grouped_box_plot(
        hifis_surveyval=hifis_surveyval,
        question=question,
        grouping=grouping,
        stats_to_plot=grouped_data
    )

    # Generate normal probability plots to check normality assumption
    for group in grouped_data.columns:
        probplot(
            grouped_data[group].dropna().astype(int), dist="norm", plot=plt
        )
        plt.title("Probability Plot: " + question.label + " in " + group)
        plt.show()

    # Compute ratio of min and max standard deviation to check homogeneity of
    # variance assumption
    group_stats: DataFrame = grouped_data.astype(float).describe()
    ratio: float = (group_stats.loc["std", :].max() /
                    group_stats.loc["std", :].min())
    print("\nAssumption of homogeneity is fulfilled: {}\nRatio is {}"
          .format((ratio < 2), ratio)
          )
    print("\n", group_stats)

    # Conduct one-way ANOVA to detect an overall effect of group level
    [f_statistic, p_value] = f_oneway(
        grouped_data.iloc[:, 0].dropna(),
        grouped_data.iloc[:, 1].dropna(),
        grouped_data.iloc[:, 2].dropna(),
        grouped_data.iloc[:, 3].dropna(),
    )

    # Display results
    df1: int = len(grouped_data.columns) - 1
    df2: int = grouped_data.notna().sum().sum() - len(grouped_data.columns)
    print("\nOne-way ANOVA: F({},{}) = {}, p = {}"
          .format(df1, df2, round(f_statistic, 2), p_value, 3)
          )

    # Prepare data for post-hoc test
    data_df: DataFrame = data.data_frame_for_ids(
        requested_ids=[question_id, grouping_id]
    ).dropna(how="any")

    # Perform pair-wise comparison using Tukey's HSD test
    tukey = pairwise_tukeyhsd(endog=data_df[question_id],
                              groups=data_df[grouping_id],
                              alpha=0.05)

    # Display results
    print("\n", tukey)

    # Grouping variable
    grouping_id: str = "Q038/_"
    grouping: Question = data.question_for_id(grouping_id)

    # Group data by team size
    grouped_data: DataFrame = util.filter_and_group_series(
        question.as_series(), grouping.as_series().dropna()
    )

    # Plot descriptive statistics for numeric variable
    stats.grouped_box_plot(
        hifis_surveyval=hifis_surveyval,
        question=question,
        grouping=grouping,
        stats_to_plot=grouped_data
    )

    # Generate normal probability plots to check normality assumption
    for group in grouped_data.columns:
        probplot(
            grouped_data[group].dropna().astype(int), dist="norm", plot=plt
        )
        plt.title("Probability Plot: " + question.label + " in " + group)
        plt.show()

    # Compute ratio of min and max standard deviation to check homogeneity of
    # variance assumption
    group_stats: DataFrame = grouped_data.astype(float).describe()
    ratio: float = (group_stats.loc["std", :].max() /
                    group_stats.loc["std", :].min())
    print("\nAssumption of homogeneity is fulfilled: {}\nRatio is {}"
          .format((ratio < 2), ratio)
          )
    print("\n", group_stats)

    # Conduct one-way ANOVA to detect an overall effect of group level
    [f_statistic, p_value] = f_oneway(
        grouped_data.iloc[:, 0].dropna(),
        grouped_data.iloc[:, 1].dropna(),
        grouped_data.iloc[:, 2].dropna(),
        grouped_data.iloc[:, 3].dropna(),
    )

    # Display results
    df1: int = len(grouped_data.columns) - 1
    df2: int = grouped_data.notna().sum().sum() - len(grouped_data.columns)
    print("\nOne-way ANOVA: F({},{}) = {}, p = {}"
          .format(df1, df2, round(f_statistic, 2), p_value, 3)
          )

    # Prepare data for post-hoc test
    data_df: DataFrame = data.data_frame_for_ids(
        requested_ids=[question_id, grouping_id]
    ).dropna(how="any")

    # Perform pair-wise comparison using Tukey's HSD test
    tukey = pairwise_tukeyhsd(endog=data_df[question_id],
                              groups=data_df[grouping_id],
                              alpha=0.05)

    # Display results
    print("\n", tukey)

    return stats_tbl
