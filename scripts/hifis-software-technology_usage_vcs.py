# SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
# SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
#
# SPDX-License-Identifier: MIT

"""Analyse technology questions, in particular questions related to VCSs."""

from pathlib import Path
from typing import Dict, List

import pandas as pd
from hifis_surveyval.core import util
from pandas import DataFrame, Series

from hifis_surveyval.data_container import DataContainer
from hifis_surveyval.hifis_surveyval import HIFISSurveyval
from hifis_surveyval.models.question import Question
from hifis_surveyval.models.question_collection import QuestionCollection


def run(hifis_surveyval: HIFISSurveyval, data: DataContainer):
    """Execute script that analyses technology questions related to VCSs."""
    print("======== Beginning of Script: " + Path(__file__).stem + " ========")

    # Get QuestionCollections and Questions analysed in this script.
    vcs: QuestionCollection = data.collection_for_id("Q015")
    developers: Question = data.question_for_id("V001/_")
    team: Question = data.question_for_id("V003/_")

    # Get data as DataFrames and Series for these Collections and Questions
    data_vcs_raw: DataFrame = vcs.as_data_frame()
    data_developers_raw: DataFrame = developers.as_series()
    data_team_raw: DataFrame = team.as_series()

    # Define a mapping to recode boolean to string values.
    bool_recode_dict: Dict[str, str] = {True: "Yes", False: "No"}

    # Define an ordering of the AnswerOptions.
    options_order_list: List[str] = ["Yes",
                                     "Don't know it",
                                     "Not relevant",
                                     "Don't know how",
                                     "Doesn't fit my needs",
                                     "Not available"]

    # Define an ordering of the recodes AnswerOptions.
    options_yesno_order_list: List[str] = ["Yes", "No"]

    # Define a mapping to replace the full ID by the label.
    questions_mapping_dict: Dict[str, str] = {question.full_id: question.label
                                              for question in vcs.questions}

    title: str = vcs.text("en-GB")
    print(title)
    questions: List[str] = [question.text("en-GB")
                            for question in vcs.questions]
    print(questions)

    # Create a DataFrame in which only data of participants is contained
    # who answered the question and does not contain only None values.
    data_vcs_answered_only: DataFrame = data_vcs_raw.dropna(axis=0, how="all")
    # Rename columns in DataFrame from the full ID to the label.
    data_vcs_renamed: DataFrame = data_vcs_answered_only.rename(
        mapper=questions_mapping_dict, axis='columns')

    # Define a Series of boolean values which determine which rows have all
    # "No" in each cell.
    vcs_all_no: DataFrame = (data_vcs_renamed["Git"] != "Yes") & \
                            (data_vcs_renamed["SVN"] != "Yes") & \
                            (data_vcs_renamed["Mercurial"] != "Yes")
    # Invert it to a Series of boolean values which determine which cells have
    # not all "No" in each cell.
    vcs_not_all_no = ~vcs_all_no

    ###########################################################################
    # Frequencies of answer options given regarding usage of VCSs
    ###########################################################################

    data_vcs_freq_abs: DataFrame = util.dataframe_value_counts(
        data_vcs_renamed, relative_values=False)

    data_vcs_freq_abs = data_vcs_freq_abs.reindex(options_order_list)

    # Divide all absolute values by the number of answers given to get relative
    # values of multiple choice question.
    data_vcs_freq_rel: DataFrame = \
        data_vcs_freq_abs.div(len(data_vcs_answered_only))

    hifis_surveyval.plotter.plot_bar_chart(
        data_frame=data_vcs_freq_rel,
        plot_file_name="Q015-vcs-per-options",
        plot_title=title,
        x_axis_label="Answer options",
        y_axis_label="Usage VCSs (relative counts)",
        x_label_rotation=45,
        round_value_labels_to_decimals=2,
        figure_size=(10, 5))

    ###########################################################################
    # Frequencies regarding overall usage of VCSs
    ###########################################################################

    data_vcs_all_yesno_freq_rel: DataFrame = util.dataframe_value_counts(
        DataFrame(vcs_not_all_no), relative_values=True)
    data_vcs_all_yesno_freq_rel = \
        data_vcs_all_yesno_freq_rel.rename(bool_recode_dict, axis=0)
    data_vcs_all_yesno_freq_rel = \
        data_vcs_all_yesno_freq_rel.rename({0: "Version Control Systems"},
                                           axis=1)
    data_vcs_all_yesno_freq_rel = \
        data_vcs_all_yesno_freq_rel.T[options_yesno_order_list]

    hifis_surveyval.plotter.plot_bar_chart(
        data_frame=data_vcs_all_yesno_freq_rel,
        plot_file_name="Q015-vcs-overall",
        plot_title=title,
        plot_title_fontsize=16,
        x_axis_label="",
        y_axis_label="Usage VCSs (relative counts)",
        x_label_rotation=0,
        round_value_labels_to_decimals=2,
        figure_size=(6, 5))

    ###########################################################################
    # Frequencies regarding usage of VCSs based on answers given by developers
    ###########################################################################

    data_vcs_per_developers: DataFrame = \
        util.filter_and_group_series(vcs_not_all_no,
                                     data_developers_raw.dropna())

    data_vcs_all_yesno_per_developers_freq_rel: DataFrame = \
        util.dataframe_value_counts(data_vcs_per_developers,
                                    relative_values=True)
    data_vcs_all_yesno_per_developers_freq_rel = \
        data_vcs_all_yesno_per_developers_freq_rel.rename(
            {"Yes": "Software-Developers", "No": " Non-Software-Developers"},
            axis=1)
    data_vcs_all_yesno_per_developers_freq_rel = \
        data_vcs_all_yesno_per_developers_freq_rel.rename(
            bool_recode_dict, axis=0)
    data_vcs_all_yesno_per_developers_freq_rel = \
        data_vcs_all_yesno_per_developers_freq_rel.T[options_yesno_order_list]

    hifis_surveyval.plotter.plot_bar_chart(
        data_frame=data_vcs_all_yesno_per_developers_freq_rel,
        plot_file_name="Q015-vcs-swdevs",
        plot_title=title,
        plot_title_fontsize=16,
        x_axis_label="",
        y_axis_label="Usage VCSs (relative counts)",
        x_label_rotation=0,
        round_value_labels_to_decimals=2,
        figure_size=(6, 5))

    ###########################################################################
    # Frequencies regarding usage of VCSs depending on team size
    ###########################################################################

    data_vcs_plus_developers = \
        DataFrame(vcs_not_all_no).join(DataFrame(data_developers_raw),
                                       on="id",
                                       how="inner")
    data_vcs_only_developers = \
        data_vcs_plus_developers[data_vcs_plus_developers["V001/_"] == "Yes"]

    data_vcs_per_team_size: DataFrame = \
        util.filter_and_group_series(data_vcs_only_developers[0],
                                     data_team_raw.dropna())

    data_vcs_all_yesno_per_team_size_freq_rel: DataFrame = \
        util.dataframe_value_counts(data_vcs_per_team_size,
                                    relative_values=True)
    data_vcs_all_yesno_per_team_size_freq_rel = \
        data_vcs_all_yesno_per_team_size_freq_rel.rename(
            {"Yes": "Team-Developer", "No": "Individual-Developer"},
            axis=1)
    data_vcs_all_yesno_per_team_size_freq_rel = \
        data_vcs_all_yesno_per_team_size_freq_rel.rename(
            bool_recode_dict, axis=0)
    data_vcs_all_yesno_per_team_size_freq_rel = \
        data_vcs_all_yesno_per_team_size_freq_rel[["Individual-Developer",
                                                   "Team-Developer"]]
    data_vcs_all_yesno_per_team_size_freq_rel = \
        data_vcs_all_yesno_per_team_size_freq_rel.T[options_yesno_order_list]

    hifis_surveyval.plotter.plot_bar_chart(
        data_frame=data_vcs_all_yesno_per_team_size_freq_rel,
        plot_file_name="Q015-vcs-team",
        plot_title=title,
        plot_title_fontsize=16,
        x_axis_label="Team Size",
        y_axis_label="Usage VCSs (relative counts)",
        x_label_rotation=0,
        round_value_labels_to_decimals=2,
        figure_size=(6, 5))

    ###########################################################################

    print("=========== End of Script: " + Path(__file__).stem + " ===========")
