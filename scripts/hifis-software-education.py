# SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
# SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
#
# SPDX-License-Identifier: MIT

"""Analyze education specific survey questions."""


import pandas as pd

from hifis_surveyval.data_container import DataContainer
from hifis_surveyval.hifis_surveyval import HIFISSurveyval


def run(hifis_surveyval: HIFISSurveyval, data: DataContainer):
    """Framework specific entry point."""
    _analyze_used_operating_systems_q020(data, hifis_surveyval.plotter)
    _analyze_used_programming_language_q021(data, hifis_surveyval.plotter)
    _analyze_primer_programming_language_q022(data, hifis_surveyval.plotter)
    _analyze_trending_programming_language_q023(data, hifis_surveyval.plotter)
    _analyze_learning_mode_q024(data, hifis_surveyval.plotter)


def _analyze_used_operating_systems_q020(data, plotter):
    question_collection = data.collection_for_id("Q020")
    answers_df = _get_labeled_dataframe_for(question_collection)
    abs_answers_series = answers_df.count()
    num_answers = len(answers_df.dropna(how="all"))
    rel_answers_series = abs_answers_series / num_answers * 100

    # Print question and answers
    print(question_collection.text("en-GB"))
    print("Number of Answers", num_answers)
    print("Absolute Counts")
    print(abs_answers_series)
    print()
    print("Relative Counts to number of answers")
    print(rel_answers_series)
    print()


def _analyze_used_programming_language_q021(data, plotter):
    question_collection = data.collection_for_id("Q021")
    answers_df = _get_labeled_dataframe_for(question_collection)
    answers_df.rename(columns = {"C#": "C Sharp"}, inplace=True)
    num_answers = len(answers_df.dropna(how="all"))
    abs_answers_series = answers_df.count()
    rel_answers_series = abs_answers_series / num_answers * 100

    # Print question and answers
    print(question_collection.text("en-GB"))
    print("Number of Answers", num_answers)
    print("Absolute Counts")
    print(abs_answers_series)
    print()
    print("Relative Counts to number of answers")
    print(rel_answers_series)
    print()

    # Plot answers
    _plot_answers(question_collection, rel_answers_series[rel_answers_series > 1], plotter)


def _analyze_primer_programming_language_q022(data, plotter):
    question_collection = data.collection_for_id("Q022")
    answers_df = _get_labeled_dataframe_for(question_collection)

    # Calculate absolute counts and clean up the labels
    abs_answers_series = answers_df.value_counts()
    flat_index = abs_answers_series.index.to_flat_index()
    labels = []
    for programming_language in flat_index:
        if programming_language[0] == "C#": # Otherwise Latex build gets confused
            labels.append("C Sharp")
        else:
            labels.append(programming_language[0])
    abs_answers_series.index = labels

    # Calculate number of answers and relative counts
    num_answers = abs_answers_series.sum()
    rel_answers_series = abs_answers_series / num_answers * 100

    # Print question and answers
    print(question_collection.text("en-GB"))
    print("Number of Answers", num_answers)
    print("Absolute Counts")
    print(abs_answers_series)
    print()
    print("Relative Counts to number of answers")
    print(rel_answers_series)
    print()

    # Plot answers
    _plot_answers(question_collection, rel_answers_series[rel_answers_series > 1], plotter)


def _analyze_trending_programming_language_q023(data, plotter):
    question_collection = data.collection_for_id("Q023")
    answers_df = _get_labeled_dataframe_for(question_collection)
    yes_no_answers_series = answers_df["Trending programming languages"].value_counts()
    language_answers_df = answers_df.drop("Trending programming languages", axis=1)
    abs_language_answers_series = language_answers_df.count()
    num_language_answers = abs_language_answers_series.sum()
    rel_language_answers_series = abs_language_answers_series / num_language_answers * 100
    rel_language_answers_series.sort_values(ascending=False, inplace=True)

    # Print question and answers
    print(question_collection.text("en-GB"))
    print(yes_no_answers_series)
    print("Number of language answers", num_language_answers)
    print("Absolute language answers")
    print(abs_language_answers_series)
    print()
    print("Relative language answers")
    print(rel_language_answers_series)
    print()

    # Plot answers
    _plot_answers(question_collection, rel_language_answers_series, plotter)


def _analyze_learning_mode_q024(data, plotter):
    question_collection = data.collection_for_id("Q024")
    answers_df = _get_labeled_dataframe_for(question_collection)
    answers_df.drop("No answer", axis=1, inplace=True)
    answers_df.drop("Other", axis=1, inplace=True)
    abs_answers_series = answers_df.count()
    num_answers = len(answers_df.dropna(how="all"))
    rel_answers_series = abs_answers_series / num_answers * 100

    # Print question and answers
    print(question_collection.text("en-GB"))
    print("Number of Answers", num_answers)
    print("Absolute Counts")
    print(abs_answers_series)
    print()
    print("Relative Counts to number of answers")
    print(rel_answers_series)
    print()

    # Plot answers
    _plot_answers(question_collection, rel_answers_series, plotter)


def _get_labeled_dataframe_for(question_collection):
    dataframe = question_collection.as_data_frame()
    question_id_label_map = {
        question.full_id: question._label for question in question_collection.questions
    }
    return dataframe.rename(columns=question_id_label_map)


def _plot_answers(question_collection, answer_series, plotter):
    plotter.plot_bar_chart(
        pd.DataFrame(answer_series),
        show_legend=False,
        plot_title=question_collection.short_id + ": " + question_collection.text("en-GB"),
        plot_file_name="education_" + question_collection.short_id,
        x_label_rotation=45,
        figure_size=[10, 4]
    )
