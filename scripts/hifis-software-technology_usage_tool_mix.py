# SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
# SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
#
# SPDX-License-Identifier: MIT

"""Analyse technology questions about various tools."""

from pathlib import Path
from typing import Dict, List

from hifis_surveyval.core import util
from pandas import DataFrame

from hifis_surveyval.data_container import DataContainer
from hifis_surveyval.hifis_surveyval import HIFISSurveyval
from hifis_surveyval.models.question import Question
from hifis_surveyval.models.question_collection import QuestionCollection


def run(hifis_surveyval: HIFISSurveyval, data: DataContainer):
    """Analyses technology question about various tools."""
    print("======== Beginning of Script: " + Path(__file__).stem + " ========")

    # Get QuestionCollections and Questions analysed in this script.
    mix: QuestionCollection = data.collection_for_id("Q019")

    # Get data as DataFrames and Series for these Collections and Questions
    data_mix_raw: DataFrame = mix.as_data_frame()

    # Define an ordering of the AnswerOptions.
    options_order_list: List[str] = ["Yes",
                                     "Don't know it",
                                     "Not relevant",
                                     "Don't know how",
                                     "Doesn't fit my needs",
                                     "Not available"]

    # Define a mapping to replace the full ID by the label.
    questions_mapping_dict: Dict[str, str] = {question.full_id: question.label
                                              for question in mix.questions}

    title: str = mix.text("en-GB")
    print(title)
    questions: List[str] = [question.text("en-GB")
                            for question in mix.questions]
    print(questions)

    # Create a DataFrame in which only data of participants is contained
    # who answered the question and does not contain only None values.
    data_mix_answered_only: DataFrame = data_mix_raw.dropna(axis=0, how="all")
    # Rename columns in DataFrame from the full ID to the label.
    data_mix_renamed: DataFrame = data_mix_answered_only.rename(
        mapper=questions_mapping_dict, axis='columns')

    ###########################################################################
    # Frequencies of answers given regarding usage of various tools
    ###########################################################################

    data_mix_freq_abs: DataFrame = util.dataframe_value_counts(
        data_mix_renamed, relative_values=False)

    data_mix_freq_abs = data_mix_freq_abs.reindex(options_order_list)

    # Divide all absolute values by the number of answers given to get relative
    # values of multiple choice question.
    data_mix_freq_rel: DataFrame = \
        data_mix_freq_abs.div(len(data_mix_answered_only))

    list_tools_1 = ["OpenStack", "JupyterHub", "Rocket.Chat", "Mattermost"]
    list_tools_2 = ["Nextcloud", "LimeSurvey", "ShareLaTex", "Jira"]

    hifis_surveyval.plotter.plot_bar_chart(
        data_frame=data_mix_freq_rel[list_tools_1],
        plot_file_name="Q019-mix1-per-options",
        plot_title=title,
        plot_title_fontsize=16,
        x_axis_label="Answer options",
        y_axis_label="Usage various tools (relative counts)",
        x_label_rotation=0,
        round_value_labels_to_decimals=2,
        figure_size=(13, 5))

    hifis_surveyval.plotter.plot_bar_chart(
        data_frame=data_mix_freq_rel[list_tools_2],
        plot_file_name="Q019-mix2-per-options",
        plot_title=title,
        plot_title_fontsize=16,
        x_axis_label="Answer options",
        y_axis_label="Usage various tools (relative counts)",
        x_label_rotation=0,
        round_value_labels_to_decimals=2,
        figure_size=(13, 5))

    ###########################################################################

    print("=========== End of Script: " + Path(__file__).stem + " ===========")
