# SPDX-FileCopyrightText: 2021 Deutsches Elektronen-Synchrotron DESY
# SPDX-FileCopyrightText: 2021 Deutsches Zentrum für Luft- und Raumfahrt e. V. (DLR)
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Berlin für Materialien und Energie GmbH
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden - Rossendorf e. V.
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum - GFZ
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum für Umweltforschung GmbH - UFZ
#
# SPDX-License-Identifier: MIT

from pathlib import Path
from typing import Dict, List

import numpy as np
import pandas as pd
from pandas import DataFrame, Series

from hifis_surveyval.core import util
from hifis_surveyval.data_container import DataContainer
from hifis_surveyval.hifis_surveyval import HIFISSurveyval
from hifis_surveyval.models.question import Question
from hifis_surveyval.models.question_collection import QuestionCollection


def run(hifis_surveyval: HIFISSurveyval, data: DataContainer):

    # -- INIT VARIABLE ----- #
    rawdata          = {}
    freq_data        = {}
    freq_data_tmp    = {}
    freq_quests_dict = {}
    freq_answer_options_dict = {}
    quests           = ["Q012"]
    show_plots       = True
    # ---------------------- #

    # Available plotter types:
    #   bar chart:    hifis_surveyval.plotter.plot_bar_chart()
    #   box chart:    hifis_surveyval.plotter.plot_box_chart()
    #   matrix chart: hifis_surveyval.plotter.plot_matrix_chart()
    for q in quests:
        q_collection: QuestionCollection = data.collection_for_id(q)
        q_data: DataFrame             = q_collection.as_data_frame()
        question_text: str            = q_collection.text("en-GB")
        title: str                    = f'{q} : {question_text}'

        # extract question data
        rawdata[q]: QuestionCollection = data.collection_for_id(q)

        # extraction of the frequencies of the multiple choice questions
        freq_data_tmp[q]: DataFrame = util.dataframe_value_counts(
            dataframe=rawdata[q].as_data_frame(),
            relative_values=False,
            drop_nans=True
        )

        # >> freq_quests_dict = { 'Q025':'What ...', ... }
        freq_quests_dict[q] = q + ' : ' + rawdata[q]._text.get_translation('en-GB')
        print()
        print(freq_quests_dict[q])

        # >> freq_answer_dict = { 'Q025/SQ000':'None', ... }
        freq_answer_options_dict[q] = { question.full_id : question._label for question in rawdata[q].questions }

        freq_data[q] = freq_data_tmp[q].rename(
            mapper=freq_answer_options_dict[q],
            axis='columns'
        )

        print('\n<< -- Available answer options -- >>')
        for val in freq_answer_options_dict[q]:
            print(val, ':', freq_answer_options_dict[q][val])

        print ("Absolute:")
        print (freq_data_tmp[q])

        # extraction of the frequencies of the multiple choice questions
        freq_data_tmp[q]: DataFrame = util.dataframe_value_counts(
            dataframe=rawdata[q].as_data_frame(),
            relative_values=True,
            drop_nans=True
        )

        print ("Relative:")
        print (freq_data_tmp[q])

        if show_plots==True:
            hifis_surveyval.plotter.plot_bar_chart(
                data_frame=freq_data[q].T,
                x_label_rotation=45,
                plot_file_name="cloud_"+q,
                figure_size=[7,4],
                y_axis_label='Relative User answers [0..1]',
                show_value_labels=False
        )




